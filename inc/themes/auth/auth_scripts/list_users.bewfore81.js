$(document).ready(function() {
    
        
    // remove the default button class
    $.fn.dataTable.Buttons.defaults.dom.button.className = 'btn';
    
    
    // configure datatables
    var table = $('#table-list-users').DataTable( {
    
    // remember the state of the table
    stateSave: false,
    
    order: [
        [ 6, "dsc" ]
    ],
     
    pagingType: "full_numbers",
    
    infoEmpty: "No records available",
     
     
    dom :
        "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
        //  "order": [[ 3, "desc" ]]
    buttons: [
        {
            extend:     'print',
            text:       '<i class="fas fa-print"></i>',
            titleAttr:  'Print',
            className:  'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            orientation: 'landscape',
        },   
        {
            extend:    'copyHtml5',
            text:      '<i class="fas fa-copy"></i>',
            titleAttr: 'Copy',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fas fa-file-excel"></i>',
            titleAttr: 'Excel',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fas fa-file-csv"></i>',
            titleAttr: 'CSV',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fas fa-file-pdf"></i>',
            titleAttr: 'PDF',
            className: 'btn btn-primary btn-md',
            orientation: 'landscape',
            
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            pageSize: 'A3' 
        },
    ],
    language: { 
            search: '', 
            searchPlaceholder: "Search...",
            lengthMenu: "_MENU_",
    },
    
    columnDefs: [
        
        // this orders by last login first and then by created.  This links
        // to order: [ [ 6, "dsc" ] ], in the top part.
        {
        targets: [ 5 ],
        orderData: [ 6, 7 ]
         },
    
    
        {
            targets: [ 0 ],
            visible: false,
            searchable: false
        },
        {
            targets: [ 1 ],
            visible: true,
        },
        {
            
            targets: [7],
            
            render: function(data, type, full) {
                return '<a href="">'+data+'</a>';
            },
                
        },
        
        
        
        
    ],
        
    
} ); // end of datatables declaration

 $('#table-list-users tbody').on('mouseenter', 'tr', function(){
        var data = table.row( this ).data();
        console.log(data[0]);
        var subMenu = '<div> <a href=/MH_auth_admin/edit_user/' + data[0] +'>edit</a> | view | delete </div>';
        console.log(subMenu);       
        $(this).append(subMenu);
    });
    $('#table-list-users tbody').on('mouseleave', this, function(){
       $(this).children("div").remove();
   });  
/**   
    $('#table-list-users tbody').on('mouseenter', 'tr', function(){
        var data = table.row( this ).data();
        console.log(data[0]);
        
        var subMenu = '<div> <a href=/MH_auth_admin/edit_user/' + data[0] +'>edit</a> | view | delete </div>';
        console.log(subMenu);       
        $('#first_name').append(subMenu);
    });
    $('#table-list-users tbody').on('mouseleave', '#first_name', function(){
       $(this).children("div").remove();
   });
**/
  
});
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
B - btton
**/

/**
    "iDisplayLength": 25,
    "dom" : '<Blfr<t>ipl>',
    "ordering"  : true,
    
    "buttons" : [
                'copy', 'excel', 'pdf', 'print'
            ],
            
    "select"    : true,
    
    "order": [[ 7, "desc" ]],
    
   
    
   // "stateSave"     : true,
   **/