$(document).ready(function() {
    

    // configure datatables
    var table = $('#table-list-users').DataTable( {
    scrollX:    true,
    select:     true,
    select:     {
                    style:    'os',
                },
        
    // read data into object
    columns: [
        
        { // column 0  
            "data":             null,
            "className":        'details-control',
            "orderable":        false,
            "defaultContent":   '',
            "render":           function () {
                                    return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                                },
            "width":            "15px"
        },
        
        { // column 1  
            "data":         "id",
            "visible":      true,
            
        },
        
        {   // column 2
            "data":         "first_name",
            "render":       function ( data, type, row ) {
                                return row.first_name +'</br> '+ row.last_name;
                            },
        },
        
        {   // column 3   
            "data":         "last_name",
            "visible":      false,
        },
        
        {   // column 4
            "data":         "email",
        },
        
        {   // column 5
            "data":         "phone"
        },
        
        {   // column 6
            "data":         "company",
        },
        {   // column 6
            "data":         "move_from",
        },
                {   // column 6
            "data":         "move_to",
        },
                {   // column 6
            "data":         "expected_date",
        },
        {   // column 7
            "data":         "last_login"
        },
        
        {   // column 8
            "data":         "Create",
        },
        
        {   // column 9
            "data":         "groups"
        },
        
        {   // column 10
            "data":         "active",
            
            
        },
        
        // column 11
        null, // this is the icon for view, edit and delete
    ], 
     
    // remember the state of the table
    stateSave: false,
    
    order: [
        [ 6, "dsc" ]
    ],
     
    pagingType: "full_numbers",
    
    infoEmpty: "No records available",
     
    dom :
        "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
        //  "order": [[ 3, "desc" ]]
    buttons: [
        {
            extend:     'print',
            text:       '<i class="fas fa-print"></i>',
            titleAttr:  'Print',
            className:  'btn btn-primary btn-md',
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9,10]
            },
            orientation: 'landscape',
        },   
        {
            extend:    'copyHtml5',
            text:      '<i class="fas fa-copy"></i>',
            titleAttr: 'Copy',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fas fa-file-excel"></i>',
            titleAttr: 'Excel',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fas fa-file-csv"></i>',
            titleAttr: 'CSV',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fas fa-file-pdf"></i>',
            titleAttr: 'PDF',
            className: 'btn btn-primary btn-md',
            orientation: 'landscape',
            
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9]
            },
            pageSize: 'A3' 
        },
    ],
    language: { 
            search: '', 
            searchPlaceholder: "Search...",
            lengthMenu: "_MENU_",
    },
   
    columnDefs: [
        
        // this orders by last login first and then by created.  This links
        // to order: [ [ 6, "dsc" ] ], in the top part.
        {
        targets: [ 5 ],
        orderData: [ 6, 7 ]
         },
    
    ],// end of columnDefs
        
} ); // end of datatables declaration

  
  
}); // end doc ready
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
B - btton
**/
