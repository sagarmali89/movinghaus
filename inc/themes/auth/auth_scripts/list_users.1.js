$(document).ready(function() {
    // remove the default button class
    $.fn.dataTable.Buttons.defaults.dom.button.className = 'btn';
    
   var table = $('#table-list-users').DataTable( {
    "dom" :
        "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
    buttons: [
            {
                extend:    'copyHtml5',
                text:      '<i class="fas fa-copy"></i>',
                titleAttr: 'Copy',
                className: 'btn btn-primary btn-md'
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-primary btn-md'
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fas fa-file-csv"></i>',
                titleAttr: 'CSV',
                className: 'btn btn-primary btn-md'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-primary btn-md'
            },
        ],
        
         language: { 
             search: '', 
             searchPlaceholder: "Search...",
             lengthMenu: "_MENU_",
             
         },


    
        
    } );
    
       // var table = $('#table-list-users').DataTable();

        $('#table-list-users').on('mouseenter', 'td', function(){
            // currow = $(this).closest('tr');
            // someValueTwo = currow.find('td:eq(0)').text();    
           var data = table.row( this ).data();
     
            console.log(data);
          //  someValueTwo = 1;
            var subMenu = '<div> <a href=/MH_auth_admin/edit_user/' + 1+' >edit</a> | view | delete </div>';
            $(this).append(subMenu);
        }
        );
 
         $('#table-list-users tbody').on('mouseleave', 'td', function(){
            $(this).children("div").remove();
         });
        
 

    
    
    
    
});
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
B - btton
**/

/**
    "iDisplayLength": 25,
    "dom" : '<Blfr<t>ipl>',
    "ordering"  : true,
    
    "buttons" : [
                'copy', 'excel', 'pdf', 'print'
            ],
            
    "select"    : true,
    
    "order": [[ 7, "desc" ]],
    
   
    
   // "stateSave"     : true,
   **/