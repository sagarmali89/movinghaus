$(document).ready(function() {
    $('#table-mh-admin-surcharge-index').DataTable( {
    dom : '<Blfr<t>ipl>',
          buttons: [
              'copy', 'excel', 'pdf', 'print'
          ],
        select: true,
                "order": [[ 4, "asc" ]],
        stateSave: true,
        
        "ajax": {
                "url": "https://www.movinghaus.net/Surcharge_admin/surcharge_load_data",  
                "type": "GET", 
                "dataSrc": ""
                },
  
        "columns": [
      
         { "data": "surcharge_id", "orderable": true },
         { "data": "surcharge_curr", "orderable": true },
         { "data": "surcharge_name", "orderable": true },
         { "data": "surcharge_unit_id", "orderable": true },
         { "data": "surcharge_value", "orderable": true },
         { "data": "surcharge_category_id", "orderable": true },
         { "data": "created_timestamp", "orderable": true },
         { "data": "updated_timestamp", "orderable": true },
         
        ]
    } );
} );
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
**/