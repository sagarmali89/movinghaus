$(document).ready(function(){

	load_data();
	
	
	// The hash (#) specifies to select elements by their ID's

	// The dot (.) specifies to select elements by their classname

		
	/*
	|	This is the LIST method of CRUD. 
	|	At the top of the table, a blank row is listed to simply add and entry.
	*/
	
	function load_data(){
		$.ajax({
			url: "https://www.movinghaus.net/Surcharge_admin/load_data",
			dataType: "JSON",
			success: function(data){
				// create a blank form at the top of the table for submission.
				var html = '<tr>';
				html += '<td id="surcharge_id" contenteditable placeholder="Enter surcharge id"></td> ';
				html += '<td id="surcharge_curr" contenteditable placeholder="Enter surcharge curr"></td> ';
				html += '<td id="surcharge_name" contenteditable placeholder="Enter surcharge name"></td> ';
				html += '<td id="surchage_unit" contenteditable placeholder="Enter surchage unit"></td> ';
				html += '<td id="surcharge_value" contenteditable placeholder="Enter surcharge value"></td> ';
				html += '<td id="surcharge_category_id" contenteditable placeholder="Enter surcharge category id"></td> ';
				html += '<td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span></button></td></tr>';
				
				for(var count=0; count < data.length; count++) {
					
					console.log(data[count].surcharge_id);
					html += '<tr>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surcharge_id" contenteditable>'+data[count].surcharge_id+'</td>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surcharge_curr" contenteditable>'+data[count].surcharge_curr+'</td>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surcharge_name" contenteditable>'+data[count].surcharge_name+'</td>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surchage_unit" contenteditable>'+data[count].surchage_unit+'</td>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surcharge_value" contenteditable>'+data[count].surcharge_value+'</td>';
					html += '<td class="table_data" data-row_id="'+data[count].surcharge_id+'" data-column_name="surcharge_category_id" contenteditable>'+data[count].surcharge_category_id+'</td>';
					 html += '<td><button type="button" name="delete_btn" id="'+data[count].surcharge_id+'" class="btn btn-xs btn-danger btn_delete"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
				}
				
				console.log(html);
				
				$('tbody').html(html); // spit out the html
				
				//console.log(html);
			} // end of success
		})// end of ajax
	} // end of load_data
	
	/*
	|	This is the CREATE method of CRUD.  
	*/
	$(document).on('click', '#btn_add', function(){
		var surcharge_curr			= $('#surcharge_curr').text();
		var surcharge_name			= $('#surcharge_name').text();
		var surchage_unit			= $('#surchage_unit').text();
		var surcharge_value 		= $('#surcharge_value').text();
		var surcharge_category_id	= $('#surcharge_category_id').text();
		if (surcharge_curr === "") {
			alert ('Enter surcharge_curr');
			return false;
		}
		
		if (surcharge_name == "" || surcharge_name ==  null) {
			console.log("Value of surcharge_name is:", surcharge_name);
			alert ('Enter surcharge_name');
			return false;	
		}
		if (surcharge_name.length > 10 ) {
		
			alert ('Surcharge name must be less than 10 characters');
			return false;	
		}
		
		if (surchage_unit === '') {
			alert ('Enter surchage_unit');
			return false;
		}
		if (surcharge_value === '') {
			alert ('Enter surcharge_value');
			return false;
		}
		if (surcharge_category_id === '') {
			alert ('Enter surcharge_category_id');
			return false;
		}
		$.ajax({
			url:	"https://www.movinghaus.net/Surcharge_admin/insert_data",
			method:  "POST",
			data:{surcharge_curr:surcharge_curr, surcharge_name:surcharge_name, surchage_unit:surchage_unit, surcharge_value:surcharge_value, surcharge_category_id:surcharge_category_id},
			success: function(data){
				load_data();
			}
		});
	});

	// UPDATE
	$(document).on('blur', '.table_data', function(){
		var id				= $(this).data('row_id');
		var table_column	= $(this).data('column_name');
		var value			= $(this).text();
		$.ajax({
				url: "https://www.movinghaus.net/Surcharge_admin/update_data",
				method:  "POST",
				data:{id:id, table_column:table_column, value:value},
				success: function(data){
					load_data();
				}
				
		})
	});
	
	$(document).on('click', '.btn_delete', function(){
		var surcharge_id	= $(this).attr('id');
		console.log(surcharge_id);
		if(confirm("Are you sure you want to delete?")){
			$.ajax({
				url: "https://www.movinghaus.net/Surcharge_admin/delete_data",
				method:  "POST",
				data:{surcharge_id:surcharge_id},
				success: function(data){
					load_data();
				}
			});
		}
	});	
	
}); 