$(document).ready(function() {
    $('#table-mh-admin-surcharge-category-index').DataTable( {
   
    dom : '<Blfr<t>ipl>',

    buttons:    [
                'copy', 'excel', 'pdf', 'print'
                ],
          
    select:     true,
                "order": [
                    [ 4, "asc" ]
                ],
 
    stateSave: true,
   
    columns:    [
        
                { // column 0  
                    "data":             null,
                    "className":        'details-control',
                    "orderable":        false,
                    "defaultContent":   '',
                    "render":           function () {
                                            return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                                        },
                    "width":            "15px"
                },
        
                { // column 1  
                    "data":         "surcharge_category_name",
                    "visible":      true,
                    
                },
                
                {   // column 2
                    "data":         "surcharge_category_comment	",
                     "visible":      true,
                },
              
                
                {   // column 3
                    "data":         "created_timestamp"
                },
                
                {   // column 4
                    "data":         "updated_timestamp",
                },
           
                // column 5
                null,
                ],
   
    
} );
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
**/