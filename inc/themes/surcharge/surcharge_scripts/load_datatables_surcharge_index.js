var mainData = null;
var newSurchargeForm  = 0;
$(document).ready(function() {
    var editableArray =  new Array();
    var surcharge_categories_opt;
    var surcharge_curr_opt;
    var surcharge_unit_opt;
    var surcharge_names_array;

    $.get("https://www.movinghaus.net/Surcharge_admin/get_surcharge_categories", function(data){
        data = JSON.parse(data);
        mainData = data;
        surcharge_categories_opt = data['surcharge_categories_opt'];
        surcharge_curr_opt = data['surcharge_curr_opt'];
        surcharge_unit_opt = data['surcharge_unit_opt'];
        surcharge_names_array = data['surcharge_names_array'];
    });

    var table = $('#table-mh-admin-surcharge-index').DataTable( {
        dom : "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        scrollX:    true,
        stateSave: true,
        select: true,
        "orderClasses": false,
        "ajax": {
            "url": "https://www.movinghaus.net/Surcharge_admin/surcharge_load_data",  
            "type": "GET", 
            "dataSrc": ""
        },
        rowId: 'surcharge_id',
        "columns": [
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                     return "<input type='checkbox' name='surcharges_check[]' value='"+full.surcharge_id+"'/>";
                 }
            },
            //{ "data": "surcharge_id",   "orderable": true },
            { "data": "surcharge_category_name",    "orderable": true },
            { "data": "surcharge_name", "orderable": true },
            { "data": "surcharge_curr", "orderable": true },
            { "data": "surcharge_value",    "orderable": true },
            { "data": "unit_name",  "orderable": true },
            { "data": "created_timestamp",  "orderable": true },
            { "data": "updated_timestamp",  "orderable": true },
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                     return "<button id='delete_btn-"+full.surcharge_id+"' class='btn btn-danger btn-xs' style='margin:2.5px;' onclick='deleteSurcharge(this)'><i class='fa fa-trash'></i></button>";
                 }
            },
        ],
        buttons: [
            {
                extend:     'print',
                text:       '<i class="fas fa-print"></i>',
                titleAttr:  'Print',
                className:  'btn btn-primary btn-md',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                },
                orientation: 'landscape',
            },   
            {
                extend:    'copyHtml5',
                text:      '<i class="fas fa-copy"></i>',
                titleAttr: 'Copy',
                className: 'btn btn-primary btn-md',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                },
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-primary btn-md',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                },
                
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fas fa-file-csv"></i>',
                titleAttr: 'CSV',
                className: 'btn btn-primary btn-md',
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                },
                
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fas fa-file-pdf"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-primary btn-md',
                orientation: 'landscape',
                
                exportOptions: {
                    columns: [1,2,3,4,5,6]
                },
                pageSize: 'A3' 
            },
        ],
        language: { 
                search: '', 
                searchPlaceholder: "Search...",
                lengthMenu: "_MENU_",
        }
    } );
    
    table.on('dblclick', 'td', function (){
        var rowIdx = table.cell(this).index().row;
        var colIdx = table.cell(this).index().column;
        var colData = table.cell(this).data();
        var id = table.row(rowIdx).id();
        
        if(!editableArray.includes(id+"-"+colIdx)){
            editableArray.push(id+"-"+colIdx);
            switch (colIdx) {
                case 1:
                    var surcharge_category_id = table.row(rowIdx).data().surcharge_category_id;
                    table.cell(this).data( '<select class="form-control js-example-basic-single" id="surcharge_category-'+id+'" data-row="'+rowIdx+'" data-column="'+colIdx+'" style="width:100%;">'+surcharge_categories_opt+'</select>');
                    $('#surcharge_category-'+id).select2().val(surcharge_category_id).trigger("change");
                    $('#surcharge_category-'+id).trigger("focus");
                    break;
                case 2:
                    table.cell(this).data('<input type="text" class="form-control form-input" id="surcharge_name-'+id+'"  data-row="'+rowIdx+'" data-column="'+colIdx+'" data-old="'+colData+'" style="width:100%; height:30px;" value="'+colData+'"/>');
                    $("#surcharge_name-"+id).autocomplete({
                        source: surcharge_names_array,
                        matchContains: true,
                        minChars: 0,
                    });
                    $("#surcharge_name-"+id).trigger("focus");
                    break;
                case 3:
                    table.cell(this).data('<select class="form-control js-example-basic-single" id="surcharge_curr-'+id+'" data-row="'+rowIdx+'" data-column="'+colIdx+'"  style="width:100%;">'+surcharge_curr_opt+'</select>');
                    $('#surcharge_curr-'+id).select2().val(colData).trigger("change");
                    $('#surcharge_curr-'+id).trigger("focus");
                    break;
                case 4:
                    table.cell(this).data('<input type="number" class="form-control form-control form-input" id="surcharge_value-'+id+'"  data-row="'+rowIdx+'" data-column="'+colIdx+'" style="width:100%; height:30px;" value="'+colData+'"/>');
                    $('#surcharge_value-'+id).trigger("focus");
                    break;
                case 5:
                    var surcharge_unit_id = table.row(rowIdx).data().surcharge_unit_id;
                    table.cell(this).data('<select class="form-control js-example-basic-single" id="surcharge_unit-'+id+'" data-row="'+rowIdx+'" data-column="'+colIdx+'"  style="width:100%;">'+surcharge_unit_opt+'</select>');
                    $('#surcharge_unit-'+id).select2().val(surcharge_unit_id).trigger("change");
                    $('#surcharge_unit-'+id).trigger("focus");
            }
        }
    });
    
    $('body table').click(function(){
        if($('#table-mh-admin-surcharge-index .form-control').length > 0 && !$(event.target).hasClass('form-control') && !$(event.target).hasClass('select2') && !$(event.target).hasClass('selection') && !$(event.target).hasClass('select2-selection') && !$(event.target).hasClass('select2-selection__rendered') && !$(event.target).hasClass('select2-selection__arrow') && !$(event.target).hasClass('dropdown-wrapper') && !$(event.target).hasClass('select2-results__option') && !$(event.target).hasClass('select2-results__option') && !$(event.target).hasClass('select2-search__field') && !$(event.target).closest('select2').length > 0){
            var table = $('#table-mh-admin-surcharge-index').DataTable();
            $('#table-mh-admin-surcharge-index .form-control').each(function(){
                var isDirty = null;
                var id = $(this).attr('id').split("-")[1];
                var columnName = $(this).attr('id').split("-")[0];
                var rowIdx = $(this).data('row'); 
                var colIdx = $(this).data('column');
                if(editableArray.includes(id+"-"+colIdx)){
                    var updateData = {}; 
                    switch (colIdx) {
                        case 1:
                          var surcharge_category = $('#surcharge_category-'+id).select2('data')[0].text;
                          updateData['surcharge_category_id'] = $('#surcharge_category-'+id).val();
                          table.cell(rowIdx, colIdx).data(surcharge_category);
                          table.row(rowIdx).data().surcharge_category_id = updateData['surcharge_category_id'];
                          break;
                        case 2:
                            updateData['surcharge_name'] = $('#surcharge_name-'+id).val();
                            if(updateData['surcharge_name'] == ""){ 
                                isDirty = "Surcharge Name Should Not Be Empty";
                                table.cell(rowIdx, colIdx).data($('#surcharge_name-'+id).attr("data-old"));
                            } else if(! /^[a-zA-Z\s]+$/.test(updateData['surcharge_name'])){ 
                                isDirty = "Surcharge Name Should Be Valid String";
                                table.cell(rowIdx, colIdx).data($('#surcharge_name-'+id).attr("data-old"));
                            } else if(updateData['surcharge_name'].length > 50){ 
                                isDirty = "Surcharge Name Should Be Of Maximum 50 Alphabates";
                                table.cell(rowIdx, colIdx).data($('#surcharge_name-'+id).attr("data-old"));
                            } else {
                                table.cell(rowIdx, colIdx).data($('#surcharge_name-'+id).val());
                            }
                            break;
                        case 3:
                            updateData['surcharge_curr'] = $('#surcharge_curr-'+id).val();
                            table.cell(rowIdx, colIdx).data($('#surcharge_curr-'+id).val());
                            break;
                        case 4:
                            updateData['surcharge_value'] = $('#surcharge_value-'+id).val();
                            table.cell(rowIdx, colIdx).data($('#surcharge_value-'+id).val());
                            break;
                        case 5:
                            var surcharge_unit = $('#surcharge_unit-'+id).select2('data')[0].text;
                            updateData['surcharge_unit_id'] = $('#surcharge_unit-'+id).val();
                            table.cell(rowIdx, colIdx).data(surcharge_unit);
                            table.row(rowIdx).data().surcharge_unit_id = updateData['surcharge_unit_id'];
                    }
                    updateData['surcharge_id'] = id;
                    if(isDirty == null){
                        updateSurcharge(updateData, function(time){
                            table.cell(rowIdx, 7).data(time);
                        });
                    } else {
                        alert(isDirty);
                    }
                    editableArray.splice(editableArray.indexOf(id+"-"+colIdx),1);
                }
            })
        }
    });
    
    function updateSurcharge(data, callback){
        $.post('https://www.movinghaus.net/Surcharge_admin/update_surcharge', data).done(function(response){
            response = JSON.parse(response);
            if(response.status != 1){
                alert("Surcharge Not Updated");
            } else {
                callback(response.time);
            }
        });
    }
    
    
   
});
     
     
function deleteSurcharge(thatElement){
    var table = $('#table-mh-admin-surcharge-index').DataTable();
    var id = $(thatElement).attr('id').split("-")[1];
    if(id == 'newsurcharge'){
        newSurchargeForm = 0;
        table.row($(thatElement).parents('tr')).remove().draw(false);
    } else {
        if(confirm("Are you sure you want to delete this items?")){
            $.post('https://www.movinghaus.net/Surcharge_admin/delete_surcharge', {surcharge_id : id});
            table.row($(thatElement).parents('tr')).remove().draw(false);
        }
    }
}

function addNewSurcharge(){
    if(newSurchargeForm == 0){
        var id = 'newsurcharge';
        var table = $('#table-mh-admin-surcharge-index').DataTable();
        var newRow = table.row.add({
            created_timestamp: "",
            surcharge_category_id: "",
            surcharge_category_name: "",
            surcharge_curr: "",
            surcharge_id: id,
            surcharge_name: "",
            surcharge_unit_id: "",
            surcharge_value: "",
            unit_name: "",
            updated_timestamp: ""
        }).draw( false );
        var newRowIndex = newRow.index();
        var newSurchargeData = table.row(newRowIndex).data();
        table.cell(newRowIndex,1).data('<select class="form-control js-example-basic-single" id="surcharge_category-'+id+'" data-row="'+newRowIndex+'" data-column="'+2+'" style="width:100%;">'+mainData['surcharge_categories_opt']+'</select>');
        $('#surcharge_category-'+id).select2({placeholder: "Select a category"}).val(null).trigger('change');
        table.cell(newRowIndex,2).data('<input type="text" class="form-control form-input" id="surcharge_name-'+id+'"  data-row="'+newRowIndex+'" data-column="'+3+'" style="width:100%; height:30px;" placeholder="name"/>');
        $("#surcharge_name-"+id).autocomplete({
          source: mainData['surcharge_names_array']
        });
        table.cell(newRowIndex,3).data('<select class="form-control js-example-basic-single" id="surcharge_curr-'+id+'" data-row="'+newRowIndex+'" data-column="'+4+'"  style="width:100%;">'+mainData['surcharge_curr_opt']+'</select>');
        $("#surcharge_curr-"+id+" option").filter(function() {
            return this.text == 'USD'; 
        }).attr('selected', true);
        $("#surcharge_curr-"+id).select2().trigger('change');
        table.cell(newRowIndex,4).data('<input type="number" class="form-control form-control form-input" id="surcharge_value-'+id+'"  data-row="'+newRowIndex+'" data-column="'+5+'" style="width:100%; height:30px;" placeholder="value"/>');
        table.cell(newRowIndex,5).data('<select class="form-control js-example-basic-single" id="surcharge_unit-'+id+'" data-row="'+newRowIndex+'" data-column="'+6+'"  style="width:100%;">'+mainData['surcharge_unit_opt']+'</select>');
        $("#surcharge_unit-"+id+" option").filter(function() {
            return this.text == 'FT'; 
        }).attr('selected', true);
        $("#surcharge_unit-"+id).select2().trigger('change');
        table.cell(newRowIndex,6).data("<button id='saveNewFormBtn-"+id+"' class='btn btn-success btn-xs' style='margin:2.5px;'><i class='fa fa-check'></i> &nbsp; Save Surcharge</button>");
        table.cell(newRowIndex,7).data("<button id='cancel_btn-"+id+"' class='btn btn-warning btn-xs' style='margin:2.5px;'><i class='fa fa-times'></i> &nbsp; Cancel</button>");
        
        $("#saveNewFormBtn-"+id).click(function(){
            saveNewForm(id,function(response, newSurchargeData){
               var newSurcharge = newSurchargeData;
               newSurcharge['created_timestamp'] = response.time;
               newSurcharge['updated_timestamp'] = response.time;
               newSurcharge['surcharge_id'] = response.status;
               table.row($('#saveNewFormBtn-'+id).parents('tr')).remove().draw(false);
               table.row.add(newSurcharge).draw( false );
               newSurchargeForm = 0;   
            });
        })
        $("#cancel_btn-"+id).click(function(){
            deleteSurcharge(this);
        })
        newSurchargeForm = 1;   
    }
}

function saveNewForm(id, callback){
    var isDirty = null; 
    if($('#surcharge_unit-'+id).val() != "" && $('#surcharge_category-'+id).val() != ""){
        if($('#surcharge_name-'+id).val() == ""){ 
            isDirty = "Surcharge Name Should Not Be Empty";
        } else if(! /^[a-zA-Z\s]+$/.test($('#surcharge_name-'+id).val())){ 
            isDirty = "Surcharge Name Should Be Valid String";
        } else if($('#surcharge_name-'+id).val().length > 50){ 
            isDirty = "Surcharge Name Should Be Of Maximum 50 Alphabates";
        } else {
            var surcharge_unit = $('#surcharge_unit-'+id).select2('data')
            var surcharge_category = $('#surcharge_category-'+id).select2('data')
        
            var newSurchargeData = {
               surcharge_curr : $('#surcharge_curr-'+id).val(),
               surcharge_name : $('#surcharge_name-'+id).val(),
               surcharge_unit_id : $('#surcharge_unit-'+id).val(),
               surcharge_value : $('#surcharge_value-'+id).val(),
               surcharge_category_id : $('#surcharge_category-'+id).val(),
               surcharge_category_name : surcharge_category[0].text,
               unit_name : surcharge_unit[0].text,
            }
            
            $.post('https://www.movinghaus.net/Surcharge_admin/save_surcharge', newSurchargeData).done(function(response){
                response = JSON.parse(response);
                if(response.status != 0){
                    callback(response, newSurchargeData);
                } else {
                    alert("Surcharge Not Created");
                }
            });
        }
    } else {
        isDirty = "Surcharge unit and category should not be empty";
    }
    
    if (isDirty !== null) {
        alert(isDirty);
    }
}


function deleteManySurcharge(){
    if(confirm("Are you sure you want to delete many items?")){
        var table = $('#table-mh-admin-surcharge-index').DataTable();
        var values = $("input[name='surcharges_check[]']:checked").map(function(){return $(this).val();}).get();
        if(values.length > 0){
            $("input[name='surcharges_check[]']:checked").each(function(){
                table.row($(this).parents('tr')).remove().draw(false);
            })
            $.post('https://www.movinghaus.net/Surcharge_admin/delete_surcharges_many', {surcharge_ids : values});
        }
    }
}
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
**/