$(document).ready(function() {
    
    // configure datatables
    var table = $('#table1').DataTable( {
    scrollX:    true,
    select:     true,
    select:     {
                    style:    'os',
                },
        
    // read data into object
    columns: [
        
        { // column 0  
            "data":             "from_to_currency",
        },
        
        { // column 1  
            "data":         "hourly rate",
          
        },
        
        {   // column 2
            "data":         "update"
        },
    ], 
     
    // remember the state of the table
    stateSave: false,
    
    order: [
        [ 0, "dsc" ]
    ],
     
    pagingType: "full_numbers",
    
    infoEmpty: "No records available",
     
    dom :
        "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
    buttons: [
 
        {
            extend:    'copyHtml5',
            text:      '<i class="fas fa-copy"></i>',
            titleAttr: 'Copy',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fas fa-file-excel"></i>',
            titleAttr: 'Excel',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
            
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fas fa-file-csv"></i>',
            titleAttr: 'CSV',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
            
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fas fa-file-pdf"></i>',
            titleAttr: 'PDF',
            className: 'btn btn-primary btn-md',
            orientation: 'landscape',
            
            exportOptions: {
                columns: [0,1,2]
            },
            pageSize: 'A3' 
        },
    ],
    language: { 
            search: '', 
            searchPlaceholder: "Search...",
            lengthMenu: "_MENU_",
    },
} ); // end of datatables declaration





// configure datatables
    var table = $('#table2').DataTable( {
    scrollX:    true,
    select:     true,
    select:     {
                    style:    'os',
                },
        
    // read data into object
    columns: [
        
        { // column 0  
            "data":             "from_to_currency",
        },
        
        { // column 1  
            "data":         "rateindb",
          
        },
        
        {   // column 2
            "data":         "update"
        },
    ], 
     
    // remember the state of the table
    stateSave: false,
    
    order: [
        [ 0, "dsc" ]
    ],
     
    pagingType: "full_numbers",
    
    infoEmpty: "No records available",
     
    dom :
        "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        
    buttons: [
  
        {
            extend:    'copyHtml5',
            text:      '<i class="fas fa-copy"></i>',
            titleAttr: 'Copy',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
        },
        {
            extend:    'excelHtml5',
            text:      '<i class="fas fa-file-excel"></i>',
            titleAttr: 'Excel',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
            
        },
        {
            extend:    'csvHtml5',
            text:      '<i class="fas fa-file-csv"></i>',
            titleAttr: 'CSV',
            className: 'btn btn-primary btn-md',
            exportOptions: {
                columns: [0,1,2]
            },
            
        },
        {
            extend:    'pdfHtml5',
            text:      '<i class="fas fa-file-pdf"></i>',
            titleAttr: 'PDF',
            className: 'btn btn-primary btn-md',
            orientation: 'landscape',
            
            exportOptions: {
                columns: [0,1,2]
            },
            pageSize: 'A3' 
        },
    ],
    language: { 
            search: '', 
            searchPlaceholder: "Search...",
            lengthMenu: "_MENU_",
    },
} ); // end of datatables declaration




  
  
}); // end doc ready
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
B - btton
**/
