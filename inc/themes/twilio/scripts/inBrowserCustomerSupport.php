<?php
    header("content-type: text/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>

<Response>
    <?php if ($_GET['option'] == "billing") { ?>
      <Say>We'll now connect you to our billing team.</Say>
    <?php } elseif ($_GET['option'] == "tech") { ?>
      <Say>We'll now connect you to our technical support team.</Say>
    <?php } elseif ($_GET['option'] == "sales") { ?>
      <Say>We'll now connect you to our sales team.</Say>
    <?php } else { ?> 
      <Say>We didn't recognise your option, so we'll put you through to our switchboard.</Say>
    <?php } ?>

    <Play>http://com.twilio.sounds.music.s3.amazonaws.com/ClockworkWaltz.mp3</Play>
    <Hangup />
</Response>
