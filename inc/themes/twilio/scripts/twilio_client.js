Twilio.Device.setup("<?php echo $token; ?>");

Twilio.Device.ready(function (device) {
// handle device ready
});

Twilio.Device.error(function (error) {
// handle device error
});

Twilio.Device.connect(function (conn) {
// handle device connect
});

Twilio.Device.disconnect(function (conn) {
// handle device disconnect
});

function call() {
// make call
Twilio.Device.connect();
}

function hangup() {
Twilio.Device.disconnectAll();
}
