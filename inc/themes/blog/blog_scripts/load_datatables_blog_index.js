$(document).ready(function() {
    $('#table-mh-admin-blog').DataTable( {
    dom : '<Blfr<t>ipl>',
          buttons: [
              'copy', 'excel', 'pdf', 'print'
          ],
        select: true,
                "order": [[ 4, "asc" ]],
        stateSave: true,
    } );
} );
/**    dom : '<Blfr<t>ipl>',
Read about the DOM here:
https://datatables.net/reference/option/dom
l - length changing input control
f - filtering input
t - The table!
i - Table information summary
p - pagination control
r - processing display element
**/