<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;  // get a capability token
use Twilio\Twiml;
use Illuminate\Http\Request;
use App\Http\Requests;
use Twilio\TwiML\VoiceResponse;
//use Twilio\Twiml;

class MH_twilio_public extends MH_Public_Controller  {

    function __construct() 
    {
        parent::__construct();
        
        // load the xml helper
        $this->load->helper('xml');
        
        // Define the module name here
        $this->module_system_name   = 'MH_twilio';
        $this->module_view_name     = 'Twilio'; 
        //$this->model_name           = $this->module_system_name.'_model';
        
        // set the template
        $this->mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];

        //$config['mh_twilio_enable']             = TRUE; 

        $this->mh_twilio_phone_number_jp    = $this->config->item('mh_twilio_phone_number_jp');    
        $this->mh_twilio_account_sid_jp     = $this->config->item('mh_twilio_account_sid_jp');    
        $this->mh_twilio_auth_token_jp      = $this->config->item('mh_twilio_auth_token_jp');      
    }  
  
    // customer clicks in browser to call.
    function inBrowserCustomerSupport(){
        // add datatables theme/css
        $this->data['mh_theme_public']       .=  '<!-- MH Public Theme -->
                                            <link rel="stylesheet" type="text/css" href="/inc/themes/twilio/css/client.css">';
                                            
        $accountSid = $this->mh_twilio_account_sid_jp ;
        $authToken  = $this->mh_twilio_auth_token_jp ;
 
        $capability = new ClientToken($accountSid, $authToken);
    
        // application sid
        $capability->allowClientOutgoing('APa5dbcfdbb80f4eea8c2247b9fc2dae46');
        
        $this->data['token'] = $capability->generateToken();
   
        // in browser customer support
        $token = $capability->generateToken();

        // Set the titles
        $this->data['mh_public_title']      = $this->data['mh_title_public'] .' | Call Support - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_public_view_file']       = $this->module_system_name .'/public/'.$this->module_system_name .'-public-inBrowserCustomerSupport';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }   

    function twmil_incomingCallToBrowser(){
        
        //echo 'header(\'Content-type: text/html\')';
        echo '  <Repsonse>
                    <Dial>
                        <Client>movinghausBrowserSupport1</Client>
                    </Dial>
                </Repsonse>
        
        ';
        
    }
    
    
}
  
 