<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class MH_auth_public extends MH_Public_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		$this->load->model('MH_auth_public_model');
	}


/**
	 * Log the user in
	 */
	public function login() {
		$this->data['title'] = $this->lang->line('login_heading');

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool)$this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('/', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			// set the title 
    		$this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Login!';
    
    		// get the view file
    		$this->data['mh_public_view_file']      = 'mh-login/mh-login';
    
    		// set the template
    		$mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    		
    		// send all data to the view
    		//$this->load->view($mh_template, $this->data);
    		$this->_render_page($mh_template, $this->data);
    
			//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
		}
	}



	/**
	 * Create a new user
	 */
	public function register_user() {
		//	this is used with phone numbers.
		//	# We load the helper
		$this->load->helper('flags');
	
		//	# Then we echo the dropdown. The first value is the name of the dropdown and the second value is the default value.
		//	echo select_countries('flags', 'pa');

		
		$tables 						= $this->config->item('tables', 'ion_auth');
		$identity_column				= $this->config->item('identity', 'ion_auth');
		$this->data['identity_column']	= $identity_column;

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
		
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
		}
		
		//	$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
		
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'callback_phonenumber_check');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');


		$this->form_validation->set_rules('moving_from','Moving from should not be empty! ', 'required');
		$this->form_validation->set_rules('moving_to','Moving to should not be empty! ', 'required');
		$this->form_validation->set_rules('expected_month','Month should not be empty! ', 'required|callback_validate_expected_month');
		$this->form_validation->set_rules('expected_year','Year should not be empty! ', 'required');
		$this->form_validation->set_message('validate_expected_month','Please select valid month for the year');

		

		if ($this->form_validation->run() === TRUE){
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$pipeline_stage_date  = date("Y-m-d H:i:s"); 
			
			$additional_data = array(
				'first_name'			=> $this->input->post('first_name'),
				'last_name' 			=> $this->input->post('last_name'),
				'company'				=> $this->input->post('company'),

				'fk_loading_zones' 		=> $this->input->post('moving_from'),
				'fk_rates' 				=> $this->input->post('moving_to'),
				'expected_date' 		=> date('d M Y', strtotime($this->input->post('expected_month').' 01 '.$this->input->post('expected_year'))),
				
				'phone' 				=> $this->phonenumber_make(),
				
				'pipeline_stage_date'	=> $pipeline_stage_date,
				'pipeline_stage'		=> '1', // start as a lead
			);
		}
		
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data))
		{
			// check to see if we are creating the user
			// redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("MH_auth_public/login", 'refresh');
		}
		else
		{
			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'	=> 'first_name',
				'id'	=> 'first_name',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'	=> 'last_name',
				'id'	=> 'last_name',
				'type'	=> 'text',
				'value'	=> $this->form_validation->set_value('last_name'),
			);
			$this->data['identity'] = array(
				'name'	=> 'identity',
				'id'	=> 'identity',
				'type'	=> 'text',
				'value'	=> $this->form_validation->set_value('identity'),
			);
			$this->data['email'] = array(
				'name'	=> 'email',
				'id'	=> 'email',
				'type'	=> 'text',
				'value'	=> $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name'	=> 'company',
				'id'	=> 'company',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('company'),
			);


			$this->data['moving_from'] = array(
				'name'	=> 'moving_from',
				'id'	=> 'moving_from',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('moving_from'),
			);
			$this->data['moving_to'] = array(
				'name'	=> 'moving_to',
				'id'	=> 'moving_to',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('moving_to'),
			);
			$this->data['expected_month'] = array(
				'name'	=> 'expected_month',
				'id'	=> 'expected_month',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('expected_month'),
			);
			$this->data['expected_year'] = array(
				'name'	=> 'expected_year',
				'id'	=> 'expected_year',
				'type'	=> 'text',
				'value' => $this->form_validation->set_value('expected_year'),
			);

			$this->data['phone'] = array(
				'name'	=> 'phone',
				'id'	=> 'phone',
				'type'	=> 'text',
				'value'	=> $this->form_validation->set_value('phone'),
			);
			$this->data['password'] = array(
				'name'	=> 'password',
				'id'	=> 'password',
				'type'	=> 'password',
				'value'	=> $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'	=> 'password_confirm',
				'id'	=> 'password_confirm',
				'type'	=> 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			// set the title 
    		$this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Register for free!';
    
    		// get the view file
    		$this->data['mh_public_view_file']      = 'auth/public/create_user_public';
    
	        $this->data['mh_scripts_public']    	.=  '<!-- MH Scripts Public -->
	                									<script src="https://cdn.jsdelivr.net/npm/places.js@1.16.4"></script>';
    		// set the template
    		$mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];


    		$this->data['loadingZones'] 			= $this->MH_auth_public_model->get_loading_zones();
    		$this->data['movingTo'] 				= $this->MH_auth_public_model->get_rate_to();
    	
    		// send all data to the view
    		$this->_render_page($mh_template, $this->data);
		}
	}

	/**
	* Check to see if the phone number is valid or not.
	*/
	
	public function phonenumber_check($str){
		
		$lowercase_country = $this->input->post('flags');
		$uppercase_country = strtoupper($lowercase_country);
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		try{
			$phoneNumber	= $phoneUtil->parse($str, $uppercase_country);
			$isValid = $phoneUtil->isValidNumber($phoneNumber);
			//echo $phoneUtil->format($phoneNumber, \libphonenumber\PhoneNumberFormat::E164);
			if ($isValid==TRUE){
				return TRUE;
			}else{
				$this->form_validation->set_message('phonenumber_check','The Phone Number is not correct.');
				return FALSE;
			}	
		}catch( \libphonenumber\NumberParseException $e){
			$this->form_validation->set_message('phonenumber_check','The Phone Number is not correct.');
			return FALSE;
		}
	}


	public function validate_expected_month(){
		$year = $this->input->post('expected_year'); 
		$month = $this->input->post('expected_month');

		if (date('Y') > date('Y', strtotime("1 ".$month." ".$year))) {
		 	return false;
		} else if (date('Y') == date('Y', strtotime("1 ".$month." ".$year)) && date('n') > date('n', strtotime("1 ".$month." ".$year))) {
			return false;
		} else {
		 	return true;
		}
	}
	
	public function phonenumber_make(){
		$str = $this->input->post('phone');
		$lowercase_country = $this->input->post('flags');
		$uppercase_country = strtoupper($lowercase_country);
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		try{
			$phoneNumber	= $phoneUtil->parse($str, $uppercase_country);
			$isValid = $phoneUtil->isValidNumber($phoneNumber);
			$eOneSixtyFour =  $phoneUtil->format($phoneNumber, \libphonenumber\PhoneNumberFormat::E164);
			if ($isValid==TRUE){
				return $eOneSixtyFour;;
			}else{
				$this->form_validation->set_message('phonenumber_check','The Phone Number is not correct.');
				return FALSE;
			}	
		}catch( \libphonenumber\NumberParseException $e){
			$this->form_validation->set_message('phonenumber_check','The Phone Number is not correct.');
			return FALSE;
		}
	}
	
	
	 
	/**
	* Redirect a user checking if is admin
	*/
	public function redirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('MH_auth_admin', 'refresh');
		}
		redirect('/', 'refresh');
	}

	/**
	 * Forgot password
	 */
	public function forgot_password()
	{
		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'ion_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity', 'class' => 'form-control'
			);

			if ($this->config->item('identity', 'ion_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			
			// set the title 
    		$this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Forgot Password!';
    
    		// get the view file
    		$this->data['mh_public_view_file']      = 'auth/public/forgot_password_public';
    
    		// set the template
    		$mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    		// send all data to the view
    		$this->_render_page($mh_template, $this->data);
    		
			//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'ion_auth') != 'email')
				{
					$this->ion_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->ion_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("MH_auth_public/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("MH_auth_public/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("MH_auth_public/forgot_password", 'refresh');
			}
		}
	}

	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	/**
	 * Reset password - final step for forgotten password
	 *
	 * @param string|null $code The reset code
	 */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
					'type' => 'password',
					'class' => 'form-control',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
					'type' => 'password',
					'class' => 'form-control',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;


			// set the title 
    		$this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Reset Password!';
    
    		// get the view file
    		$this->data['mh_public_view_file']      = 'auth/public/reset_password_public';
    
    		// set the template
    		$mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    		// send all data to the view
    		$this->_render_page($mh_template, $this->data);
    		
    		
    		
				// render
				//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("MH_auth_public/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('MH_auth_public/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("MH_auth_public/forgot_password", 'refresh');
		}
	}



	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

}
