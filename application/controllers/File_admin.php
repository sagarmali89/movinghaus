<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File_admin extends MH_Admin_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('File_model');
        $this->load->helper('bootstrap_alert');

        $config['upload_path']      = $this->config->item('file_upload_path'); 
        $config['allowed_types']    = $this->config->item('file_allowed_types');
        $config['max_size']         = $this->config->item('file_max_size'); // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
        $config['max_width']        = $this->config->item('file_max_width'); // The maximum width (in pixels) that the file can be. Set to zero for no limit.
        $config['max_height']       = $this->config->item('file_max_height');  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
        $config['remove_spaces']    = $this->config->item('file_remove_spaces');
        $config['encrypt_name']     = $this->config->item('file_encrypt_name');
        $config['overwrite']        = $this->config->item('file_overwrite');
            
        // $config['encrypt_name']   = TRUE;
        $this->load->library('upload', $config);
        
        // load up the validation rules for file Info form
        $this->config->load('mh_file_validate');
    }  
    
    // LIST view of file posts
    function index(){
        $config['base_url']     = site_url("File_admin/index");
        $this->data['files']    = $this->File_model->files_get($slug = FALSE, $per_page = null , $offset = null, $year='', $month='');
    
        // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | File Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-file/admin/mh-admin-file';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // add datatables theme/css
        $this->data['mh_theme_admin']       .=  '<!-- MH Admin Theme -->
                                            <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
                                            <link rel="stylesheet" href="/inc/themes/file/file_css/file_select_color.css">';
        // add datatables
        
        /** Below three files combine many files together for a, typically, faster download.
        |AutoFill Excel-like click and drag copying and filling of data.    v2.3.0
        |Buttons A common framework for user interaction buttons.           v1.5.2
        |   Column visibility End user buttons to control column visibility.v1.5.2
        |   HTML5 export Copy to clipboard and create Excel, PDF and CSV files from the tables data.v1.5.2
        |       JSZip Required for the Excel HTML5 export button.v2.5.0
        |       pdfmake Required for the PDF HTML5 export button.v0.1.36
        |   Print view Button that will display a printable view of the table.v1.5.2
        |   ColReorder Click-and-drag column reordering.v1.5.0
        |   FixedColumns Fix one or more columns to the left or right of a scrolling table.v3.2.5
        |   FixedHeader Sticky header and / or footer for the table.v3.1.4
        |   KeyTable Keyboard navigation of cells in a table, just like a spreadsheet.v2.4.0
        |   Responsive Dynamically show and hide columns based on the browser size.v2.2.2
        |   RowGroup Show similar data grouped together by a custom data point.v1.0.3
        |   RowReorder Click-and-drag reordering of rows.v1.2.4
        |   Scroller Virtual rendering of a scrolling table for large data sets.v1.5.0
        |   Select Adds row, column and cell selection abilities to a table.v1.2.6
        **/
        
        $this->data['mh_scripts_admin']     .= '<!-- MH Scripts Admin -->
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.js"></script>
                                            <script src="/inc/themes/file/file_scripts/load_datatables_file_index.js"></script>';
                                          
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }   
    
    // CREATE
    function file_create() {
       
       // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | File Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-file/admin/mh-admin-file-create';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        $this->data['mh_scripts_admin']       .=    '<!-- MH Scripts Admin -->
                                            <script src="/inc/themes/plugins/bower_components/ckeditor/ckeditor.js"></script>
                                            <script src="/inc/themes/file/file_scripts/load_ckeditor.js"></script>
                                            ';
        
        $this->data['categories']       = $this->File_model->file_get_categories();   

        $this->form_validation->set_rules($this->config->item('validate_file_create'));
        if ($this->form_validation->run('validate_file_create') === FALSE) {
                //  an error here would be an error inserting to the db
                //  or you could echo validation_errors()
        } else {
            $user_id  = $this->ion_auth->user()->row()->id;
            $created_timestamp = date("Y-m-d H:i:s");  
            $submitted_data = array(
                    'user_id'           => $user_id,
                    'title'             => $this->input->post('title'),
                    'body'              => $this->input->post('body'),
                    'slug'              => url_title($this->input->post('title')),
                    'created_timestamp' => $created_timestamp,
                    'category_id'       => $this->input->post('category_id')
                    );
            
            // upload image
           // $config['image_path'] = $this->config->item('image_path'); 
           // $config['upload_path']      = $this->config->item('upload_path'); 
           // $config['allowed_types']    = $this->config->item('allowed_types');
           // $config['max_size']         = $this->config->item('max_size'); // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
           // $config['max_width']        = $this->config->item('max_width'); // The maximum width (in pixels) that the file can be. Set to zero for no limit.
           // $config['max_height']       = $this->config->item('max_height');  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
          //  $config['remove_spaces']    = $this->config->item('remove_spaces');
          //  $config['encrypt_name']     = $this->config->item('encrypt_name');
          //  $config['overwrite']        = $this->config->item('overwrite');
           
           
           
            $this->load->library('upload', $this->config);
            
            // make sure this is not set first
            $file_upload_success = "";
            
            // if there is a file submitted then run this
            if(!empty($_FILES['userfile']['name'])) {
                // if validation fails, run this...
                if (!$this->upload->do_upload()){ 
                    $errors = array(    'message'   => $this->upload->display_errors());
                    $message = array(   'message'   => 'Warning - '.$errors['message'],
                                        'class'     => 'danger',  // must be warning, danger, success or info.
                                     );
                     $this->data['alert']   = bootstrap_alert($message);
                     // will never get submitted so commented this out.
                     // $post_image            = 'noimage.jpg';
                     // $submitted_data        += array('post_image'=> $post_image); 
                     $file_upload_success   = FALSE;
                // passed the validation so run this...
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $post_image                 = $this->upload->data('file_name');
                    
                    // $post_image             = $_FILES['userfile']['name'];
                    // add the name of the file into the $submitted_data string
                    $submitted_data         += array('post_image'=> $post_image); 
                    $file_upload_success    = TRUE;
                }
            }
            
            // run this if no file was submitted or above did not FAIL
            if ($file_upload_success!==FALSE) {
                if ($this->File_model->file_create($submitted_data)) {       
                // if data successfully added to db.
                    $message = array(  'message'     => 'Success - The new file was added!',
                                       'class'       => 'success',  // must be warning, danger, success or info.
                                 );
                    //$this->session->set_flashdata('message', $array_msg ); 
                    $this->data['alert'] = bootstrap_alert($message);
                } else {
                    echo "some error"; 
                }
            }
        }
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }
    
    // READ
    function file_view_single($slug = NULL){
        
        // need to get the file title first!
        $this->data['file']                 = $this->File_model->files_get($slug);
        
        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | File Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-file/admin/mh-admin-file-view-single';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // send all data to the view
        if (empty($this->data['file'])){
            show_404();
        }
                // send all data to the view
        $this->load->view($mh_template, $this->data);
    }  
   
    // UPDATE
    function file_update($slug) {
        // need to get the file title first!
        $data = $this->uri->uri_to_assoc();
        $slug = $data['slug'];
        $this->data['file']                 = $this->File_model->files_get($slug);

        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | File Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-file/admin/mh-admin-file-update';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        $this->data['mh_scripts_admin']     .= '<!-- MH Scripts Admin -->
                                            <script src="/inc/themes/plugins/bower_components/ckeditor/ckeditor.js"></script>
                                            <script src="/inc/themes/file/file_scripts/load_ckeditor.js"></script>';

        $this->data['categories']       = $this->File_model->file_get_categories(); 
      
        // send all data to the view
        if (empty($this->data['file'])){
            show_404();
        }
        
        $this->form_validation->set_rules($this->config->item('validate_file_update')); 
                                                                    
        if ($this->form_validation->run('validate_file_update') === FALSE) {
           
           if ($_POST) {  // only do the below if the form has been posted.
                $errors     = array('message'   => $this->upload->display_errors());
                $message    = array('message'   => 'Warning - '.$errors['message'],
                                    'class'     => 'danger',  // must be warning, danger, success or info.
                                    );
                $this->data['alert']   = bootstrap_alert($message);
                $file_upload_success   = FALSE;     
           }
           
        // validation passed and run this.
        } else { 
                $updated_user_id    = $this->ion_auth->user()->row()->id;
                $updated_timestamp  = date("Y-m-d H:i:s"); 
                $submitted_data     = array(
                    'id'                => $this->input->post('file_id'),
                    'updated_user_id'   => $updated_user_id,
                    'title'             => $this->input->post('title'),
                    'body'              => $this->input->post('body'),
                    /*  Note:
                    | $title = "What's wrong with CSS?";
                    | $url_title = url_title($title);
                    | Produces: Whats-wrong-with-CSS (not the hyphenated title)
                    */
                    'slug'              => url_title($this->input->post('title')),
                    'updated_timestamp' => $updated_timestamp,
                    'category_id'       => $this->input->post('category_id')
                );

                // make sure this is not set first
                $file_upload_success = "";
            
                // if there is a file submitted then run this
                if(!empty($_FILES['userfile']['name'])) {
                    // if validation fails, run this...
                    if (!$this->upload->do_upload()){ 
                        $errors     = array('message'   => $this->upload->display_errors());
                        $message    = array('message'   => 'Warning - '.$errors['message'],
                                            'class'     => 'danger',  // must be warning, danger, success or info.
                                            );
                         $this->data['alert']   = bootstrap_alert($message);
                         $file_upload_success   = FALSE;
                    // passed the validation so run this...
                    } else {
                        $data           = array('upload_data' => $this->upload->data());
                        $post_image     = $this->upload->data('file_name');
                        // add the name of the file into the $submitted_data string
                        $submitted_data += array('post_image'=> $post_image); 
                        $file_upload_success    = TRUE;
                    }
                }
                
                // run this if no file was submitted or above did not FAIL
                if ($file_upload_success!==FALSE) {
                    if ($this->File_model->file_update($submitted_data) !== FALSE) {       
                        $array_msg = array(
                                        'message'     => 'Success - The post was updated',
                                        'class'       => 'success'
                                        );
                        $this->session->set_flashdata('message', $array_msg );
                        redirect('File_admin/index');
                    } else {
                        //echo "some error"; 
                        //echo print_r(validation_errors());
                    }
                }
        }
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }
    
    
  
    // DELETE
    function file_delete() {
        // looks for id in the url and take the var after it.  Eg. /id/1 would be file id 1.
        $data = $this->uri->uri_to_assoc();
        $id = $data['id'];
        $this->File_model->file_delete($id);
        redirect('File_admin');
    }
    
    // method to ensure title is unique on update/ edit. Title is changed
    // to slug with url_title();  Below is used from the validation and 
    // is a CALL BACK method.
    function is_slug_unique_on_update() {      
        //list($title, $file_id) = explode('||', $data);
       // $p_file_id  = $this->input->post('file_id');
        $new_slug    = url_title($this->input->post('title'));
        
        if ( $new_slug == $this->input->post('slug')) {
            // no change in slug so update
            // echo "no change in title";
            return TRUE;
        } elseif ( $new_slug !== $this->input->post('slug')) {
            // new slug
            $result = $this->File_model->is_slug_unique_on_update($new_slug);
            return $result; // returns FALSE if the title is not unique
        }
    }
    
     // takes in the post id and deletes the associative image
    function file_image_delete() {
        $data = $this->uri->uri_to_assoc();
        $id = $data['id'];
        if ( $this->File_model->file_image_delete($id) !== FALSE) {
            $array_msg = array(
                                'message'     => 'Success - The file image was deleted',
                                'class'       => 'success'
                                );
                    
                        $this->session->set_flashdata('message', $array_msg );
                        redirect('File_admin');
        }  
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | File Category Management
    |--------------------------------------------------------------------------
    | file_category_index()     - a list of categories
    | file_category_create()    - create a new category
    | file_category_read()      - not implemented.  No need.
    | file_category_update()    - change the name or description of a category
    | file_category_delete()    - delete a category.
    |
    | file_posts_by_category()  - return an array of posts by category
    */
    
     function file_category_index(){

        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Category Admin!';
        
        // get number of post by category
        //$this->File_model->count_posts_by_category($id)
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-file/admin/mh-admin-file-category';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        $this->data['categories']           = $this->File_model->file_get_categories();

 
        
        // add datatables theme/css
        $this->data['mh_theme_admin']       .=  '<!-- MH Admin Theme -->
                                            <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">';
                                            
         /** Below three files combine many files together for a, typically, faster download.
        |AutoFill Excel-like click and drag copying and filling of data.    v2.3.0
        |Buttons A common framework for user interaction buttons.           v1.5.2
        |   Column visibility End user buttons to control column visibility.v1.5.2
        |   HTML5 export Copy to clipboard and create Excel, PDF and CSV files from the tables data.v1.5.2
        |       JSZip Required for the Excel HTML5 export button.v2.5.0
        |       pdfmake Required for the PDF HTML5 export button.v0.1.36
        |   Print view Button that will display a printable view of the table.v1.5.2
        |   ColReorder Click-and-drag column reordering.v1.5.0
        |   FixedColumns Fix one or more columns to the left or right of a scrolling table.v3.2.5
        |   FixedHeader Sticky header and / or footer for the table.v3.1.4
        |   KeyTable Keyboard navigation of cells in a table, just like a spreadsheet.v2.4.0
        |   Responsive Dynamically show and hide columns based on the browser size.v2.2.2
        |   RowGroup Show similar data grouped together by a custom data point.v1.0.3
        |   RowReorder Click-and-drag reordering of rows.v1.2.4
        |   Scroller Virtual rendering of a scrolling table for large data sets.v1.5.0
        |   Select Adds row, column and cell selection abilities to a table.v1.2.6
        **/
        
        $this->data['mh_scripts_admin']     .= '<!-- MH Scripts Admin -->
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.js"></script>
                                            <script src="/inc/themes/file/file_scripts/load_datatables_file_category_index.js"></script>';
        
        
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }
    
    function file_category_create(){

        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Category Admin!';
        
        // get the view file                                 
        $this->data['mh_admin_view_file']   = 'mh-file/admin/mh-admin-file-category-create';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
         // load up the validation rules for Company Info form
        $this->config->load('mh_file_validate_categories');
        $this->form_validation->set_rules($this->config->item('validate_file_categories'));
       
        if ($this->form_validation->run('mh_file_validate_categories') == FALSE) {
            
            //echo validation_errors();
            // if the validation rules do not pass,
            //  errors will be shown in form automatically
        } else {
             
            // if the validation rules pass: 
            $user_id  = $this->ion_auth->user()->row()->id;
            $created_timestamp = date("Y-m-d H:i:s");  
            $submitted_data = array(
                    'user_id'           => $user_id,
                    'name'              => $this->input->post('name'),
                    'created_timestamp' => $created_timestamp
                    );
            
             if ($this->File_model->file_category_create($submitted_data)) {
                 // if data successfully added to db.
                 $message = array(  'message'     => 'Success - The new category was added!',
                                    'class'       => 'success',  // must be warning, danger, success or info.
                                 );
                 //$this->session->set_flashdata('message', $array_msg ); 
                 $this->data['alert'] = bootstrap_alert($message);
               
             } else {
                //  an error here would be an error inserting to the db
                //  or you could echo validation_errors()
             }
        }
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    } 
    
    
     /*
     * Editing a category
     */
    function file_category_update($id) {   
        // The first parameter lets you set an offset, which defaults to 3 
        // since your URI will normally contain a controller/method pair in 
        // the first and second segments. 
        
        $data = $this->uri->uri_to_assoc(2);

        // throw the id into $id.
        $id = $data['file_category_update']; 
        
        // grab the category data
        $this->data['category']                 = $this->File_model->file_category_name_get($id);
        
        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Category Edit!';
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-file/admin/mh-admin-file-category-update';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
                                            
        // set some rules
        $this->form_validation->set_rules('name', 'Category Name', 'required|min_length[5]|max_length[50]');
 
        if ($this->form_validation->run() == TRUE){
            /** Success **/
            $updated_timestamp  = date("Y-m-d H:i:s");
            
            $new_data = array(
                'user_id'           => $this->ion_auth->get_user_id(), // from logged in ion-auth
                'name'              => $this->input->post('name'),
                'updated_timestamp' => $updated_timestamp,
            );

            

            $update_data = $this->File_model->category_update($id, $new_data);
            
            if ($update_data == true) {
                //pop-up message success
                $this->session->set_flashdata('msg_noti', 'Success - Updated');
                redirect('File_admin/file_category_update/'. $id);
            } else {
                //pop-up message error
                $this->session->set_flashdata('msg_error', 'Error update Member');
                redirect('member/file_category_update/'. $id);
            }

        } else {
        
            /** Fail **/
            $this->session->set_flashdata('msg_error', validation_errors());

        }
        
        $this->data['mh_admin_view_file']       = 'mh-file/admin/mh-admin-file-category-update';
        $this->load->view($mh_template, $this->data);
    } 
    
    
    function file_category_delete(){
        $data = $this->uri->uri_to_assoc();
        $id = $data['id'];
        //echo "date is".$id;
        if ($this->File_model->count_posts_by_category($id) > 0){
            
            
           $this->session->set_flashdata('msg_error', 'Category has files attached.  Cannot delete.'); 
            
           //$this->session->set_flashdata('category_deleted', 'Category has posts attached.  Cannot delete.');
           
           
            redirect('File_admin/file_category_index');
        } else {
            /** success **/
            $this->File_model->file_category_delete($id);
            $this->session->set_flashdata('msg_noti', 'Success - Category Deleted');
            //$this->session->set_flashdata('category_deleted', 'Category has been deleted');
            redirect('File_admin/file_category_index');
        }
    }



    
    function file_posts_by_category($id){
        $this->data['title']            .= $this->File_model->file_category_name_get($id)->name;
        $this->data['category_name']    = $this->File_model->file_category_name_get($id)->name;
        $this->data['view_file']         = 'File/file_view_multi';
        $this->data['headerVersion']    = 'header2';
        $this->data['navBarVersion']    = 'navBar2';
        $this->data['footerVersion']    = 'footer2';
        $this->data['css']              .= '<!-- Admin_Dashboard Index CSS -->
                                           <link rel="stylesheet" href="/themes/main_theme/b3/css/compiled/index.css" type="text/css" media="screen" />';
        $this->data['add_script']       .= '<!-- Admin_Dashboard Scripts -->';
        $this->data['css']              .= '';
        
        $this->data['files']            = $this->File_model->file_posts_by_category($id);
        
        //$this->data['categories']       = $this->File_model->posts_by_category();
        
     $this->load->view('_layouts/main/template1', $this->data);
    }
    
   

}