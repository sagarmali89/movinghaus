<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Surcharge_admin extends MH_Admin_Controller {
    
    function __construct() {
        
        parent::__construct();
        $this->load->model('Surcharge_model');
        
    }  
    

     // LIST view surcharges
    function surcharges_index(){

        // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | Surcharge Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-surcharge/admin/mh-admin-surcharge';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // load up datatables and related scripts
	        $this->data['mh_scripts_admin']     
	                .=  '<!-- MH Scripts Admin -->
	                    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

						<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
						<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
						
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>



	                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
	                    
	                    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
	                    
	                    <script src="/inc/themes/surcharge/surcharge_scripts/load_datatables_surcharge_index.js"></script>
						';
	      	        // add datatables theme/css
	        $this->data['mh_theme_admin']       
	                .=  '<!-- MH Admin Theme -->
	            		 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
	                     <link rel="stylesheet" type="text/css" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	                     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
                        
	                     <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                        
	                     <link rel="stylesheet" href="/inc/themes/auth/css/custom.css">
	                    ';
	     
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }   
    
  
    function surcharge_load_data(){
        
        // resturns a result array to $data
        $data = $this->Surcharge_model->surcharge_load_data();
        
        //foreach($data as $r) {
        //       echo $r->surcharge_id;
        //  }
          
        echo json_encode($data);
    }
    
    
    function get_surcharge_categories(){
        $data = $this->Surcharge_model->get_surcharge_categories();
        $options = "";
        foreach($data as $r) {
            $options .= "<option value='".$r->surcharge_category_id."'>".$r->surcharge_category_name."</option>";
        }
        $result['surcharge_categories_opt'] = $options;
        
        $data = $this->Surcharge_model->get_surcharge_curr();
        $options = "";
        foreach($data as $r) {
            $options .= "<option value='".$r->currencyId."'>".$r->currencyId."</option>";
        }
        $result['surcharge_curr_opt'] = $options;
        
        
        $data = $this->Surcharge_model->get_surcharge_units();
        $options = "";
        foreach($data as $r) {
            $options .= "<option value='".$r->unit_id."'>".$r->unit_name."</option>";
        }
        $result['surcharge_unit_opt'] = $options;
        
        $data = $this->Surcharge_model->get_surcharge_names();
        $options = [];
        foreach($data as $r) {
            array_push($options, $r->surcharge_name);
        }
        $result['surcharge_names_array'] = $options;
        echo json_encode($result);
    }

        
    function update_surcharge(){
        $newSurchargeArray = array();
        
        if($this->input->post('surcharge_curr')){
        	$newSurchargeArray['surcharge_curr'] = $this->input->post('surcharge_curr');
        }
        if($this->input->post('surcharge_name')){
        	$newSurchargeArray['surcharge_name'] = $this->input->post('surcharge_name');
        }
        if($this->input->post('surcharge_unit_id')){
        	$newSurchargeArray['surcharge_unit_id'] = $this->input->post('surcharge_unit_id');
        }
        if($this->input->post('surcharge_value')){
        	$newSurchargeArray['surcharge_value'] = $this->input->post('surcharge_value');
        }
        if($this->input->post('surcharge_category_id')){
        	$newSurchargeArray['surcharge_category_id'] = $this->input->post('surcharge_category_id');
        }  
        
        //$newSurchargeArray['updated_timestamp'] = date("Y-m-d H:i:s");
        
        
        // returns a datetime in gmt
        $mydate = strtotime(date("Y-m-d H:i:s"));
        
        //converts to epoch time in user's timezone.
        $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        
        // creates a human readable time
        $user_timezone_human = unix_to_human($user_timezone_epoch);
        
        $newSurchargeArray['updated_timestamp'] = date("Y-m-d H:i:s", strtotime($user_timezone_human));
        
        $whereID = $this->input->post('surcharge_id');
        
        print_r(json_encode(array(
            'status' => $this->Surcharge_model->update_surcharge($whereID, $newSurchargeArray),
            'time' => $newSurchargeArray['updated_timestamp']
        )));   
    }
  
    function save_surcharge(){
        // returns a datetime in gmt
        $mydate = strtotime(date("Y-m-d H:i:s"));
        
        //converts to epoch time in user's timezone.
        $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        
        // creates a human readable time
        $user_timezone_human = unix_to_human($user_timezone_epoch);
        
        $newSurchargeArray = array(
            'surcharge_curr' => $this->input->post('surcharge_curr'),
            'surcharge_name' => $this->input->post('surcharge_name'),
            'surcharge_unit_id' => $this->input->post('surcharge_unit_id'),
            'surcharge_value' => $this->input->post('surcharge_value'),
            'surcharge_category_id' => $this->input->post('surcharge_category_id'),
            'updated_timestamp' => date("Y-m-d H:i:s", strtotime($user_timezone_human)),
            'created_timestamp' => date("Y-m-d H:i:s", strtotime($user_timezone_human))
        );
        print_r(json_encode(array(
            'status' => $this->Surcharge_model->create_surcharge($newSurchargeArray),
            'time' => $newSurchargeArray['created_timestamp']
        )));
    }
    
    
    function delete_surcharge(){
        $this->Surcharge_model->delete_surcharge($this->input->post('surcharge_id'));
    }
    
    function delete_surcharges_many(){
        $ids = $this->input->post('surcharge_ids'); 
        foreach($ids as $id){
            $this->Surcharge_model->delete_surcharge($id);
        }
    }
    /*
    |--------------------------------------------------------------------------
    | Surcharge Category Management
    |--------------------------------------------------------------------------
    | surcharge_category_index()     - a list of categories
    | surcharge_category_create()    - create a new category
    | surcharge_category_read()      - not implemented.  No need.
    | surcharge_category_update()    - change the name or description of a category
    | surcharge_category_delete()    - delete a category.
    | surcharges_by_category()  - return an array of surcharges by surcharge category
    */
    
     function surcharge_category_index(){

        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Surcharge Category Admin!';
        
        // get number of post by category
        //$this->Faq_model->count_posts_by_category($id)
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-surcharge/admin/mh-admin-surcharge-category';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        $this->data['surcharge_categories']           = $this->Surcharge_model->Surcharge_get_categories();

 
        // add datatables theme/css
        $this->data['mh_theme_admin']       .=  '<!-- MH Admin Theme -->
                                            <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">';
                                            
         /** Below three files combine many files together for a, typically, faster download.
        |AutoFill Excel-like click and drag copying and filling of data.    v2.3.0
        |Buttons A common framework for user interaction buttons.           v1.5.2
        |   Column visibility End user buttons to control column visibility.v1.5.2
        |   HTML5 export Copy to clipboard and create Excel, PDF and CSV files from the tables data.v1.5.2
        |       JSZip Required for the Excel HTML5 export button.v2.5.0
        |       pdfmake Required for the PDF HTML5 export button.v0.1.36
        |   Print view Button that will display a printable view of the table.v1.5.2
        |   ColReorder Click-and-drag column reordering.v1.5.0
        |   FixedColumns Fix one or more columns to the left or right of a scrolling table.v3.2.5
        |   FixedHeader Sticky header and / or footer for the table.v3.1.4
        |   KeyTable Keyboard navigation of cells in a table, just like a spreadsheet.v2.4.0
        |   Responsive Dynamically show and hide columns based on the browser size.v2.2.2
        |   RowGroup Show similar data grouped together by a custom data point.v1.0.3
        |   RowReorder Click-and-drag reordering of rows.v1.2.4
        |   Scroller Virtual rendering of a scrolling table for large data sets.v1.5.0
        |   Select Adds row, column and cell selection abilities to a table.v1.2.6
        **/
        
        $this->data['mh_scripts_admin']     .= '<!-- MH Scripts Admin -->
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
                                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                            <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.js"></script>
                                            <script src="/inc/themes/surcharge/surcharge_scripts/load_datatables_surcharge_category_index.js"></script>';
        
        
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }
    
   function surcharge_category_create(){

        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Category Admin!';
        
        // get the view file                                 
        $this->data['mh_admin_view_file']   = 'mh-surcharge/admin/mh-admin-surcharge-category-create';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
         // load up the validation rules for Company Info form
        $this->config->load('mh_surcharge_validate_categories');
        $this->form_validation->set_rules($this->config->item('validate_surcharge_categories'));
       
        if ($this->form_validation->run('mh_faq_validate_categories') == FALSE) {
            
            //echo validation_errors();
            // if the validation rules do not pass,
            //  errors will be shown in form automatically
        } else {
             
            // if the validation rules pass: 
          
            $created_timestamp = date("Y-m-d H:i:s");  
            $submitted_data = array(
                    'surcharge_category_name'       => $this->input->post('surcharge_category_name'),
                    'surcharge_category_comment'    => $this->input->post('surcharge_category_comment'),
                    'created_timestamp'             => $created_timestamp
                    );
            
             if ($this->Surcharge_model->surcharge_category_create($submitted_data)) {
                 // if data successfully added to db.
                 $message = array(  'message'     => 'Success - The new category was added!',
                                    'class'       => 'success',  // must be warning, danger, success or info.
                                 );
                 //$this->session->set_flashdata('message', $array_msg ); 
                 $this->data['alert'] = bootstrap_alert($message);
               
             } else {
                //  an error here would be an error inserting to the db
                //  or you could echo validation_errors()
             }
        }
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    } 
    
    
     /*
     * Editing a category
     */
    function surcharge_category_update($id) {   
     
     
        $surcharge_category_id = $this->uri->segment(3);

        
        // throw the id into $id.
       // $surcharge_category_id = $data['surcharge_category_update']; 
        
        // grab the category data
        $this->data['surcharge_categories'] = $this->Surcharge_model->surcharge_category_name_get($surcharge_category_id);
         
        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Surcharge Category Edit!';
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-surcharge/admin/mh-admin-surcharge-category-update';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
                                            
        // set some rules
        //$this->form_validation->set_rules('surcharge_category_name', 'Surcharge Category Name', 'required|min_length[5]|max_length[50]');
        
        // set validation rules
        $this->form_validation->set_rules('surcharge_category_name', 'Surcharge Category Name', 'required|min_length[5]|max_length[50]');
        $this->form_validation->set_rules('surcharge_category_comment', 'Surcharge Category Comment', 'max_length[255]');
 
        if ($this->form_validation->run() == TRUE){
            /** Success **/
            $updated_timestamp  = date("Y-m-d H:i:s");
            
            $new_data = array(
                'surcharge_category_name'       => $this->input->post('surcharge_category_name'),
                'surcharge_category_comment'    => $this->input->post('surcharge_category_comment'),   
                'updated_timestamp'             => $updated_timestamp,
            );

            $update_data = $this->Surcharge_model->surcharge_category_update($surcharge_category_id, $new_data);
            
            if ($update_data == true) {
                //pop-up message success
                $this->session->set_flashdata('msg_noti', 'Success - Updated');
                redirect('Surcharge_admin/surcharge_category_update/'. $surcharge_category_id);
            } else {
                //pop-up message error
                $this->session->set_flashdata('msg_error', 'Error update Member');
                redirect('Surcharge_admin/surcharge_category_update/'. $surcharge_category_id);
            }

        } else {
        
            /** Fail **/
            $this->session->set_flashdata('msg_error', validation_errors());

        }
        
        //$this->data['mh_admin_view_file']       = 'mh-faq/admin/mh-admin-faq-category-update';
        $this->load->view($mh_template, $this->data);
    } 
    
    
    function surcharge_category_delete(){
        $data = $this->uri->uri_to_assoc();
        $id = $data['id'];
        //echo "date is".$id;
        if ($this->Surcharge_model->count_surcharges_by_category($id) > 0){
            
            
           $this->session->set_flashdata('msg_error', 'Category has posts attached.  Cannot delete.'); 
            
           //$this->session->set_flashdata('category_deleted', 'Category has posts attached.  Cannot delete.');
           
           
            redirect('Surcharge_admin/surcharge_category_index');
        } else {
            /** success **/
            $this->Surcharge_model->surcharge_category_delete($id);
            $this->session->set_flashdata('msg_noti', 'Success - Category Deleted');
            //$this->session->set_flashdata('category_deleted', 'Category has been deleted');
            redirect('Surcharge_admin/surcharge_category_index');
        }
    }



   

}