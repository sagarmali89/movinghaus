<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

use GuzzleHttp\Client as guzzleClient;




class MH_twilio_fax_admin extends MH_Admin_Controller  {

    function __construct() 
    {
        parent::__construct();
        
        // load the upload library for faxes to be stored on server
        $this->load->library('upload', $this->config);
            
        // Define the module name here
        $this->module_system_name   = 'MH_twilio_fax';
        $this->module_view_name     = 'Twilio'; 
        
        // set the template
        $this->mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
     
    }  
  
    // customer clicks in browser to call.
    function fax_send(){
                        
        // set validation rules for form input
		$this->form_validation->set_rules('fax_to', 'Fax to', 'trim|required');
		$this->form_validation->set_rules('fax_from', 'Fax from', 'trim|required');
	
		if ($this->form_validation->run() === TRUE) {
			$data = array(
				'fax_to' => $this->input->post('fax_to'),
				'fax_from' => $this->input->post('fax_from'),
				'fax_media_url' => 'https://www.twilio.com/docs/documents/25/justthefaxmaam.pdf'
			);
		}
		
		
	    if ($this->form_validation->run() === TRUE && $this->send_dat_fax()) {
        
            // check to see if we are creating the user
			// redirect them back to the admin page
			$this->session->set_flashdata('message', 'a message');
			redirect("MH_auth_admin", 'refresh');
	    } else {
	        
	        // display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            //print_r(validation_errors());
            //exit();
            
	        // Set the titles
            $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Techi Support - '.$this->module_view_name;
        
            // nominate the view file
            $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-fax-send';

            $this->load->view($this->mh_template, $this->data);
	   }

        
        // get the submitted data
       // $user_id  = $this->ion_auth->user()->row()->id;
    //    $created_timestamp = date("Y-m-d H:i:s");  
      //  $submitted_data = array(
        //            'user_id'           => $user_id,
          //          'fax_to'            => $this->input->post('fax_to'),
            //        'fax_from'          => $this->input->post('fax_from'),
              //      'fax_media_url'     => $this->input->post('fax_media_url'),
        //            'created_timestamp' => $created_timestamp,
          //      );
                    


        // print($fax->sid);

    }   
    
    function send_dat_fax(){
        // get the account sid                    
        $mh_twilio_fax_account_sid  = $this->config->item('mh_twilio_fax_account_sid');
        
        // get the auth token for the account
        $mh_twilio_auth_token       = $this->config->item('mh_twilio_auth_token');
        
        // create a new client
        $twilio = new Client($mh_twilio_fax_account_sid, $mh_twilio_auth_token);
        
        
            $fax = $twilio->fax->v1->faxes->create(
                "+818032456954", // to
                "https://www.twilio.com/docs/documents/25/justthefaxmaam.pdf", // mediaUrl
                array("from" => "+815036280634")
            );
            
            print "<pre>";
            print_r($fax);
            print "</pre>";
            exit();
        return $fax;
    }
    
    // fax history
    function fax_history(){
        
        // get the account sid                    
         $mh_twilio_fax_account_sid  = $this->config->item('mh_twilio_fax_account_sid');
        
        // get the auth token for the account
         $mh_twilio_auth_token       = $this->config->item('mh_twilio_auth_token');
        
        // create a new guzzle client
        $guzzleResource = new guzzleClient();
        
        // guzzle up the data
        $res = $guzzleResource->request('GET', 'https://fax.twilio.com/v1/Faxes', [
            'auth' => [$mh_twilio_fax_account_sid, $mh_twilio_auth_token]
        ]);

        // test to see if the right data is returned
        $this->data['status_code']  = $res->getStatusCode();
        $this->data['content_type'] = $res->getHeaderLine('content-type');
        $content                    = $res->getBody()->getContents();
        
        $array = json_decode($content , true);
        
        $this->data['faxes'] = $array['faxes'];
        //$student_id = $array['response']['docs'][0]['student_id'];

        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Fax List - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-fax-history';

        // load up the view
        $this->load->view($this->mh_template, $this->data);
    }
    
    // fax  accounts
    function fax_accounts(){
        
        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Fax Accounts - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-fax-accounts';

        $this->load->view($this->mh_template, $this->data);
        
    }
    
    // fax dashboard
    function fax_dashboard(){
        
        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Fax Dashboard - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-fax-dashboard';

        $this->load->view($this->mh_template, $this->data);
        
    }
    
    
    // fax twilio settings
    function fax_twilio_settings(){
        
        
        
        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Fax Twilio Settings - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-fax-twilio-settings';

        $this->load->view($this->mh_template, $this->data);
        
    }
    
    


}