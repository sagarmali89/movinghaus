<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class MH_staff_admin extends MH_Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language', 'my_date_helper', 'bootstrap_alert'));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
	}

	// pass in group array to limit the list by group number
	// $users = $this->ion_auth->users(array('admin','members'))->result(); // get users from 'admin' and 'members' group
	public function index()
	{
	
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('MH_auth_admin/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			// just show admin and managers
			$this->data['users'] = $users = $this->ion_auth->users(array('admin','Manager'))->result();
 		
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			
			// Set the titles
            $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | List ';
    
			// load up datatables and related scripts
	        $this->data['mh_scripts_admin']     
	                .=  '<!-- MH Scripts Admin -->
						<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
						<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
						
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

						<script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

	                    <script src="/inc/themes/auth/auth_scripts/list_users.js"></script>
	                   
	                    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	                    
	                    <script src="/inc/themes/auth/auth_scripts/enable-disable-user-account.js"></script>
						';
	        
	        // add datatables theme/css
	        $this->data['mh_theme_admin']       
	                .=  '<!-- MH Admin Theme -->
	            		 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
	                     <link rel="stylesheet" type="text/css" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	                     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
	                     
	                     <link rel="stylesheet" href="/inc/themes/auth/css/custom.css">
	                    ';
	     
	       // get the view file
            $this->data['mh_admin_view_file']       = 'auth/admin/index_admin_staff';
        
            $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];

            $this->load->view($mh_template, $this->data);
		}
	}
	


	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

}
