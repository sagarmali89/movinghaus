<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class MH_xrates extends MH_Admin_Controller  {

  function __construct() {
    parent::__construct();
    $this->load->library('ccci' );
    $this->load->model('Ccci_model');
    $this->load->helper(array('bootstrap_alert', 'dropdown_helper'));
    
    $this->currency_array = array('JPY_USD',
                            'USD_JPY',
                            'AUD_USD',
                            'USD_AUD',
                            'CAD_USD',
                            'USD_CAD',
                            'JPY_NZD',
                            'NZD_JPY');
                            
  }  

  public function index() {
    if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
		// set the flash data error message if there is one
		//	$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    
    // theme/css
    $this->data['mh_theme_admin']       
            .=  '<!-- MH Admin Theme -->
                <link rel="stylesheet" type="text/css" href="/inc/themes/ccci/css/ccci.css">';
    
    $this->data['mh_scripts_admin']     
	                .=  '<!-- MH Scripts Admin -->
						<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
						<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
						
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
						<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

						<script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
	         <script src="/inc/themes/xrates/js/table1and2.js"></script>
						';            

    // set the title 
    $this->data['mh_admin_title']          = $this->data['mh_title_admin'] .' | Xrates!';
    
    // get the view file
    $this->data['mh_admin_view_file']      = 'mh-xrates/mh-xrates';
    
    // set the template
    $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];

    $currency_array = $this->currency_array;

    $this->data['ccci_convert_many'] = $this->ccci->ccci_convert_many(1, $currency_array);
    
    // For the currencie above, create an identical array to go
    // grab flags from https://countryflags.io/.  It MUST match above.`
    
    $this->data['currency_flag_array'] = array( 
                                  0 => 'JP_US',
                                  1 => 'US_JP',
                                  2 => 'AU_US',
                                  3 => 'US_AU',
                                  4 => 'CA_US',
                                  5 => 'US_CA',
                                  6 => 'JP_NZ',
                                  7 => 'NZ_JP');
                            
    
    $this->data['ccci_get_xrates'] = $this->Ccci_model->ccci_get_xrates();

    //$this->data['ccci_convert'] = $this->ccci->ccci_convert(200000000000, 'USD', 'JPY');

    $this->data['ccci_usage'] = $this->ccci->ccci_usage();
   // $ccci_country_list = $this->data['ccci_country_list'] = $this->ccci->ccci_country_list();
    
    //$this->Ccci_model->ccci_country_list_update_db($ccci_country_list['results']);
    
    //$this->data['currencylist'] = $this->ccci->ccci_currency_list();
    // send all data to the view
    $this->load->view($mh_template, $this->data);
    }
  }

  public function ccci_country_list_update() {

    // get all the new data
    $ccci_country_list = $this->data['ccci_country_list'] = $this->ccci->ccci_country_list();
    
    //update existing or add new data
    if ($this->Ccci_model->ccci_country_list_update_db($ccci_country_list['results']))
    {
      $message = array( 'message'   => 'Success - Database Updated',
                        'class'     => 'success',  // must be warning, danger, success or info.
                      );
      $this->data['alert'] = bootstrap_alert($message);
      $this->session->set_flashdata('message', $this->data['alert'] );              
    } else {
      $message = array( 'message'   => 'Danger - I am afraid something went wrong!',
                        'class'     => 'danger',  // must be warning, danger, success or info.
                      );
      $this->data['alert'] = bootstrap_alert($message);
      $this->session->set_flashdata('message', $this->data['alert'] ); 
    }
    redirect('/MH_xrates/index');
  }
 
  // update of replace the tracked xrates 
  public function ccci_update_xrates($currency_array = NULL)
  {
    // if no currency_array is supplied, then update all rates 
    // as denoted in the _contruct
    if ($currency_array == NULL){
      $currency_array= $this->currency_array;
    } 
    
   // if(!$this->input->is_cli_request())
   //  {
   //      echo "greet my only be accessed from the command line";
   //      return;
   //  }
     
    // the first argument is the amount.  in this case 1.
    $this->data['ccci_convert_many'] = $this->ccci->ccci_convert_many(1, $currency_array);

    if ($this->Ccci_model->ccci_update_xrates($this->data['ccci_convert_many']))
    {
      $message = array( 'message'   => 'Success - Exchange Rates Database Updated',
                        'class'     => 'success',  // must be warning, danger, success or info.
                      );
      $this->data['alert'] = bootstrap_alert($message);
      $this->session->set_flashdata('message', $this->data['alert'] );              
    } else {
      $message = array( 'message'   => 'Danger - I am afraid something went wrong and xrates were not updated!',
                        'class'     => 'danger',  // must be warning, danger, success or info.
                      );
      $this->data['alert'] = bootstrap_alert($message);
      $this->session->set_flashdata('message', $this->data['alert'] ); 
    }
    redirect('/MH_xrates/index');
  }
  
  
}  // end MH_rates