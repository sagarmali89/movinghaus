<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MH_login_attempts_admin extends MH_Admin_Controller {
    
    function __construct() {
        
        parent::__construct();
   
        // Define the module name here
        $this->module_system_name   = 'MH_login_attempts';
        $this->module_view_name     = 'Login Attempt'; 
        $this->model_name           = $this->module_system_name.'_model';
        
        // set the template
        $this->mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];

        // Add any scripts 
        // $this->data['mh_scripts_admin'] .= '<!-- MH Scripts Admin --> ';
        
        // load up the model
        $this->load->model($this->model_name);
        
        // load the validation library
        $this->load->library('form_validation');
    }
    
   
    /*---------- LIST ----------*/
    
    function index() {
        
        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | List - '.$this->module_view_name;
    
        // load up datatables and related scripts
        $this->data['mh_scripts_admin']     
                .=  '<!-- MH Scripts Admin -->
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.js"></script>
                    <script src="/inc/themes/auth/auth_scripts/login_attempts.js"></script>';
        
        // add datatables theme/css
        $this->data['mh_theme_admin']       
                .=  '<!-- MH Admin Theme -->
                    <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">';
        
        // grab all the data                        
        $model = $this->model_name; 
        $this->data[$this->module_system_name]  = $this->$model->index_all();
        
        // nominate the view file
        $this->data['mh_admin_view_file']       = $this->module_system_name .'/admin/'.$this->module_system_name .'-admin-index';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }
    
    /*---------- CREATE ----------*/
    
    function create() {
        
        // set some rules  required|min_length[5]|max_length[11] // required|min_length[5]|max_length[50
        $this->form_validation->set_rules('ip_address', 'IP Address', 'required');
        $this->form_validation->set_rules('login', 'Login', 'required');
 
        if ($this->form_validation->run() == FALSE){
                $this->data['mh_admin_view_file']       = $this->module_system_name .'/admin/'.$this->module_system_name .'-admin-create';
                $this->load->view($this->mh_template, $this->data);
        } else {
            echo "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        }
        
        // get the view file
       // $this->data['mh_admin_view_file'] = $this->module_system_name .'/admin/'.$this->module_system_name .'-admin-create';
       // $this->load->view($this->mh_template, $this->data);
    }    
    
    function save($value='') {
        
        // set response
        $repsonse = array();
        
        // set some rules  required|min_length[5]|max_length[11] // required|min_length[5]|max_length[50
        $this->form_validation->set_rules('ip_address', 'IP Address', 'required');
        $this->form_validation->set_rules('login', 'Login', 'required');
 
        // if the form is run and val
        
        if ($this->form_validation->run() == FALSE){
        
            $response = array(
                'status'    =>  'danger',
                'data'      =>  validation_errors('<div class="error">', '</div>')
            );
            echo json_encode($response);
            //return FALSE;
        } else {
            
            $this->db->insert('login_attempts', array(
                'ip_address'        => $this->input->post('ip_address'),
                'login'             => $this->input->post('login'),
                'time'              => date('l Y/m/d H:i:s'),
                
            ));
            
            if ($this->db->insert_id()){
                $response = array(
                    'status'    =>  'success',
                    'data'      =>  'Record Added'
                );
                echo json_encode($response);
            } else {
                $response = array(
                    'status'    =>  'danger',
                    'data'      =>  'Error occured while saving data'
                );
                echo json_encode($response);
            }
                
        }
        // get the view file
        $this->data['mh_admin_view_file']       = $this->module_system_name .'/admin/'.$this->module_system_name .'-admin-create';
        $this->load->view($this->mh_template, $this->data);
    }
    
    /*---------- READ ----------*/
    
    function read(){
        

        
        // get the data
       // $this->data[$module_system_name]   = $this->$model->read($id);
        
        // get the view file
        $this->data['mh_admin_view_file']   = $this->module_system_name .'/admin/'.$this->module_system_name .'-admin-read';
        
        // send all data to the view
    //    if (empty($this->data[$this->module_system_name])){
    //        show_404();
    //    }
    
        // send all data to the view
        $this->load->view($this->mh_template, $this->data);
    }  
    

    
    /*---------- UPDATE ----------*/
    
    function update($slug) {
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'mh-blog/admin/mh-admin-blog-update';
        $this->load->view($this->mh_template, $this->data);
    }
    
      
    /*---------- DELETE ----------*/
    
    function delete() {
        // looks for id in the url and take the var after it.  Eg. /id/1 would be blog id 1.
        $data = $this->uri->uri_to_assoc();
        $id = $data['id'];
        if ($this->MH_login_attempts_model->delete($id)) {
            redirect('MH_login_attempts_admin');
        } else {
            echo "fail";
        }
    }
    
}