<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// makes this call from twilio
// https://www.movinghaus.net/secure_controller/MH_twilio/phone_tree

// Use the REST API Client to make requests to the Twilio REST API

use Twilio\Rest\Client; // as twilioClient;
use Twilio\Jwt\ClientToken;  // get a capability token
use Twilio\Twiml;
use Illuminate\Http\Request;
use App\Http\Requests;
use Twilio\TwiML\VoiceResponse;

class MH_twilio extends MH_Public_Controller  {

    function __construct() {
        parent::__construct();
    }  
  
    function index(){
    }
    
    function voice_fallback(){
    }
    
    function say_hello(){
        $response = new VoiceResponse();
        $response->say('Please wait while I connect your call.');
        $response->dial('+815036283822');
        echo $response;
    }
    
    function receive_call(){
      
      // use Twilio\TwiML;
      
      $city         = $_POST['FromState'];
      $From         = $_POST['From'] ;
      //$To           = $_POST['To'] ;
      //$CallStaus    = $_POST['CallStatus'] ;
      //$Direction    = $_POST['Direction'] ;
      //$Country      = $_POST['Country'] ;

      $response = new TwiML();
      
      $response->say("Never gonna give you up, {$city}! You are caling from {$From}", array('voice' => 'alice'));
      $response->play("https://demo.twilio.com/docs/classic.mp3");
      
      echo $response;

      //{{To}}
      //  $response = new VoiceResponse();
     //   $dial = $response->dial('');
     //   $dial->sip('+815036283822@sailfish.sip.us1.twilio.com');
      //  echo $response;
    }
    
    //$reader = new XMLReader(); //Initialize the reader
    //$reader->xml($sxml) or die("File not found"); //open the current xml string
    
    function phone_tree(){
    
    if (isset($_POST['Digits'])){
        if ($_POST['Digits'] == '1') {
            $template = '/Twilio/sales';
            $this->load->view($template, $this->data);
        } 
    } else {
      $template = '/Twilio/phone-tree';
     //$this->data['x'] = 'pig';
      $this->load->view($template, $this->data);
    }
    
}

    function sales(){
        $template = '/Twilio/sales';
        //$this->data['x'] = 'pig';
        $this->load->view($template, $this->data);
    }

    // 
    function make_call_from_browser(){
        $template = '/Twilio/make_call_from_browser';
        $this->data['phoneNumber'] = $_REQUEST['To'];
        $this->load->view($template, $this->data);
    }
    
    
    function make_call(){
      
      // get the account sid                    
      $mh_twilio_voice_account_sid  = $this->config->item('mh_twilio_voice_account_sid');
        
      // get the auth token for the account
      $mh_twilio_auth_token       = $this->config->item('mh_twilio_auth_token');
  
      //$config['mh_twilio_voice_number']       = '+815031345388';

      $twilio = new Client($mh_twilio_voice_account_sid, $mh_twilio_auth_token);

      $call = $twilio->calls->create(
          "+81354191166", // to
          "+815036283822", // from
          array("url" => "http://demo.twilio.com/docs/voice.xml")
      );

      print($call->sid);
      
    }
    
    function receive_call_in_browser(){
                
    }
    
    function capability_token($identity = 'unknown'){
        
              
        // get the account sid                    
        // $accountSid  = $this->config->item('mh_twilio_voice_account_sid');
        $accountSid = 'ACc3dc1eaa284c0d02f554c0b99a6a4fd6';
        
        // get the auth token for the account
        //$authToken       = $this->config->item('mh_twilio_auth_token');
        $authToken = 'bcb9803382b5030a722361593e133ece';
        
        // sailfishapp1  APf9815d0327d14f2fa626e66df6044691
        $appSid         = 'APf9815d0327d14f2fa626e66df6044691';
        $capability     = new ClientToken($accountSid, $authToken);
        
        $capability->allowClientOutgoing($appSid);
        $capability->allowClientIncoming($identity);

        $token = $capability->generateToken();
  
        $json = array(
            "identity"  => $identity,
            "token"     => $token
            );
            
        $payload = json_encode($json);
        $this->data['token'] = $payload;
        $template = '/Twilio/capability_token';
        $this->load->view($template, $this->data);
      
    }
    

}
  
 