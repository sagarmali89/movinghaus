<?php  defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

// Use Guzzle.  As Client is already in use, aliased to guzzle client
use GuzzleHttp\Client as guzzleClient;


class Admin extends MH_Admin_Controller  {

  function __construct() {
    
    parent::__construct();
    $this->load->library('ion_auth');
  }  

  public function index() {
    // ref https://bootsnipp.com/snippets/featured/bootstrap-4-counter
    //  This method uses https://www.twilio.com/docs/usage/api/usage-record
    if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
		// set the flash data error message if there is one
		//	$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    

    // set the title 
    $this->data['mh_admin_title']          = $this->data['mh_title_admin'] .' | Welcome!';
    
    // get the view file
    $this->data['mh_admin_view_file']      = 'mh-admin/mh-admin';
    
    // set the template
    $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];

    

    // load up any extra scripts


    // add datatables theme/css
    $this->data['mh_theme_admin']       
            .=  '<!-- MH Admin Theme -->
                <link rel="stylesheet" type="text/css" href="/inc/themes/mh_app/css/admin_dashboard.css">';

    
		// load up datatables and related scripts
    $this->data['mh_scripts_admin']     
            .=  '<!-- MH Scripts Admin -->
                <script type="text/javascript" src="/inc/themes/mh_app/js/admin_dashboard.js"></script>
                ';
	 
    // Guzzle in some data for the dashboard
    // get the account sid                    
    $mh_twilio_account_sid  = $this->config->item('mh_twilio_account_sid');
    
    // get the auth token for the account
     $mh_twilio_auth_token       = $this->config->item('mh_twilio_auth_token');
    
    /**
     * Fire up guzzle and get all usage and price info
     **/
     
    // create a new guzzle client
    $guzzleResource = new guzzleClient();
    
    // guzzle the total usage / price data
    $total_price = $guzzleResource->request('GET', 
        'https://api.twilio.com/2010-04-01/Accounts/'.$mh_twilio_account_sid.'/Usage/Records/AllTime.json?Category=totalprice', 
        [
          'auth' => [$mh_twilio_account_sid, $mh_twilio_auth_token]
        ]
    );

    // test to see if the right data is returned
    $this->data['status_code']  = $total_price->getStatusCode();
    $this->data['content_type'] = $total_price->getHeaderLine('content-type');
    $content                    = $total_price->getBody()->getContents();
  
    // this is a great resource to access decoded json:
    // https://www.elated.com/articles/json-basics/
    $total_price_records = json_decode( $content );

    // grab the usage data records.
    // For more info, visit https://www.twilio.com/docs/usage/api/usage-record?code-sample=code-one-month-date-range-inbound-calls-only&code-language=PHP&code-sdk-version=5.x
    $this->data['usage_records'] = $total_price_records;
        
    /**
     * Fire up guzzle and get all CALLS info
     **/
     
    // create a new guzzle client
    $guzzleCalls = new guzzleClient();
    
    // guzzle the total usage / price data
    $total_calls = $guzzleCalls->request('GET', 
        'https://api.twilio.com/2010-04-01/Accounts/'.$mh_twilio_account_sid.'/Usage/Records/AllTime.json?Category=calls', 
        [
          'auth' => [$mh_twilio_account_sid, $mh_twilio_auth_token]
        ]
    );

    // test to see if the right data is returned
    $this->data['calls_status_code']  = $total_calls->getStatusCode();
    $this->data['calls_content_type'] = $total_calls->getHeaderLine('content-type');
    $calls_content                    = $total_calls->getBody()->getContents();

    // this is a great resource to access decoded json:
    // https://www.elated.com/articles/json-basics/
    $calls_records = json_decode($calls_content);

    // grab the usage data records.
    // For more info, visit https://www.twilio.com/docs/usage/api/usage-record?code-sample=code-one-month-date-range-inbound-calls-only&code-language=PHP&code-sdk-version=5.x
    $this->data['call_records'] = $calls_records;
  
  
	  // Dashboard data                   
	  $this->data['num_customers']            = $this->db->count_all_results('users');
	  $this->data['num_articles']             = $this->db->count_all_results('posts');
	  
	   
    // send all data to the view
    $this->load->view($mh_template, $this->data);
    
  }

}

}