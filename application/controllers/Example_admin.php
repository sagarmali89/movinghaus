<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Example_admin extends MH_Admin_Controller {
    
    function __construct() {
        parent::__construct();
        
        // load up bootstrap helper for display of any alerts.
        $this->load->helper('bootstrap_alert');
        
        // load up model for controller
        $this->load->model('Example_model');
        
        // if you need to changer the default theme, change below.
        // The templates are located in /views/templates/  For admin use /admin
        // admin is divided into admin/_blocks which contains page parts like header, footer navbar and 
        // _layouts which is how the parts are put together.
        // $this->data['headerVersion']    = 'header2';
        // $this->data['navBarVersion']    = 'navBar2';
        // $this->data['footerVersion']    = 'footer2';
    }  
    
    function example(){

        //get data for view
        $this->data['example']                  = $this->Example_model->Example_get();
    
        // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | Example Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-example/admin/mh-admin-example';
        
        // set the template.  Currenlty set to the standard admin template for backend.
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // add datatables theme/css
        $this->data['mh_theme_admin']           .=  '<!-- MH Example Admin Theme - add any extra css files here.  Store css in /inc/themes/example/css  -->
                                                <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
                                                ';
        // add datatables
        /** Below three files combine many files together for a, typically, faster download.
        |AutoFill Excel-like click and drag copying and filling of data.    v2.3.0
        |Buttons A common framework for user interaction buttons.           v1.5.2
        |   Column visibility End user buttons to control column visibility.v1.5.2
        |   HTML5 export Copy to clipboard and create Excel, PDF and CSV files from the tables data.v1.5.2
        |       JSZip Required for the Excel HTML5 export button.v2.5.0
        |       pdfmake Required for the PDF HTML5 export button.v0.1.36
        |   Print view Button that will display a printable view of the table.v1.5.2
        |   ColReorder Click-and-drag column reordering.v1.5.0
        |   FixedColumns Fix one or more columns to the left or right of a scrolling table.v3.2.5
        |   FixedHeader Sticky header and / or footer for the table.v3.1.4
        |   KeyTable Keyboard navigation of cells in a table, just like a spreadsheet.v2.4.0
        |   Responsive Dynamically show and hide columns based on the browser size.v2.2.2
        |   RowGroup Show similar data grouped together by a custom data point.v1.0.3
        |   RowReorder Click-and-drag reordering of rows.v1.2.4
        |   Scroller Virtual rendering of a scrolling table for large data sets.v1.5.0
        |   Select Adds row, column and cell selection abilities to a table.v1.2.6
        **/
        
        $this->data['mh_scripts_admin']         .= '<!-- MH Scripts Example Admin - add any additional scripts here.  Store in /inc/themes/example/scripts -->
                                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
                                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                                                <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.js"></script>
                                                <script src="/inc/themes/example/example_scripts/load_datatables_example_index.js"></script>';
                        
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }   
    
     // LIST view of example 
    function example_index(){

        // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | Example Admin!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-example/admin/mh-admin-example';
        
        // set the template - currently set to standard admin template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // add additional themes/css here - whatever entered here will get called at the top of the file
        $this->data['mh_theme_admin']       .=  '<!-- MH Example Admin - CSS -->
                                            ';
        // add additional scripts here - whatever entered here will get called at the bottom of the file
        $this->data['mh_scripts_admin']     .= '<!-- MH Example Admin - Scripts -->
                                            ';
                                            
        // send all data to the view
        $this->load->view($mh_template, $this->data);
    }   

}