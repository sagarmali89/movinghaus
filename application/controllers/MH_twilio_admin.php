<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Use the REST API Client to make requests to the Twilio REST API

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;  // get a capability token
use Twilio\Twiml;
use Illuminate\Http\Request;
use App\Http\Requests;
use Twilio\TwiML\VoiceResponse;
//use Twilio\Twiml;

class MH_twilio_admin extends MH_Admin_Controller  {

    function __construct() 
    {
        parent::__construct();
        
        // Define the module name here
        $this->module_system_name   = 'MH_twilio';
        $this->module_view_name     = 'Twilio'; 
        
        // set the template
        $this->mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];

        //$config['mh_twilio_enable']             = TRUE; 

        $this->mh_twilio_phone_number   = $this->config->item('mh_twilio_phone_number');    
        $this->mh_twilio_account_sid    = $this->config->item('mh_twilio_account_sid');    
        $this->mh_twilio_auth_token     = $this->config->item('mh_twilio_auth_token');      
    }  
  
    // customer clicks in browser to call.
    function incomingCallToBrowser(){
        // add datatables theme/css
        $this->data['mh_theme_admin']   .=  '<!-- MH Admin Theme -->
                                             <link rel="stylesheet" type="text/css" href="/inc/themes/twilio/css/client.css">';
                                            
        $accountSid = $this->mh_twilio_account_sid ;
        $authToken  = $this->mh_twilio_auth_token ;
 
        $capability = new ClientToken($accountSid, $authToken);
    
        $appName = 'movinghausBrowserSupport1';  // this is the name of the app to run
        
        // application sid or name
        $capability->allowClientIncoming($appName);
        
        // app sid for movinghaus-browswer-support
        $appSid = 'APf1fd4985e0954fd6b92ebf7955e5d1cc';
        
        $capability->allowClientOutgoing($appSid);
        
        $this->data['token'] = $capability->generateToken();
   
        // in browser customer support
        $token = $capability->generateToken();

        // Set the titles
        $this->data['mh_admin_title']      = $this->data['mh_title_admin'] .' | Techi Support - '.$this->module_view_name;
        
        // nominate the view file
        $this->data['mh_admin_view_file']       = $this->module_system_name .'/admin/'.$this->module_system_name.'-admin-incomingTechSupport';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }   
    
    function twmil_incomingCallToBrowser(){
        
        //echo 'header(\'Content-type: text/html\')';
        echo '  <Repsonse>
                    <Dial>
                        <Client>movinghausBrowserSupport1</Client>
                    </Dial>
                </Repsonse>
        
        ';
        
    }

    function i_am_operator(){
        
        // set the title 
        $this->data['mh_admin_title']       = $this->data['mh_title_admin'] .' | Operator!';
        
        // get the view file
        $this->data['mh_admin_view_file']   = 'MH_twilio_operator/admin/MH_twilio-admin-operator';
        
        // set the template
        $mh_template                        = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // add datatables theme/css
        
                // add datatables theme/css
        $this->data['mh_theme_admin']       .=  '<!-- MH Admin Theme -->
                                             <link rel="stylesheet" href="/inc/themes/twilio/css/makeCall.css">';

        $this->data['mh_scripts_admin']     .= '<!-- MH Scripts Admin -->
                                            <script type="text/javascript" src="//media.twiliocdn.com/sdk/js/client/v1.6/twilio.min.js"></script>
                                            <script type="text/javascript" src="/inc/themes/twilio/scripts/operator.js"></script>';
        // send all data to the view
        $this->load->view($mh_template, $this->data);
        
    }
    
}
  
 


		
		
		
//  echo anchor("MH_twilio/make_call/". $user->id, ' ', ('class="fas fa-edit"'));
//										// echo anchor("auth/edit_user/".$user->id, 'Edit') ;?>