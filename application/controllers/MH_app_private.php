<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MH_app_private extends MH_Private_Controller {
    
    function __construct() {
        
        parent::__construct();
   
        $this->load->helper(array('bootstrap_alert'));
   
        //date_default_timezone_set('GMT');
   
        // Define the module name here
        $this->module_system_name   = 'MH_app';
        $this->module_view_name     = 'Settings'; 
        $this->model_name           = $this->module_system_name.'_model';
        
        // select the appropriate template
        $this->data['mh_private_template'] = 'mh_app_with_sidebar';
        
        // load up datatables and related scripts
        $this->data['mh_scripts_private']     
                .=  '<!-- MH Scripts Private -->
                    <script src="/inc/themes/mh_app/js/mh_app.js"></script>';
        
        // add datatables theme/css
        $this->data['mh_theme_private']       
                .=  '<!-- MH Private Theme -->
                     <link rel="stylesheet" href="/inc/themes/mh_app/css/mh_app.css">';
        
        // set the template
        $this->mh_template = $this->data['mh_private_dir'] . $this->data['mh_private_template'];

    }
    
    function mh_app_dashboard(){
        // Set the titles
        $this->data['mh_private_title']      = $this->data['mh_title_private'] .' | Dashboard - '.$this->module_view_name;
    
        // nominate the view file
        $this->data['mh_private_view_file']       = $this->module_system_name .'/private/'.$this->module_system_name .'-private-dashboard';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }
    
    function mh_app_my_estimates(){
        // Set the titles
        $this->data['mh_private_title']      = $this->data['mh_title_private'] .' | My Estimates - '.$this->module_view_name;
     
        // nominate the view file
        $this->data['mh_private_view_file']       = $this->module_system_name .'/private/'.$this->module_system_name .'-private-my-estimates';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }
   
     function mh_app_received_pda(){
        // Set the titles
        $this->data['mh_private_title']      = $this->data['mh_title_private'] .' | Received PDA - '.$this->module_view_name;
     
        // nominate the view file
        $this->data['mh_private_view_file']       = $this->module_system_name .'/private/'.$this->module_system_name .'-private-received-pda';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }
    
    function mh_app_profile(){
        // Set the titles
        $this->data['mh_private_title']      = $this->data['mh_title_private'] .' | Profile - '.$this->module_view_name;
     
        // nominate the view file
        $this->data['mh_private_view_file']       = $this->module_system_name .'/private/'.$this->module_system_name .'-private-profile';

        // run it all
        $this->load->view($this->mh_template, $this->data);
    }
    
    function mh_app_settings(){
        
        // app settings for logged in user
        $this->data['app_settings'] = $this->MH_user_settings_model->mh_get_configurations();
        
        // Set the titles
        $this->data['mh_private_title']         = $this->data['mh_title_private'] .' | Settings - '.$this->module_view_name;
     
        // nominate the view file
        $this->data['mh_private_view_file']     = $this->module_system_name .'/private/'.$this->module_system_name .'-private-settings';

        // set the form rules
        $this->form_validation->set_rules('timezones', 'timezones', 'required');                                                           
        
        // if the form passes the validation (eg true) then 
        if ($this->form_validation->run() == FALSE) {
            
            $this->load->view($this->mh_template, $this->data);
        
            
        }   else {
            
            $my_time_zone   = $this->input->post('timezones');
            $user_id        = $this->ion_auth->user()->row()->id;
            $setting_insert_or_update = 'user_time_zone';
            $this->MH_user_settings_model->mh_insert_or_update_setting($setting_insert_or_update, $my_time_zone, $user_id);
			$message = array(   'message'   => 'Success - Timezone updated',
                        		'class'     => 'success',  // must be warning, danger, success or info.
                            );
            $this->data['alert'] = bootstrap_alert($message);
            $this->session->set_flashdata('message', $this->data['alert'] );              
            redirect('/MH_app_private/mh_app_settings');
            //$this->load->view($this->mh_template, $this->data); 
        }
    }
    
}
