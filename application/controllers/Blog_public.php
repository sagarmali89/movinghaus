<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_public extends MH_Public_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Blog_model');
    }  

    // list view of blog posts
    function index(){

        // if there is session data, grab it.
        $year       = $this->session->userdata('year');
        $month      = $this->session->userdata('month');
        $num_posts  = $this->session->userdata('num_posts');

        // #DEBUGGING
        // echo 'The year is '.$year.' and the month is '.$month;
        
        // set up some formatting for pagination in CI
        $config['full_tag_open']  = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['num_tag_open']	  = '<li class="page-item">';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = '<li class="page-item active"><a class="page-link">';
        $config['cur_tag_close']  = '</a></li>';
        $config['next_tag_open']  = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['data_page_attr'] = 'class="page-link"';

        // config vars for pagination
        $config['base_url']     = site_url("Blog_public/index");
        
        if ( (!empty($year)) && (!empty($month)) ) {
            
            // if the year and month are set, that means a filter has been 
            // applied.  If applied, get the number of posts to set the 
            // pagination links.
            $config['total_rows']   = $num_posts;
            
        } else {
            
            // if there is no filter, get all the data/rows
            $config['total_rows']   = $this->db->get('posts')->num_rows();
            
        }

        // number of posts per page
        $config['per_page']     = $this->config->item('number_of_posts_per_page');
        
        //num_links is the number of naviation links forward and backward to display
        $config['num_links']    = 10;
        
        // i think this is the offset.
        $offset = $this->uri->segment(3); //$config['uri_segment'];
        
        // initialize pagination
        $this->pagination->initialize($config);

        // this contains the paginaion links like 1,2,3,next,prev etc
        $this->data['links']                = $this->pagination->create_links();

        // get all the blog data
        $this->data['blogs']                = $this->Blog_model->blogs_get(FALSE, $config['per_page'], $offset, $year, $month );
         
        // set the title 
        $this->data['mh_public_title']      = $this->data['mh_title_public'] .' | Blog!';
        
        // get the view file
        $this->data['mh_public_view_file']      = 'mh-blog/public/mh-public-blog';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
        // Get the link data for the archive menu    
        $this->data['link_data'] = $this->Blog_model->blog_archive();
    
        // Get the link data for the category menu
        // not implemented yet 
        //$this->data['link_data_category'] = $this->Blog_model->blog_category();
        
        // load up the view file
        $this->load->view($mh_template, $this->data);
    }   
    
    // READ
    function blog_view_single($slug = NULL){
        
        // need to get the blog title first!
        $this->data['blog']                 = $this->Blog_model->blogs_get($slug);
        
        // set the title 
        $this->data['mh_public_title']       = $this->data['mh_title_public'] .' | Blog Public!';
        
        // get the view file
        $this->data['mh_public_view_file']   = 'mh-blog/public/mh-public-blog-view-single';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
        
        // send all data to the view
        if (empty($this->data['blog'])){
            show_404();
        }
                // send all data to the view
        $this->load->view($mh_template, $this->data);
    }  

    // unset the filters
    function clear_year_month(){
        $this->session->unset_userdata('year');
        $this->session->unset_userdata('month');
        $this->session->unset_userdata('num_posts');
        redirect('Blog_public/', 'index');
    }
        
    function list_blog_month_year(){
        
        // set the session year 
        $year =  $this->uri->segment(3);
        $this->session->set_userdata('year', $year);
        
        // set the session month 
        $month =  $this->uri->segment(4);
        $this->session->set_userdata('month', $month);
        
        $num_posts = $this->uri->segment(5);
        $this->session->set_userdata('num_posts', $num_posts);
        
        redirect('Blog_public/', 'index');
    }
}