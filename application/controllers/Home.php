 <?php 
 
 /**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     07.28.2018
 *
 * Description: Home is used for the home page.  .
 *
 * Requirement:
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/xxx/xxx-xxx  // not implemented
 * @filesource
 */
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MH_Public_Controller  {

  function __construct() {
    
    parent::__construct();
    
  }  

  public function index() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Welcome!';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-home';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
    
  }
  
  public function company_profile() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Company Profile!';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-company_profile';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
  }
   
  public function military() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Military Port Agency';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-military';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
  }
  

  public function commercial() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Commerical Agency';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-commercial';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
  }
  
public function privacy() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Privacy Policy';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-privacy';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
  }
  
public function terms_conditions() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Terms & Conditions';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-home/mh-terms-conditions';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    $this->data['mh_scripts_public']       .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>';
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
  }
  
  
}