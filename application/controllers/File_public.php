<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File_public extends MH_Public_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('File_model');
    }  

    // list view of file files
    function index(){
        
        // if there is session data, grab it.  This will give 
        // you the category number only.
        $file_category       = $this->session->userdata('file_category');
        
        echo 'file_category ...  change the title'.$file_category;
        
        // get the name of the file_category
        if (!empty($file_category)) {
           $this->data['filter_category_name'] = $this->File_model->get_category_name($file_category);
        }
        
        // set up some formatting for pagination in CI
        $config['full_tag_open']  = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['num_tag_open']	  = '<li class="page-item">';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = '<li class="page-item active"><a class="page-link">';
        $config['cur_tag_close']  = '</a></li>';
        $config['next_tag_open']  = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['data_page_attr'] = 'class="page-link"';

        // config vars for pagination
        $config['base_url']     = site_url("File_public/index");
        
        if ( (!empty($year)) && (!empty($month)) ) {
            
            // if the year and month are set, that means a filter has been 
            // applied.  If applied, get the number of file to set the 
            // pagination links.
            $config['total_rows']   = $num_posts;
            
        } else {
            
            // if there is no filter, get all the data/rows
            $config['total_rows']   = $this->db->get('files')->num_rows();
            
        }

        // number of file per page
        $config['per_page']     = $this->config->item('file_number_of_posts_per_page');
        
        //num_links is the number of naviation links forward and backward to display
        $config['num_links']    = 10;
        
        // i think this is the offset.
        $offset = $this->uri->segment(3); //$config['uri_segment'];
        
        // initialize pagination
        $this->pagination->initialize($config);

        // this contains the paginaion links like 1,2,3,next,prev etc
        $this->data['links']                = $this->pagination->create_links();

        // don't use them so just make them blank.
        $year ='';
        $month = '';
        
        // get all the file data
        $this->data['files']                = $this->File_model->files_get(FALSE, $config['per_page'], $offset , $year, $month, $file_category );
         
        // set the title 
        $this->data['mh_public_title']      = $this->data['mh_title_public'] .' | File!';
        
        // get the view file
        $this->data['mh_public_view_file']      = 'mh-file/public/mh-public-file';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
        // Get the link data for the archive menu    
        $this->data['link_data'] = $this->File_model->file_get_categories();
    
        // Get the link data for the category menu
        // not implemented yet 
        //$this->data['link_data_category'] = $this->File_model->file_category();
        
        // load up the view file
        $this->load->view($mh_template, $this->data);
    }   
    
    // unset the filters
    function clear_file(){
        
        // faq is filtered on categories.  By unsetting and redirecting
        // the filter is removed.
        $this->session->unset_userdata('file_category');
        redirect('File_public/', 'index');
        
    }
    
    function list_file_category(){
        
        // set the session year 
        $file_category =  $this->uri->segment(3);
        $this->session->set_userdata('file_category', $file_category);
        
        redirect('File_public/', 'index');
    }
    
    // READ
    function file_view_single($slug = NULL){
        
        // need to get the file title first!
        $this->data['file']                 = $this->File_model->files_get($slug);
        
        // set the title 
        $this->data['mh_public_title']       = $this->data['mh_title_public'] .' | File Public!';
        
        // get the view file
        $this->data['mh_public_view_file']   = 'mh-file/public/mh-public-file-view-single';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
        
        // send all data to the view
        if (empty($this->data['file'])){
            show_404();
        }
                // send all data to the view
        $this->load->view($mh_template, $this->data);
    }  

    // unset the filters
    function clear_year_month(){
        $this->session->unset_userdata('year');
        $this->session->unset_userdata('month');
        $this->session->unset_userdata('num_posts');
        redirect('File_public/', 'index');
    }
        
    function list_file_month_year(){
        
        // set the session year 
        $year =  $this->uri->segment(3);
        $this->session->set_userdata('year', $year);
        
        // set the session month 
        $month =  $this->uri->segment(4);
        $this->session->set_userdata('month', $month);
        
        $num_posts = $this->uri->segment(5);
        $this->session->set_userdata('num_posts', $num_posts);
        
        redirect('File_public/', 'index');
    }
}