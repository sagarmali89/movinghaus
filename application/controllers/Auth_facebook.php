<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     08.26.2018
 *
 * Description: Login for Facebook
 *
 * Requirement:
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/xxx/xxx-xxx  // not implemented
 * @filesource
 */

class Auth_facebook extends MH_Public_Controller  {

  function __construct() {
    
    parent::__construct();
    
  }  

  public function index() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Welcome!';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'Welcome';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
    
  }


	public function facebook() {
		
	  $helper = $this->fb->getRedirectLoginHelper();
	  
	  try {
	    $accessToken = $helper->getAccessToken();
	  } catch(Facebook\Exceptions\FacebookResponseException $e) {
	    // When Graph returns an error
	    echo 'There was an error while trying to login using Facebook: ' . $e->getMessage();
	    exit;
	  } catch(Facebook\Exceptions\FacebookSDKException $e) {
	    // When validation fails or other local issues
	    echo 'Facebook SDK returned an error: ' . $e->getMessage();
	    exit;
	  }
	 
	  if (isset($accessToken)) {
	  	
		$this->fb->setDefaultAccessToken($accessToken);
	    try {
	      $response = $this->fb->get('/me?fields=id,name,email');
	      $user = $response->getGraphUser(); // we retrieve the user data
	      // I added the defaultAccessToken to a session var for simplicity.
	      $_SESSION['facebook_defaultAccessToken'] = $this->fb->getDefaultAccessToken();
	    } catch(Facebook\Exceptions\FacebookResponseException $e) {
	      // When Graph returns an error
	      echo 'Could not retrieve user data: ' . $e->getMessage();
	      exit;
	    } catch(Facebook\Exceptions\FacebookSDKException $e) {
	      // When validation fails or other local issues
	      echo 'Facebook SDK returned an error: ' . $e->getMessage();
	      exit;
	    }
	    //we do not actually need to verify if the user email address is correct... but we should make sure
        if($this->form_validation->valid_email($user['email'])) {
          $this->load->model('Auth_facebook_model');
          	if($this->Auth_facebook_model->login_with_facebook($user['email'])) {
            	redirect($config['redirect_after_fb_login'] );
          	} else {
            	redirect($config['redirect_if_fb_login_fail'] );
          	}
        }
	  	} else {
	    echo 'oups... where is the access token???';
	  	}
	  	// debug code is below
	  	// echo '<pre>';
	  	// print_r($this->fb);
	  	// echo '</pre>';
	  	// end
	}

function facebook_logout(){
	
		//$token = $this->fb->getAccessToken();
		$token = $_SESSION['facebook_defaultAccessToken'];
		$url = 'https://www.facebook.com/logout.php?next=' . 'www.movinghaus.net' .
		'&access_token='.$token;
		session_destroy();
		header('Location: '.$url);
	}
} // End class