<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_public extends MH_Public_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Faq_model');
    }  

    // list view of faq posts
    function index(){

        // load the hide show js
        $this->data['mh_scripts_public']     .=  
            '<!-- MH Scripts Admin -->
            <script type="text/javascript" src="/inc/themes/faq/faq_scripts/hide_show.js"></script>
            ';
         
        // add datatables theme/css
        $this->data['mh_theme_public']       .=  '<!-- MH Public Theme -->
                                            <link rel="stylesheet" type="text/css" href="/inc/themes/faq/faq_css/faq_new.css">';
                                            
        // if there is session data, grab it.  This will give 
        // you the category number only.
        $category       = $this->session->userdata('category');

        // get the name of the category
        if (!empty($category)) {
           $this->data['filter_category_name'] = $this->Faq_model->get_category_name($category);
        }
        
        // #DEBUGGING
        // echo 'The year is '.$year.' and the month is '.$month;
        
        // set up some formatting for pagination in CI
        $config['full_tag_open']  = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['num_tag_open']	  = '<li class="page-item">';
        $config['num_tag_close']  = '</li>';
        $config['cur_tag_open']   = '<li class="page-item active"><a class="page-link">';
        $config['cur_tag_close']  = '</a></li>';
        $config['next_tag_open']  = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open']  = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['data_page_attr'] = 'class="page-link"';

        // config vars for pagination
        $config['base_url']     = site_url("Faq_public/index");
        
        if (!empty($this->session->userdata('category'))){
            
            // if the year and month are set, that means a filter has been 
            // applied.  If applied, get the number of posts to set the 
            // pagination links.
            //echo 'filter applied';
            $category_id = $this->session->userdata('category');
            
            //$config['total_rows']   = $this->db->get('faqs')->where('category_id', '76')->num_rows();
            
            $config['total_rows']   = $this->db->get_where('faqs', array('category_id' => $category_id))->num_rows();
            
            
            //$config['total_rows']   = $num_posts;
            
        } else {
            
            // if there is no filter, get all the data/rows
            $config['total_rows']   = $this->db->get('faqs')->num_rows();
            //print_r($config['total_rows']);
        }

        // number of posts per page
        $config['per_page']     = $this->config->item('faq_number_of_posts_per_page');
        
        //num_links is the number of naviation links forward and backward to display
        $config['faq_num_links']    = 10;
        
        // i think this is the offset.
        $offset = $this->uri->segment(3); //$config['uri_segment'];
        
        // initialize pagination
        $this->pagination->initialize($config);

        // this contains the paginaion links like 1,2,3,next,prev etc
        $this->data['links']                = $this->pagination->create_links();

        // don't use them so just make them blank.
        $year ='';
        $month = '';

        // get all the faq data
        $this->data['faqs']                = $this->Faq_model->faqs_get(FALSE, $config['per_page'], $offset , $year, $month, $category );
         
        // set the title 
        $this->data['mh_public_title']      = $this->data['mh_title_public'] .' | Faq!';
        
        // get the view file
        $this->data['mh_public_view_file']      = 'mh-faq/public/mh-public-faq';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
        // Get the link data for the archive menu    
        $this->data['link_data'] = $this->Faq_model->faq_get_categories();
        //$this->Faq_model->faq_archive();
    
        // Get the link data for the category menu
        // not implemented yet 
        //$this->data['link_data_category'] = $this->Faq_model->faq_category();
        
        // load up the view file
        $this->load->view($mh_template, $this->data);
    }   
    
    // This is not implemented but if you wanted to have a 'read more' this would
    // be the method to use.
    function faq_view_single($slug = NULL){
        
        // need to get the faq title first!
        $this->data['faq']                 = $this->Faq_model->faqs_get($slug);
        
        // set the title 
        $this->data['mh_public_title']       = $this->data['mh_title_public'] .' | Faq Public!';
        
        // get the view file
        $this->data['mh_public_view_file']   = 'mh-faq/public/mh-public-faq-view-single';
        
        // set the template
        $mh_template = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
        
        // send all data to the view
        if (empty($this->data['faq'])){
            show_404();
        }
                // send all data to the view
        $this->load->view($mh_template, $this->data);
    }  

    // unset the filters
    function clear_faq(){
        
        // faq is filtered on categories.  By unsetting and redirecting
        // the filter is removed.
        $this->session->unset_userdata('category');
        redirect('Faq_public/', 'index');
        
    }
        
    function list_faq_category(){
        
        // set the session year 
        $category =  $this->uri->segment(3);
        $this->session->set_userdata('category', $category);
        
        redirect('Faq_public/', 'index');
    }
}