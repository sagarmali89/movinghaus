<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MH_settings_admin extends MH_Admin_Controller {

	Public function __construct() { 
        parent::__construct(); 
	} 

	public function index()
	{
        $data['main_settings'] = $this->MH_settings_model->mh_get_configurations();

        
        // set the title 
        $this->data['mh_admin_title']           = $this->data['mh_title_admin'] .' | Main Settings!';
        
        // get the view file
        $this->data['mh_admin_view_file']       = 'mh-settings/admin/mh-main-settings';
        
        // set the template
        $mh_template = $this->data['mh_admin_dir'] . $this->data['mh_admin_template'];
        
        // send all data to the view
        $this->load->view($mh_template, $this->data);
	}

/**
	public function savedata()
    {
        If( $_SERVER['REQUEST_METHOD']  != 'POST'  ){
            redirect('table');
        }
        
        $id = $this->input->post('id',true);
        $title = $this->input->post('title',true);
        
        $fields = array(
                    'title' => $title,
                  );
        
        $this->Tablemodel->posts_save($id,$fields);
        
        echo "Successfully saved";
          
    }
**/
}