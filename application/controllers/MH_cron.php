<?php // if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MH_Cron extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        if (!$this->input->is_cli_request()) {
           show_error('Direct access is not allowed.  Only available via CLI');
        }
        
        // used for xrates
        $this->load->library('ccci' );
        $this->load->model('Ccci_model');
    }
    
    // Insert reference to hourly scripts here like	$this->ccci_update_xrates(); and then put script far below.
    public function mh_hourly(){
    
    }
    
    // Insert reference to daily scripts here like	$this->ccci_update_xrates(); and then put script far below.
    public function mh_daily(){
    	/// usr/bin/php /var/www/movinghaus.net/public_html/index.php MH_cron mh_daily 
    	// if you want your script to run daily, include it here.
    	$this->ccci_update_xrates();
    }
    
/**
 * CRON JOB SCRIPTS GO BELOW HERE
 **/

  // update of replace the tracked xrates 
  public function ccci_update_xrates($currency_array = NULL)
  {
    // if no currency_array is supplied, then update all rates 
    // as denoted in the _contruct
    if ($currency_array == NULL){
    $this->currency_array = array('JPY_USD',
                            'USD_JPY',
                            'AUD_USD',
                            'USD_AUD',
                            'CAD_USD',
                            'USD_CAD',
                            'JPY_NZD',
                            'NZD_JPY');
                            
    } 
    // the first argument is the amount.  in this case 1.
    $this->data['ccci_convert_many'] = $this->ccci->ccci_convert_many(1, $this->currency_array);
    if ($this->Ccci_model->ccci_update_xrates($this->data['ccci_convert_many']))
    {
       	log_message('info', 'Your daily cron job for xrates was successful.');  
    } else {
		log_message('error', 'Your daily cron job for xrates incurred an error.');  
    }
    
  }

   
}