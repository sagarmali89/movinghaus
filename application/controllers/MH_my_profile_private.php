<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MH_my_profile_private extends MH_Private_Controller {
    
    function __construct() {
        
        parent::__construct();
   
        // Define the module name here
        $this->module_system_name   = 'MH_my_profile';
        $this->module_view_name     = 'My Profile'; 
        $this->model_name           = $this->module_system_name.'_model';
        
        // set the template
        $this->mh_template = $this->data['mh_private_dir'] . $this->data['mh_private_template'];

        // Add any scripts 
        // $this->data['mh_scripts_admin'] .= '<!-- MH Scripts Admin --> ';
        
        // load up the model
        $this->load->model($this->model_name);
        
        // load the validation library
        $this->load->library('form_validation');
    }
    
    function my_profile(){

        // Set the titles
        $this->data['mh_private_title']      = $this->data['mh_title_private'] .' | List - '.$this->module_view_name;
    
        // load up datatables and related scripts
        $this->data['mh_scripts_private']     
                .=  '<!-- MH Scripts Private -->
                    ';
        
        // add datatables theme/css
        $this->data['mh_theme_private']       
                .=  '<!-- MH Private Theme -->
                    ';
        
        // grab all the data                        
        $model = $this->model_name; 
       // $this->data[$this->module_system_name]  = $this->$model->index_all();
        
        // nominate the view file
        $this->data['mh_private_view_file']       = $this->module_system_name .'/private/'.$this->module_system_name .'-private-index';

        // run it all
        $this->load->view($this->mh_template, $this->data);
        
    }
    
}
