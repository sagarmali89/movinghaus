 <?php defined('BASEPATH') OR exit('No direct script access allowed');

class MH_register_public extends MH_Public_Controller  {

  function __construct() {
    
    parent::__construct();
    
  }  

  public function index() {
    
    // set the title 
    $this->data['mh_public_title']          = $this->data['mh_title_public'] .' | Register!';
    
    // get the view file
    $this->data['mh_public_view_file']      = 'mh-register/mh-register';
    
    // set the template
    $mh_template                            = $this->data['mh_public_dir'] . $this->data['mh_public_template'];
    
    // send all data to the view
    $this->load->view($mh_template, $this->data);
    
  }

}