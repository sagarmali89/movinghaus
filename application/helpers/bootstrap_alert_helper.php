<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a simple helper to display a bootstrap 3 alert box
 * $message must contain:
 * 
 * $message['message']  - can be any message.
 * $message['class']    - must be one of following:
 *                          warning
 *                          danger
 *                          success
 *                          info
 * */
if ( ! function_exists('bootstrap_alert')) {
   
    // takes an array
    function bootstrap_alert($message) {

        if (isset($message['message'])){
		    $bootstrap_alert_html = '<div class="alert alert-'.$message['class'].'">';
		    $bootstrap_alert_html .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
  		    $bootstrap_alert_html .= $message['message'];
		    $bootstrap_alert_html .= '</div>';
		    return $bootstrap_alert_html; 
        } else {
            return false;
        }
    
    }   
}


