<?php
/**
 * Name:    CurrencyConverterCI (ccci)
 * Author:  Richard Stinchcombe
 *          ircnits@gmail.com
 *     
 * Created:  APR 18 2019
 *
 * Description: Simple Currency Converter Library
 * Requirements: PHP5 or above
 *
 * @package    CurrencyConverterCI (ccci)
 * @author     Richard Stinchcombe
 * @link       tba
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class ccci
 */
class Ccci
{
    private $CI;
    
	public function __construct()
	{   
	    $CI =& get_instance();
	    $CI->config->load('ccci');
        $this->ccci_api_key         = $CI->config->item('ccci_key');         
        $this->ccci_endpoint        = $CI->config->item('ccci_endpoint');    
        $this->ccci_endpoint_usage  = $CI->config->item('ccci_endpoint_usage');   
	}
	
	// This code mainly taken from currconv.com with some tweaks.
    function ccci_convert($amount,$from_currency,$to_currency)
    {
        $ccci_api_key   =  $this->ccci_api_key;
        $ccci_endpoint  =  $this->ccci_endpoint;
        $from_Currency  = urlencode($from_currency);
        $to_Currency    = urlencode($to_currency);
        $query          = "{$from_Currency}_{$to_Currency}";
        $json           = file_get_contents($ccci_endpoint."convert?q={$query}&compact=ultra&apiKey={$ccci_api_key}");
        $obj            = json_decode($json, true);
        // trick to just get the numbers from the array.  Maybe not the best way.
        $val            = floatval($obj["$query"]);
        $total          = $val * $amount;
        //number_format ( float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = "," ) : string
        return number_format($total, 2, '.', ',');
    }
    
    function ccci_convert_many($amount,$from_to_currency_array)
    {
        $ccci_api_key   =  $this->ccci_api_key;
        $ccci_endpoint  =  $this->ccci_endpoint;
        $currency_string = '';
        $i = 0;  // simple counter
        $len = count($from_to_currency_array); //how many SETS of currencies
        foreach ($from_to_currency_array as $currency_array) {
            if ($i < ($len -1)) {
                // add a comma to the string 
                $currency_string .= $currency_array.',';
            } else {
                // don't add a commma to the last one
                $currency_string .= $currency_array ;
            }
            $i++;
        }
        $json   = file_get_contents($ccci_endpoint."convert?q={$currency_string}&compact=ultra&apiKey={$ccci_api_key}");
        $obj    = json_decode($json, true);
        return $obj;
    }

    // /api/v7/currencies?apiKey=[YOUR_API_KEY]
    function ccci_currency_list()
    {
        $ccci_api_key   = $this->ccci_api_key;
        $ccci_endpoint  = $this->ccci_endpoint;
        $json           = file_get_contents($ccci_endpoint."currencies?apiKey={$ccci_api_key}");
        $obj            = json_decode($json, true);
        return $obj;
    }
    
    // /api/v7/countries?apiKey=[YOUR_API_KEY]
    function ccci_country_list()
    {
        $ccci_api_key   = $this->ccci_api_key;
        $ccci_endpoint  = $this->ccci_endpoint;
        $json           = file_get_contents($ccci_endpoint."countries?apiKey={$ccci_api_key}");
        $obj            = json_decode($json, true);
        return $obj;
    }
    
    // Historical Data (Experimental, Single Date)
    // /api/v7/convert?q=USD_PHP,PHP_USD&compact=ultra&date=[yyyy-mm-dd]&apiKey=[YOUR_API_KEY]
    function ccci_historical()
    {
        
    }
    
    // /others/usage?apiKey=[YOUR_API_KEY]
    function ccci_usage()
    {
        $ccci_api_key           = $this->ccci_api_key;
        $ccci_endpoint_usage    = $this->ccci_endpoint_usage;
        $json                   = file_get_contents($ccci_endpoint_usage);
        $obj                    = json_decode($json, true);
        return $obj;
    }
    
    // /api/v7/convert?q=USD_PHP,PHP_USD&compact=ultra&apiKey=[YOUR_API_KEY]
    public function ccci_get_rates(){

	}
	
	public function ccci_country_list_update_db($country_list_array = NULL)
	{
	    $i = 0;
	    if ($country_list_array != NULL) {
	        
	        foreach ($country_list_array as $key => $country_list ) {
	            
	            $data = $country_list_array[$key];
	            $this->db->replace('ccci_countries', $data);
	            exit();
	            
	            echo '</br>';
	            //$x= $country_list_array['alpha3'];
	            //echo $x;
	            //echo '<pre>';
	           //print_r($key=>$country_list_array);
	           // echo '</pre>';
	        }
	        //$this->db->replace('ccci_countries', $data);
	    } else {
	        return FALSE;
	    }
	}
	

}
