<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| MY_Controller
|--------------------------------------------------------------------------
|   MY_Controller defines 3 classes as follows:
|   
|   Class Name              Description
|   -----------             ------------
|   MH_Public_Controller    Extend this for public pages
|   MH_Private_Controller   Extend this for pages that require a login by users/
|                           members or admin
|   MH_Admin_Controller     Extend this for pages that require a login by Admins
|
*/

/*
|--------------------------------------------------------------------------
| MH_Public_Controller
|--------------------------------------------------------------------------
|   Extend this controller in your controller for all public accessible 
|   pages.  No login is required to view a public page.
|
*/
class MY_Controller extends CI_Controller {
    
    function __construct() {
        
        parent::__construct();
        
        $appConfigOptions = $this->MH_settings_model->mh_get_configurations();
 
        if($appConfigOptions) {
        
            foreach($appConfigOptions as $appConfigOption)
                {
                $this->config->set_item($appConfigOption->key,$appConfigOption->value);
                }
 
        }

        // load all the settings for MH-App
        $this->config->load('mh_app_settings');
        
        // now we build the facebook object...
        $this->fb                   = new Facebook\Facebook([
            'app_id'                => $this->config->item('facebook_app_id'),
            'app_secret'            => $this->config->item('facebook_app_secret'),
            'default_graph_version' => $this->config->item('default_graph_version')
        ]);
    
    }
}

class MH_Public_Controller extends MY_Controller {

	function __construct() {
	    
	    parent::__construct();
	    
	    $this->config->load('mh_app_settings');
	    
	    // Check to see if the site is up or down 
	    // through application/config/mh_app_setting.pgp
	    
	    //echo $this->config->item('mh_site_up_public'); exit();
	    
	    
	    if(((boolean) $this->config->item('mh_site_up_public')) ==  FALSE) {
            show_error($this->config->item('mh_site_down_public_message'));
        }
        
        // Check to see if the profiler is enabled or not 
	    // through application/config/mh_app_setting.pgp
	    
        if($this->config->item('mh_profiler_public') === TRUE) {
            $this->output->enable_profiler($this->config->item('profiler'));
        }
    
        /*
        |--------------------------------------------------------------------------
        | Home basic settings
        |--------------------------------------------------------------------------
        |   These are the default settings for MH-App.  Remeber, the site is divided 
        |   into 3 sections, namely, pubic, private and admin.  Check the file
        |   /application/config/mh_app_settings.php for default settings. 
        |   $this->data['mh_public_dir']          = $this->config->item('mh_public_dir'); 
        |   $this->data['mh_public_template']     = $this->config->item('mh_public_template'); 
        |   $this->data['mh_header_public']       = $this->config->item('mh_header_public');   
        |   $this->data['mh_navbar_public']       = $this->config->item('mh_navbar_public');
        |   $this->data['mh_footer_public']       = $this->config->item('mh_footer_public');
        |   $this->data['mh_view_file']           = '';  // nothin set in default.
        |   $this->data['mh_title_public']        = $this->config->item('mh_title_public');
        |   $this->data['mh_theme_public']        = $this->config->item('mh_theme_public');
        |   $this->data['mh_scripts_public']      = $this->config->item('mh_scripts_public');
        | 
        |   All settings can be overridden.  If you wish to change the defaults, 
        |   change the below in the __constructor or in a method.
        |
        */
        
        // Override above settings with below.
        $this->data['mh_public_dir']            = $this->config->item('mh_public_dir'); 
        $this->data['mh_public_template']       = $this->config->item('mh_public_template'); 
        $this->data['mh_header_public']         = $this->config->item('mh_header_public');   
        $this->data['mh_navbar_public']         = $this->config->item('mh_navbar_public');
        $this->data['mh_footer_public']         = $this->config->item('mh_footer_public');
        // $this->data['mh_view_file']           = ''; // this should be set in this controller's method
        $this->data['mh_title_public']          = $this->config->item('mh_title_public');
        $this->data['mh_theme_public']          = $this->config->item('mh_theme_public');
        $this->data['mh_scripts_public']        = $this->config->item('mh_scripts_public');
        
        // Pagination for Public
        
        /* This Application Must Be Used With BootStrap 3 *  */
        $this->data['public_full_tag_open']     = $this->config->item('public_full_tag_open');
        $this->data['public_full_tag_close']     = $this->config->item('public_full_tag_close');
        $this->data['public_num_tag_open']       = $this->config->item('public_num_tag_open');
        $this->data['public_num_tag_close']      = $this->config->item('public_num_tag_close');
        $this->data['public_cur_tag_open']       = $this->config->item('public_cur_tag_open');
        $this->data['public_cur_tag_close']      = $this->config->item('public_cur_tag_close');
        $this->data['public_next_tag_open']      = $this->config->item('public_next_tag_open');
        $this->data['public_next_tagl_close']    = $this->config->item('public_next_tagl_close');
        $this->data['public_prev_tag_open']      = $this->config->item('public_prev_tag_open');
        $this->data['public_prev_tagl_close']    = $this->config->item('public_prev_tagl_close');
        $this->data['public_first_tag_open']     = $this->config->item('public_first_tag_open');
        $this->data['public_first_tagl_close']   = $this->config->item('public_first_tagl_close');
        $this->data['public_last_tag_open']      = $this->config->item('public_last_tag_open');
        $this->data['public_last_tagl_close']    = $this->config->item('public_last_tagl_close');
   
	}	
	
}

class MH_Admin_Controller extends MY_Controller {

	function __construct() {
	    
	    parent::__construct();
	    
	    $this->config->load('mh_app_settings');
	    
	    // Check to see if the site is up or down 
	    // through application/config/mh_app_setting.pgp
	    
	    if($this->config->item('mh_site_up_admin') === FALSE) {
            show_error($this->config->item('mh_site_down_admin_message'));
        }
        
        // Check to see if the profiler is enabled or not 
	    // through application/config/mh_app_setting.pgp
	    
        if($this->config->item('mh_profiler_admin') === TRUE) {
            $this->output->enable_profiler($this->config->item('profiler'));
        }
    
        /*
        |--------------------------------------------------------------------------
        | Admin basic settings
        |--------------------------------------------------------------------------
        |   These are the default settings for MH-App.  Remeber, the site is divided 
        |   into 3 sections, namely, pubic, private and admin.  Check the file
        |   /application/config/mh_app_settings.php for default settings. 
        |   $this->data['mh_admin_dir']          = $this->config->item('mh_admin_dir'); 
        |   $this->data['mh_admin_template']     = $this->config->item('mh_admin_template'); 
        |   $this->data['mh_header_admin']       = $this->config->item('mh_header_admin');   
        |   $this->data['mh_navbar_admin']       = $this->config->item('mh_navbar_admin');
        |   $this->data['mh_footer_admin']       = $this->config->item('mh_footer_admin');
        |   $this->data['mh_view_file']          = '';  // nothin set in default.
        |   $this->data['mh_title_admin']        = $this->config->item('mh_title_admin');
        |   $this->data['mh_theme_admin']        = $this->config->item('mh_theme_admin');
        |   $this->data['mh_scripts_admin']      = $this->config->item('mh_scripts_admin');
        | 
        |   All settings can be overridden.  If you wish to change the defaults, 
        |   change the below in the __constructor or in a method.
        |
        */
        
        // Override above settings with below.
        $this->data['mh_admin_dir']             = $this->config->item('mh_admin_dir'); 
        $this->data['mh_admin_template']        = $this->config->item('mh_admin_template'); 
        $this->data['mh_header_admin']          = $this->config->item('mh_header_admin');   
        $this->data['mh_navbar_admin']          = $this->config->item('mh_navbar_admin');
        $this->data['mh_footer_admin']          = $this->config->item('mh_footer_admin');
        // $this->data['mh_view_file']           = ''; // this should be set in this controller's method
        $this->data['mh_title_admin']           = $this->config->item('mh_title_admin');
        $this->data['mh_theme_admin']           = $this->config->item('mh_theme_admin');
        $this->data['mh_scripts_admin']         = $this->config->item('mh_scripts_admin');
        
        // Twillio for Admins
        $this->data['mh_twilio_enable']         = $this->config->item('mh_twilio_enable');
        $this->data['mh_twilio_phone_number']   = $this->config->item('mh_twilio_phone_number');
        $this->data['mh_twilio_account_sid']    = $this->config->item('mh_twilio_account_sid');
        $this->data['mh_twilio_auth_token']     = $this->config->item('mh_twilio_auth_token');
        
        // Pagination for Admins
        
        /* This Application Must Be Used With BootStrap 3 *  */
        $this->data['admin_full_tag_open']      = $this->config->item('admin_full_tag_open');
        $this->data['admin_full_tag_close']     = $this->config->item('admin_full_tag_open');
        $this->data['admin_num_tag_open']       = $this->config->item('admin_num_tag_open');
        $this->data['admin_num_tag_close']      = $this->config->item('admin_num_tag_close');
        $this->data['admin_cur_tag_open']       = $this->config->item('admin_cur_tag_open');
        $this->data['admin_cur_tag_close']      = $this->config->item('admin_cur_tag_close');
        $this->data['admin_next_tag_open']      = $this->config->item('admin_next_tag_open');
        $this->data['admin_next_tagl_close']    = $this->config->item('admin_next_tagl_close');
        $this->data['admin_prev_tag_open']      = $this->config->item('admin_prev_tag_open');
        $this->data['admin_prev_tagl_close']    = $this->config->item('admin_prev_tagl_close');
        $this->data['admin_first_tag_open']     = $this->config->item('admin_first_tag_open');
        $this->data['admin_first_tagl_close']   = $this->config->item('admin_first_tagl_close');
        $this->data['admin_last_tag_open']      = $this->config->item('admin_last_tag_open');
        $this->data['admin_last_tagl_close']    = $this->config->item('admin_last_tagl_close');
        
        // this is a hack to get an instance of CI externally to the application folder.  Used it for CKfinder auth var.
        if (defined('REQUEST') && REQUEST === 'external') {
            return ;
        }
        
        
        /** 
         * Automatically add user settings to session var. 
         * This is also the place to set default user settings.
         */ 
        $settings = $this->MH_user_settings_model->mh_get_configurations();
        foreach ($settings as $setting) {
           $this->session->set_userdata($setting['mh_us_key'], $setting['mh_us_value']);
            // left in for potential debugging
            //echo $setting['mh_us_key'];
            //echo $setting['mh_us_value'];
        }
        // left in for potential debugging
        // print_r( $this->session->userdata());
        
        /**
         * SET THE DEFAULT TIMEZONE FOR THE USER
         */
        
        if (empty($this->session->userdata('user_time_zone')) || ($this->session->userdata('user_time_zone') == NULL) ) {
             $this->session->set_userdata('user_time_zone', 'UTC');
        } 
        
         
        
        // only allow access to logged in admins.
        if (((!$this->ion_auth->logged_in()) && (!$this->ion_auth->is_admin())))
		{
			redirect('auth/login');
		}
	
	    // Number of new leads in the last 24 hrs
   
	   $this->data['num__new_leads_24'] = $this->db->where("FROM_UNIXTIME(`created_on`) >= NOW() - INTERVAL 1 DAY")->from('users')->count_all_results();
	  // future implementation
	  //$this->data['num__new_customers_24'] 
	  //$this->data['num__new_quotes_24'] 
	  //$this->data['num__new_sales_24'] 
	  //$this->data['num__new_expenses_24'] 
	  
   

	}
}

class MH_Private_Controller extends MY_Controller {

	function __construct() {
	    
	    parent::__construct();
	    
	    $this->config->load('mh_app_settings');
	    
	    // Check to see if the site is up or down 
	    // through application/config/mh_app_setting.pgp
	    
	    if($this->config->item('mh_site_up_private') === FALSE) {
            show_error($this->config->item('mh_site_down_private_message'));
        }
        
        // Check to see if the profiler is enabled or not 
	    // through application/config/mh_app_setting.pgp
	    
        if($this->config->item('mh_profiler_private') === TRUE) {
            $this->output->enable_profiler($this->config->item('profiler'));
        }
    
        /*
        |--------------------------------------------------------------------------
        | Private basic settings
        |--------------------------------------------------------------------------
        |   These are the default settings for MH-App.  Remeber, the site is divided 
        |   into 3 sections, namely, pubic, private and admin.  Check the file
        |   /application/config/mh_app_settings.php for default settings. 
        |   $this->data['mh_admin_dir']          = $this->config->item('mh_admin_dir'); 
        |   $this->data['mh_admin_template']     = $this->config->item('mh_admin_template'); 
        |   $this->data['mh_header_admin']       = $this->config->item('mh_header_admin');   
        |   $this->data['mh_navbar_admin']       = $this->config->item('mh_navbar_admin');
        |   $this->data['mh_footer_admin']       = $this->config->item('mh_footer_admin');
        |   $this->data['mh_view_file']          = '';  // nothin set in default.
        |   $this->data['mh_title_admin']        = $this->config->item('mh_title_admin');
        |   $this->data['mh_theme_admin']        = $this->config->item('mh_theme_admin');
        |   $this->data['mh_scripts_admin']      = $this->config->item('mh_scripts_admin');
        | 
        |   All settings can be overridden.  If you wish to change the defaults, 
        |   change the below in the __constructor or in a method.
        |
        */
        
        // Override above settings with below.
        $this->data['mh_private_dir']             = $this->config->item('mh_private_dir'); 
        $this->data['mh_private_template']        = $this->config->item('mh_private_template'); 
        $this->data['mh_header_private']          = $this->config->item('mh_header_private');   
        $this->data['mh_navbar_private']          = $this->config->item('mh_navbar_private');
        $this->data['mh_footer_private']          = $this->config->item('mh_footer_private');
        // $this->data['mh_view_file']           = ''; // this should be set in this controller's method
        $this->data['mh_title_private']           = $this->config->item('mh_title_private');
        $this->data['mh_theme_private']           = $this->config->item('mh_theme_private');
        $this->data['mh_scripts_private']         = $this->config->item('mh_scripts_private');
        

        // Pagination for Private
        
        /* This Application Must Be Used With BootStrap 3 *  */
        $this->data['private_full_tag_open']      = $this->config->item('private_full_tag_open');
        $this->data['private_full_tag_close']     = $this->config->item('private_full_tag_open');
        $this->data['private_num_tag_open']       = $this->config->item('private_num_tag_open');
        $this->data['private_num_tag_close']      = $this->config->item('private_num_tag_close');
        $this->data['private_cur_tag_open']       = $this->config->item('private_cur_tag_open');
        $this->data['private_cur_tag_close']      = $this->config->item('private_cur_tag_close');
        $this->data['private_next_tag_open']      = $this->config->item('private_next_tag_open');
        $this->data['private_next_tagl_close']    = $this->config->item('private_next_tagl_close');
        $this->data['private_prev_tag_open']      = $this->config->item('private_prev_tag_open');
        $this->data['private_prev_tagl_close']    = $this->config->item('private_prev_tagl_close');
        $this->data['private_first_tag_open']     = $this->config->item('private_first_tag_open');
        $this->data['private_first_tagl_close']   = $this->config->item('private_first_tagl_close');
        $this->data['private_last_tag_open']      = $this->config->item('private_last_tag_open');
        $this->data['private_last_tagl_close']    = $this->config->item('private_last_tagl_close');
        
        // this is a hack to get an instance of CI externally to the application folder.  Used it for CKfinder auth var.
        if (defined('REQUEST') && REQUEST === 'external') {
            return ;
        }
        
        // only allow access to logged in.
        if (((!$this->ion_auth->logged_in())))
		{
			redirect('auth/login');
		}

        /** 
         * automatically add user settings to session var
         * 
         */ 
        $settings = $this->MH_user_settings_model->mh_get_configurations();
        foreach ($settings as $setting) {
           $this->session->set_userdata($setting['mh_us_key'], $setting['mh_us_value']);
            // left in for potential debugging
            //echo $setting['mh_us_key'];
            //echo $setting['mh_us_value'];
        }
        // left in for potential debugging
        // print_r( $this->session->userdata());
        
        /**
         * SET THE DEFAULT TIMEZONE FOR THE USER
         */
        if (empty($this->session->userdata('user_time_zone')) || ($this->session->userdata('user_time_zone') == NULL) ) {
             $this->session->set_userdata('user_time_zone', 'UTC');
        } 
	}
	
	
}