<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Faq
|--------------------------------------------------------------------------
|
| This is all the form validation rule set.
*/


$config['validate_faq_create'] =  array(
            // this is the category name
            'title' => array(
                'field' => 'title',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]|is_unique[posts.title]',
                'errors' => array(
                    'required'      => 'The title cannot be blank.',
                    'min_length'    => 'The title must be 5 charaters or more.',
                    'is_unique'     => 'The title must be unique.'
                ),
            ),
            
            'body' => array(
                'field' => 'body',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]',
                'errors' => array(
                    'required'    => 'The body cannot be blank',
                    'min_length' => 'The body must be 5 charaters or more.',
                )    
                
            ),
); // end validate_faq_create


$config['validate_faq_update'] =  array(
            // this is the category name
            'title' => array(
                'field' => 'title',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]|callback_is_slug_unique_on_update[]',
                
               'errors' => array(
                   'required'      => 'The title cannot be blank.',
                   'min_length'    => 'The title must be 5 charaters or more.',
                   'is_unique'     => 'The title must be unique.',
                    'is_slug_unique_on_update' => 'The new title needs to be unique'
                ),
            ),
            
            'body' => array(
                'field' => 'body',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]',
                'errors' => array(
                    'required'    => 'The body cannot be blank',
                    'min_length' => 'The body must be 5 charaters or more.',
                )    
                
            ),
); // end validate_faq_create
    