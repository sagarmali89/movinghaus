<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Currency Settings - https://www.currencyconverterapi.com/docs
| You can also get your free api key from above site.
|--------------------------------------------------------------------------
*/

$config['ccci_table']           = 'currency_converter';
$config['ccci_key']             = 'c25c825c8c2373997607';
$config['ccci_endpoint']        = 'https://free.currconv.com/api/v7/'; // need the trailing slash!
$config['ccci_endpoint_usage']  = 'https://free.currconv.com/others/usage?apiKey='.$config['ccci_key'];