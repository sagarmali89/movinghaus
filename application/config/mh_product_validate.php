<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Product - your online shop
|--------------------------------------------------------------------------
|
| This is all the form validation rule set.
*/


$config['validate_product_create'] =  array(
           
            'product_name' => array(
                'field' => 'product_name',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]|is_unique[product.product_name]',
                'errors' => array(
                    'required'      => 'The product name cannot be blank.',
                    'min_length'    => 'The product name must be 5 charaters or more.',
                    'is_unique'     => 'The product name must be unique.'
                ),
            ),
            
            'product_description' => array(
                'field' => 'product_description',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]',
                'errors' => array(
                    'required'    => 'The product description cannot be blank',
                    'min_length' => 'The product description must be 5 charaters or more.',
                )    
                
            ),
            
            'product_price' => array(
                'field' => 'product_price',
                'label' => '',
                'rules' => 'numeric|required|trim|xss_clean|min_length[1]',
                'errors' => array(
                    'required'    => 'The product price cannot be blank',
                    'min_length' => 'The product price must be 5 charaters or more.',
                )    
                
            ),
            
            
); // end validate_product_create


$config['validate_product_update'] =  array(
           
            'product_name' => array(
                'field' => 'product_name',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]|callback_is_slug_unique_on_update[]',
                
               'errors' => array(
                   'required'      => 'The product name cannot be blank.',
                   'min_length'    => 'The product name must be 5 charaters or more.',
                   'is_unique'     => 'The product name must be unique.',
                    'is_slug_unique_on_update' => 'The new title needs to be unique'
                ),
            ),
            
            'product_description' => array(
                'field' => 'body',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[5]',
                'errors' => array(
                    'required'    => 'The product description cannot be blank',
                    'min_length' => 'The product description must be 5 charaters or more.',
                )    
                
            ),
); // end validate_product_update
    