<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['validate_file_categories'] =  array(
            // this is the category name
            'name' => array(
                'field' => 'name',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[3]|max_length[50]|is_unique[categories.name]',
                'errors' => array(
                    'required'    => 'Cannot be blank',
                    'is_unique'  => 'Category already in use.  Pick another name.',
                    'min_length' => 'Category must be 3 charaters or more.',
                    'max_length' => 'Category must not be more than 50 charaters.'
                ),
            ),
);
    