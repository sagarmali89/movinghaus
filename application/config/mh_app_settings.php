<?php
/**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     07.19.2018
 *
 * Description: MH App is a base website to kickstart your CI apps.
 *
 * Requirement: PHP5 or above
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/spreaderman/mh-app
 * @filesource
 */

 defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| MH App General Company Settings
|--------------------------------------------------------------------------
|  All your general company settings  
|
*/
$config['mh_company_name']  = 'MovingHaus';
$config['mh_address']       = '1-3-1 Toyosu, Koto-ku';
$config['mh_city']          = 'Tokyo';
$config['mh_country']       = 'Japan';
$config['mh_postal_code']   = '135-0061';
$config['mh_phone_number']  = '+81-3-5555-5555';
$config['mh_fax_number']    = '+81-3-5555-5556';
$config['mh_email']         = 'info@movinghaus.net';
$config['mh_logo_menu']     = '/ckfinder/userfiles/images/mh-logo/logo_size_icon_invert.jpg';
$config['mh_website']       = 'www.movinghaus.com';  // Not to be used in paths.  
                                                    // Purely text representation of the website.

// Assign the default currency code for website                                                    
// https://www.ibm.com/support/knowledgecenter/en/SSZLC2_7.0.0/com.ibm.commerce.payments.developer.doc/refs/rpylerl2mst97.htm
$config['mh_default_code_alpha']    = 'JPY';  // Japanese yen


/*
|--------------------------------------------------------------------------
| MH App Default Settings
|--------------------------------------------------------------------------
|  These are the main setting for MH.  The site is divided into 3 sections:  
|
|   Section     Description
|   --------    ---------------------
|   public      - no login required and viewable by all
|   private     - must login to see
|   admin       - only admins can these these pages if logged in.
| 
|  FYI, the section name matches with the MY_Controller name (see core folder) 
|  as follows:
|   
|   Section     Controller Name
|   --------    ---------------------
|   public      MH_Public_Controller
|   private     MH_Private_Controller
|   admin       MH_Admin_Controller
| 
|   All settings can be overridden your respective controller.
|
*/

// please set the site name in config.php, as usual
$config['mh_title_public']  = 'Sailfish | Military and Commercial Port Agency';     
$config['mh_title_private'] = 'Sailfish | Private';   
$config['mh_title_admin']   = 'Sailfish | Admin';       

/*
|--------------------------------------------------------------------------
| MH Default Templates
|--------------------------------------------------------------------------
|   MH uses the below default templates set in your application/views/templates
|   folder.   
|
*/
    
// public template
$config['mh_public_dir']            =   'templates/public/_layouts/';
$config['mh_public_template']       =   'mh_default_template';
$config['mh_header_public']         =   'mh_header_public';    
$config['mh_navbar_public']         =   'mh_navbar_public';
$config['mh_footer_public']         =   'mh_footer_public';

// private section
$config['mh_private_dir']           =   'templates/private/_layouts/';
$config['mh_private_template']      =   'mh_default_template';
$config['mh_private_admin']         =   'mh_header_private';    
$config['mh_private_admin']         =   'mh_navbar_private';
$config['mh_private_admin']         =   'mh_footer_private';

// admin section
$config['mh_admin_dir']             =   'templates/admin/_layouts/';
$config['mh_admin_template']        =   'mh_default_template';
$config['mh_header_admin']          =   'mh_header_admin';    
$config['mh_navbar_admin']          =   'mh_navbar_admin';
$config['mh_footer_admin']          =   'mh_footer_admin';

/*
|--------------------------------------------------------------------------
| MH Default THEMES & CSS
|--------------------------------------------------------------------------
|   MH uses the CDN at https://www.bootstrapcdn.com/bootswatch/.  You can 
|   change the version number of the theme and the name of the theme.
| 
|  Example:
|
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.2/united/bootstrap.min.css';
|                                                ^^^^^ ^^^^^^   
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/cerulean/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/cosmo/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/cyborg/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/darkly/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/flatly/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/journal/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/litera/bootstrap.min.css
|  https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/lumen/bootstrap.min.css
|  etc etc
*/

$config['mh_theme_public']      =   '<!-- MH Public Theme -->
                                     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/united/bootstrap.min.css">
                                     <link rel="stylesheet" href="/inc/themes/default/css/custom_min.css">
                                     <link rel="stylesheet" href="/inc/themes/default/css/animate.css">
                                     
                                     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                                     <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css" />
                                     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.min.css">
                                     <link rel="stylesheet" href="/inc/themes/mh_app/css/mh_app.css">
                                     ';
                                     
$config['mh_theme_private']      =   '<!-- MH Private Theme -->
                                     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/united/bootstrap.min.css">
                                     <link rel="stylesheet" href="/inc/themes/default/css/custom_min.css">
                                     <link rel="stylesheet" href="/inc/themes/default/css/animate.css">
                                     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                                     <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css" />
                                      ';
    
                                   
$config['mh_theme_admin']       =   '<!-- MH Admin Theme -->
                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css">
                                    <link rel="stylesheet" href="/inc/themes/blog/blog_admin/blog.css">';
                                    
/*
|--------------------------------------------------------------------------
| MH Default Scripts
|--------------------------------------------------------------------------
|   These scripts are loaded as default.  This can be overriden in a 
|   a controller
*/

$config['mh_scripts_public']       =    '<!-- MH Scripts Public -->
                                        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
                                        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                                        ';

$config['mh_scripts_private']       =    '<!-- MH Scripts Private -->
                                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
                                        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>';

$config['mh_scripts_admin']       =    '<!-- MH Scripts Admin -->
                                        ';
                                                
 
/*
|--------------------------------------------------------------------------
| MH Site Up or Down Defaults
|--------------------------------------------------------------------------
|   You can make the site up or down.  FALSE means the site is down and 
|   TRUE means the site is up.  You can customize the message.
|
*/

$config['mh_site_up_public']                =   TRUE;                                      
$config['mh_site_down_public_message']      =   'The website is down for maintenance.  Check back soon!';  

$config['mh_site_up_private']               =   TRUE;                                      
$config['mh_site_down_private_message']     =   'The member section of the website is down for maintenance.  Check back soon!';  

$config['mh_site_up_admin']                 =   TRUE;                                      
$config['mh_site_down_admin_message']       =   'The admin section of the website is down.';  

/*
|--------------------------------------------------------------------------
| Profiler
|--------------------------------------------------------------------------
|  
|  If you wish to enable to the built in CI profiler, just change FALSE to
|  TRUE for the section of the website you wish to see it in.  The profiler
|  displays helpful debugging information at the bottom of each page in the 
|  section you have enabled it for.  You can set what debugging info you 
|  would and would not like to see in application/config/profiler.php
|
*/
$config['mh_profiler_public']   = FALSE;           
$config['mh_profiler_private']  = FALSE;
$config['mh_profiler_admin']    = FALSE;

/*
|--------------------------------------------------------------------------
| Twilio
|--------------------------------------------------------------------------
|  
|
*/

// Main auth token
// Pass your Account SID as the username and Auth Token as the password
$config['mh_twilio_auth_token']         = 'bcb9803382b5030a722361593e133ece';
$config['mh_twilio_url']                = '';
$config['mh_twilio_api_version']        = 'https://fax.twilio.com/v1';
$config['mh_twilio_account_sid']        = 'ACc3dc1eaa284c0d02f554c0b99a6a4fd6';

// Voice account 
$config['mh_twilio_voice_enable']       = TRUE;
$config['mh_twilio_voice_number']       = '+815031345388';
$config['mh_twilio_voice_account_sid']        = 'ACc3dc1eaa284c0d02f554c0b99a6a4fd6';


// Fax account
$config['mh_twilio_fax_enable']         = TRUE;
$config['mh_twilio_fax_number']         = '+815036280634';
$config['mh_twilio_fax_account_sid']    = 'ACc3dc1eaa284c0d02f554c0b99a6a4fd6';


// USA account but Japan phone number
//$config['mh_twilio_phone_number_usa']   = '+81345402039';
//$config['mh_twilio_account_sid_usa']    = 'AC7c428ef8606aee2e1a03d2964b21fe16';
//$config['mh_twilio_auth_token_usa']     = '6be085a45cd7f4f37ae35bc16cb9b512';

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at https://developer.facebook.com and create an app for your project.
|
|  app_id                string   Your facebook app ID.
|  app_secret            string   Your facebook app secret.
|  default_graph_version string   Set Facebook Graph version to be used. Eg v2.9
|  login_redirect_url    string   URL tor redirect back to after login. Do not include domain.
|  logout_redirect_url   string   URL tor redirect back to after login. Do not include domain.
|  scope                 array    The permissions you need : 'email', 'public_profile', 'user_location', ...
|  remember_me           bool
|  upload_path           string   The path where you want to store the avatar
*/
// these are initially used in MY_Controller to intialize.
//$config['facebook_app_id']                  = '2045926465658731';
//$config['facebook_app_secret']              = 'b846d1cb2bf72bb2663e76f710b7a2d7';
//$config['default_graph_version']            = 'v2.9';

// other configurable items
$config['login_redirect_url']               = '';  // blank for root
//$config['logout_redirect_url']            = 'example/logout';  // Not implemented
//$config['scope']                           = ['email'];  // depreciated
$config['remember_me']                      = TRUE;
//$config['upload_path']                      = './assets/uploads/avatars/';  // 
$config['fb_permissions']                   = ['public_profile', 'email'];

/*
|--------------------------------------------------------------------------
| Blog
|--------------------------------------------------------------------------
|  Here you can make various settlings to the blog.
*/

$config['number_of_posts_per_page']     = 5;   // How many articles/post to display per page in list view
$config['number_of_words_to_display']   = 100;   // When listing multiple articles, the number of words to display before ... and READ MORE

// this is the image for "SET NEW BLOG IMAGE".  It is the one pict you select
// that represents your article.

$config['image_path']       = 'ckfinder/userfiles/images/mh-blog/';
$config['upload_path']      = FCPATH . $config['image_path'];
$config['allowed_types']    = 'gif|jpg|png';
$config['max_size']         = '2048'; // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
$config['max_width']        = '5000'; // The maximum width (in pixels) that the file can be. Set to zero for no limit.
$config['max_height']       = '5000';  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
$config['remove_spaces']    = TRUE;
$config['encrypt_name']     = FALSE; // if true, the file name will be a random string
$config['overwrite']        = FALSE;
            
 
/*
|--------------------------------------------------------------------------
| FAQ
|--------------------------------------------------------------------------
|  Here you can make various settlings to the blog.
*/

$config['faq_number_of_posts_per_page']     = 5;   // How many articles/post to display per page in list view
$config['faq_number_of_words_to_display']   = 100;   // When listing multiple articles, the number of words to display before ... and READ MORE

// this is the image for "SET NEW BLOG IMAGE".  It is the one pict you select
// that represents your article.

// Even though below is configured, the FAQ mod does not use any uploads/images

$config['faq_image_path']       = 'ckfinder/userfiles/images/mh-faq/';
$config['faq_upload_path']      = FCPATH . $config['image_path'];
$config['faq_allowed_types']    = 'gif|jpg|png';
$config['faq_max_size']         = '2048'; // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
$config['faq_max_width']        = '5000'; // The maximum width (in pixels) that the file can be. Set to zero for no limit.
$config['faq_max_height']       = '5000';  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
$config['faq_remove_spaces']    = TRUE;
$config['faq_encrypt_name']     = FALSE;  // if true, the file name will be a random string
$config['faq_overwrite']        = FALSE;


/*
|--------------------------------------------------------------------------
| File
|--------------------------------------------------------------------------
|  Here you can make various settlings to the blog.
*/

$config['file_number_of_posts_per_page']     = 5;   // How many articles/post to display per page in list view
$config['file_number_of_words_to_display']   = 100;   // When listing multiple articles, the number of words to display before ... and READ MORE

// this is the image for "SET NEW BLOG IMAGE".  It is the one pict you select
// that represents your article.

//$config['file_image_path']       = FCPATH . 'inc/themes/file/file_assets';
$config['file_image_path']       = 'ckfinder/userfiles/images/mh-file/';
$config['file_upload_path']      = FCPATH . $config['file_image_path'];
$config['file_allowed_types']    = 'gif|jpg|png|xlsx|xls|doc|pdf|ppt';
$config['file_max_size']         = '2048'; // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
$config['file_max_width']        = '5000'; // The maximum width (in pixels) that the file can be. Set to zero for no limit.
$config['file_max_height']       = '5000';  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
$config['file_remove_spaces']    = TRUE;
$config['file_encrypt_name']     = FALSE;  // if true, the file name will be a random string
$config['file_overwrite']        = FALSE;

/*
|--------------------------------------------------------------------------
| Product - Shoppping Cart
|--------------------------------------------------------------------------
|  Here you can make various settlings to the products.
*/

$config['product_number_of_posts_per_page']     = 5;   // How many articles/post to display per page in list view
$config['product_number_of_words_to_display']   = 100;   // When listing multiple articles, the number of words to display before ... and READ MORE

// this is the image for "SET NEW PRODUCT IMAGE".  It is the one pict you select
// that represents your product.

$config['product_image_path']       = 'ckfinder/userfiles/images/mh-product/';
$config['product_upload_path']      = FCPATH . $config['product_image_path'];
$config['product_allowed_types']    = 'gif|jpg|png';
$config['product_max_size']         = '2048'; // he maximum size (in kilobytes) that the file can be. Set to zero for no limit. Note: Most PHP installations have their own limit, as specified in the php.ini file. Usually 2 MB (or 2048 KB) by default.
$config['product_max_width']        = '5000'; // The maximum width (in pixels) that the file can be. Set to zero for no limit.
$config['product_max_height']       = '5000';  // The maximum height (in pixels) that the file can be. Set to zero for no limit.
$config['product_remove_spaces']    = TRUE;
$config['product_encrypt_name']     = FALSE;  // if true, the file name will be a random string
$config['product_overwrite']        = FALSE;

            
/*
|--------------------------------------------------------------------------
| Website Copyright Clause.  This is added to ever footer in the public 
| section.
|--------------------------------------------------------------------------
*/

$config['copyright_notice']     = "© ".date("Y")." All rights reserved.  www.MovingHaus.com";

/*
|--------------------------------------------------------------------------
| Admin pagination
|--------------------------------------------------------------------------
*/

/* This Application Must Be Used With BootStrap 3 *  */
$config['admin_full_tag_open']    = "<ul class='pagination'>";
$config['admin_full_tag_close']   ="</ul>";
$config['admin_num_tag_open']     = '<li>';
$config['admin_num_tag_close']    = '</li>';
$config['admin_cur_tag_open']     = "<li class='disabled'><li class='active'><a href='#'>";
$config['admin_cur_tag_close']    = "<span class='sr-only'></span></a></li>";
$config['admin_next_tag_open']    = "<li>";
$config['admin_next_tagl_close']  = "</li>";
$config['admin_prev_tag_open']    = "<li>";
$config['admin_prev_tagl_close']  = "</li>";
$config['admin_first_tag_open']   = "<li>";
$config['admin_first_tagl_close'] = "</li>";
$config['admin_last_tag_open']    = "<li>";
$config['admin_last_tagl_close']  = "</li>";
        

/*
|--------------------------------------------------------------------------
| Public pagination
|--------------------------------------------------------------------------
*/
// this is now for bootstrap 4.
$config['public_full_tag_open']        = '<ul class="pagination">';
$config['public_full_tag_close']       = '</ul>';
$config['public_first_link']           = false;
$config['public_last_link']            = false;
$config['public_first_tag_open']       = '<li class="page-item">';
$config['public_first_tag_close']      = '</li>';
$config['public_prev_link']            = '&laquo';
$config['public_prev_tag_open']        = '<li class="page-item prev">';
$config['public_prev_tag_close']       = '</li>';
$config['public_next_link']            = '&raquo';
$config['public_next_tag_open']        = '<liclass="page-item">';
$config['public_next_tag_close']       = '</li>';
$config['public_last_tag_open']        = '<liclass="page-item">';
$config['public_last_tag_close']       = '</li>';
$config['public_cur_tag_open']         = '<li class="page-item active"><a href="page-link">';
$config['public_cur_tag_close']        = '</a></li>';
$config['public_num_tag_open']         = '<li class="page-item">';
$config['public_num_tag_close']        = '</li>';

/*
|--------------------------------------------------------------------------
| Private pagination
|--------------------------------------------------------------------------
*/

/* This Application Must Be Used With BootStrap 3 *  */
$config['private_full_tag_open']    = "<ul class='pagination'>";
$config['private_full_tag_close']   ="</ul>";
$config['private_num_tag_open']     = '<li>';
$config['private_num_tag_close']    = '</li>';
$config['private_cur_tag_open']     = "<li class='disabled'><li class='active'><a href='#'>";
$config['private_cur_tag_close']    = "<span class='sr-only'></span></a></li>";
$config['private_next_tag_open']    = "<li>";
$config['private_next_tagl_close']  = "</li>";
$config['private_prev_tag_open']    = "<li>";
$config['private_prev_tagl_close']  = "</li>";
$config['private_first_tag_open']   = "<li>";
$config['private_first_tagl_close'] = "</li>";
$config['private_last_tag_open']    = "<li>";
$config['private_last_tagl_close']  = "</li>";

/*
|--------------------------------------------------------------------------
| https://www.algolia.com - providing search services and in this case, address
| https://github.com/algolia/places
| Some good documentation:  https://community.algolia.com/places/documentation.html
|--------------------------------------------------------------------------
|  Here you can make various settlings
*/

$config['algolia_application_id']       = 'pl2MHBEEL9O0'; 
$config['algolia_search_only_api_key']  = '3de42e1098f7596a51c7054abfdf15d4'; 