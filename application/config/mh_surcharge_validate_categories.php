<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['validate_surcharge_categories'] =  array(
            // this is the category name
            'surcharge_category_name' => array(
                'field' => 'surcharge_category_name',
                'label' => '',
                'rules' => 'required|trim|xss_clean|min_length[3]|max_length[50]|is_unique[surcharge_categories.surcharge_category_name]',
                'errors' => array(
                    'required'    => 'Cannot be blank',
                    'is_unique'  => 'Category already in use.  Pick another name.',
                    'min_length' => 'Category must be 3 charaters or more.',
                    'max_length' => 'Category must not be more than 50 charaters.'
                ),
            ),
);
    