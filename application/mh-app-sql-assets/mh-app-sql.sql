-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2019 at 06:00 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 5.6.40-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mh_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_timestamp`, `updated_timestamp`) VALUES
(76, 1, 'Associations and Organizations', '2018-01-10 12:57:48', NULL),
(80, 1, '24354234234', '2018-09-17 23:39:41', NULL),
(100, 1, 'aaaaa', '2018-12-11 03:03:40', NULL),
(101, 1, 'movingjapan', '2018-12-31 22:38:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) DEFAULT NULL,
  `created_timestamp` datetime NOT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_id_2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=241 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `category_id`, `user_id`, `updated_user_id`, `title`, `slug`, `body`, `post_image`, `created_timestamp`, `updated_timestamp`) VALUES
(229, 76, 1, 1, 'Customs Duties & Excises - what are these ?ss', 'Customs-Duties-Excises-what-are-these', '<p>Most countries allow personal effects to be imported "duty free" but there are exceptions depending on each country&#39;s respective regulations. <br>\r\n<br>\r\ni.e. non holder of applicable visa in some cases may have to pa<img alt="" src="/ckfinder/userfiles/images/commercial-husband.jpg" xss="removed">y VAT, GST  or customs excise taxes based on the value of the item(s) shipped. ASas</p>', NULL, '2019-02-27 21:44:18', '2019-02-28 00:00:54'),
(230, 76, 1, NULL, 'What is the difference between "Commercial" and "Personal Effects" customs clearances ?', 'What-is-the-difference-between-Commercial-and-Personal-Effects-customs-clearances', '<p><strong>Exports</strong> (from Japan)</p>\r\n\r\n<p><strong>Personal effects and household goods clearances:</strong> Duty and tax free, Shipment must depart within 6 months of shippers(your) departure from Japan.</p>\r\n\r\n<p><strong>Commercial goods clearances:</strong>  Shipment may be considered commercial at destination, and subject to import country`s import duties & taxes. (Personal effects are normally duty free but depends on country)</p>\r\n\r\n<p><strong>Note:</strong> Motor cycles and cars require a  commercial customs clearance.</p>\r\n\r\n<p>Comments:</p>\r\n\r\n<p>1) If sending personal effects and vehicles together, 2 customs clearances (Commercial & Personal efffects) are required.</p>\r\n\r\n<p>2) If goods are put into storage and not shipped within 6 months of customers departure, a commercial customs clearance is required.</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>Imports</strong> (into Japan)</p>\r\n\r\n<p><strong>Personal effects and household goods clearances:</strong> Duty and tax free.</p>\r\n\r\n<p><strong>Commercial goods clearances:</strong>  Shipment is subject to import import duties/excises & taxes. (Duties and tax depends on commodity classification).</p>\r\n\r\n<p>Note: Vehicles genenerally are duty/excise free but subject to 5% consumption tax (based on value)</p>', NULL, '2019-02-27 23:55:02', NULL),
(231, 76, 1, NULL, 'What is AMS, ISF ? and what does it mean ?', 'What-is-AMS-ISF-and-what-does-it-mean', '<p>AMS = Advanced Manifest System &#40;Shipping carriers must report their cargo to destination customs before sailing&#41;. <br>\r\n<br>\r\nISF = Importer Security Filing (Prior to loading into container or onto ship, a counter party at destination must register your cargo into the customs system - a kind of AMS reconciliation hand shake).</p>\r\n\r\n<p>Non compliance can result in fines up to or exceeding US$5,000.</p>\r\n\r\n<p>To comply with the advanced filing requirements a registered customs agent or freight forwarder at destination must be appointed - they also assist with shipping company & customs liaison and can assist with import customs clearance (and transportation where required).</p>', NULL, '2019-02-27 23:55:53', NULL),
(232, 76, 1, NULL, 'What is a Power of Attorney ? Is it needed ?', 'What-is-a-Power-of-Attorney-Is-it-needed', '<p>A power of attorney ("Ininjyo" in Japanese) gives a 3rd party the authority to act on your behalf. As per Japan export regulations it is necessary however the requirement is often waived. In local ports it is likely a signed "ininjyou" will be required in order to complete export customs clearance successfully.</p>', NULL, '2019-02-27 23:56:23', NULL),
(233, 76, 1, NULL, 'Quarantine Inspection - what is this ?', 'Quarantine-Inspection-what-is-this', '<p>Some countries have strict quarantine regulations and will inspect items to ensure they are "safe" to be imported.<br>\r\n<br>\r\nVacuum cleaners, used camping gear, antique furniture, golf clubs, footwear used on farms, car tires etc are items which are often checked and may cause problems. Refer to each country&#39;s relevant customs authority for complete details and restrictions.</p>', NULL, '2019-02-27 23:57:00', NULL),
(234, 76, 1, NULL, 'Can I clear Customs at my destination by myself?', 'Can-I-clear-Customs-at-my-destination-by-myself', '<p>Depending on country, and if sending a consolidated (CFS Container Freight Station)  shipment it may be possible to clear customs yourself.  You will need:</p>\r\n\r\n<p>(1) Packing List & Shipping Invoice</p>\r\n\r\n<p>(2) Unaccompanied Baggage Declaration customs form (Usually obtained by downloading from the government customs webpage).</p>\r\n\r\n<p>(3) Bill of Lading (B/L) and proof of living overseas for "X" number of years.<br>\r\n<br>\r\n<em>Procedurally, this is generally how to clear import customs:</em><br>\r\n<br>\r\n1) Complete the Unaccompanied Baggage Declaration form downloaded from the relevant customs authority prior to arrival.<br>\r\n<br>\r\n2) When exiting through customs at the airport, show the declaration form and receive a stamp (this stamp means you have declared unaccompanied baggage but does not "clear" the cargo for import yet). They will ask questions like how long ago did you purchase the items and how long have you lived overseas. Some countries require you have purchased the items 6 months to 1 year prior to returning. They may ask to see your detailed packing list, so have it handy. <em>Note:</em> Some airport customs staff are unfamiliar with sea imports - you can also do later when the cargo arrives.<br>\r\n<br>\r\n3) Receive "Arrival Notice" from Shipping Agent at destination as per contact information provided to MovingJapan.com when completing online booking in Japan.<br>\r\n<br>\r\n4) Take Arrival Notice, the Bill of Lading (B/L), Packing List and Shipping Invoice, Passport, Unaccompanied baggage form and proof of being in Japan for "x" number of years to the Customs authority (The shipping agent will tell you the location/phone number). The cargo must physically be in the country now to import through Customs. Note: Some countries do not accept visa stamps in your passport as proof of being away. You must have evidence such as tax records, a note from your employer or embassy etc. The Customs Authority will "clear" your import .<br>\r\n<br>\r\n5) Pickup cargo at the warehouse. The shipping agent will give the address and phone number of the warehouse. Take the approval stamp/letter from Customs to the people at the warehouse. Prompt retrieval is advised as storage charges may accrue. <br>\r\n<br>\r\n6) Take your cargo home and unpack.<br>\r\n<br>\r\n<em>Procedures and regulations vary by country. The above information is supplied without guarantee and for informational purposes only. If in doubt, you are recommended to consult your consulate, embassy and relevant customs authority.</em></p>', NULL, '2019-02-27 23:58:02', NULL),
(235, 76, 1, NULL, 'What is a customs inspection ?', 'What-is-a-customs-inspection', '<p>Customs have the right by law to inspect any shipment. This ranges from a simple X-Ray inspection of container, through to container opening and physical inspection. Costs of X-Rays, Physical inspection, associated handling, storage costs etc are billed to your account at cost. * It is important not to send items that will attract intense customs scrutiny & likely penalties.</p>', NULL, '2019-02-27 23:58:38', NULL),
(236, 76, 1, NULL, 'What Problems Will I face at Customs ?', 'What-Problems-Will-I-face-at-Customs', '<p>Each country&#39;s customs regulations are different however provided prohibited items are not shipped, customs nightmares can be avoided. It&#39;s best to check the relevant custom authority "beforehand" to ensure all items being sent comply. Prohibited items may include:<br>\r\n<br>\r\nAlcohol,<br>\r\n<br>\r\nAntique furniture, <br>\r\n<br>\r\n<strong>Food, plants, herbs & spices (anything edible or drinkable inclusive of supplements, vitamins, body building formula, teas, medicines).</strong> <br>\r\n<br>\r\nAny weapon, or items that resemble a weapon. <br>\r\n<br>\r\nFirearms, ammunition and explosives <br>\r\n<br>\r\nHazardous items, such as fireworks, toxic or poisonous substances. <br>\r\n<br>\r\nItems made from animals on the endangered species list (e.g. ivory) <br>\r\n<br>\r\nUnprocessed furs, skins and hunting trophies <br>\r\n<br>\r\nDried flowers, bulbs, seeds, pine cones, etc. <br>\r\n<br>\r\nSoil, sand, straw & hay <br>\r\n<br>\r\nUsed tires, <br>\r\n<br>\r\nPornography <br>\r\n<br>\r\nNarcotics <br>\r\n<br>\r\nSome countries also prohibit importation of cosmetics, make up, perfumes, and scented erazers.</p>', NULL, '2019-02-27 23:59:08', NULL),
(237, 76, 1, NULL, 'What happens if the packing list doesn''t match ?', 'What-happens-if-the-packing-list-doesnt-match', '<p>If the number of items on the packing list is different to the number of items received at the warehouse ~ customs export approval cannot be received and additional handling expenses to remedy the problem will be incurred.</p>', NULL, '2019-02-27 23:59:41', NULL),
(238, 76, 1, NULL, 'ZXZXxzZX', 'ZXZXxzZX', '<p>ZXZxZxZxZXzx</p>', NULL, '2019-02-28 00:35:57', NULL),
(239, 76, 1, NULL, 'ZXZXxzZX ZX ZxxzZxXZ', 'ZXZXxzZX-ZX-ZxxzZxXZ', '<p>ZXZxZxZxZXzxZXxZxZx</p>', NULL, '2019-02-28 00:36:03', NULL),
(240, 102, 1, NULL, 'xcvbxcvbxcvbxcvbxcvb', 'xcvbxcvbxcvbxcvbxcvb', '<p>xczxcvzxcvzxvcb</p>', NULL, '2019-02-28 01:08:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE IF NOT EXISTS `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `user_id`, `name`, `created_timestamp`, `updated_timestamp`) VALUES
(76, 1, 'Customs F.A.Q.', '2018-01-10 12:57:48', '2019-02-27 21:39:31'),
(100, 1, 'General F.A.Q.', '2018-12-11 03:03:40', '2019-02-27 21:38:57'),
(101, 1, 'Payment F.A.Q.', '2018-12-31 22:38:37', '2019-02-27 21:39:55'),
(102, 1, 'Packing F.A.Q.', '2019-02-27 21:38:18', '2019-02-27 21:40:17'),
(103, 1, 'Insurance F.A.Q.', '2019-02-27 21:40:33', NULL),
(104, 1, 'Trucking F.A.Q.', '2019-02-27 21:41:37', NULL),
(105, 1, 'Storage F.A.Q.', '2019-02-27 21:41:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) DEFAULT NULL,
  `created_timestamp` datetime NOT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_id_2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=248 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `category_id`, `user_id`, `updated_user_id`, `title`, `slug`, `body`, `post_image`, `created_timestamp`, `updated_timestamp`) VALUES
(243, 107, 1, 1, 'Moving to Japan - Documents', 'Moving-to-Japan-Documents', '<p>Japan Customs - Customs Form C No.5360-B - CUSTOMS DECLARATION - Declaration of Personal Effects and Unaccompanied Articles.  Import.  English Version.<br>\r\n<br>\r\n<strong><a href="/ckfinder/userfiles/files/mh-file/Japan-docs/english-import-declarion-japan.pdf">Click to download English version!</a></strong><br>\r\n<br>\r\n<strong><a href="/ckfinder/userfiles/files/mh-file/Japan-docs/c5360-c-korean.pdf">Click to download Korean version!</a></strong><br>\r\n<br>\r\n<strong><a href="/ckfinder/userfiles/files/mh-file/Japan-docs/c5360-d-chinese-version-1.pdf">Click to download Chinese version!</a></strong></p>', 'import-documents.png', '2019-03-02 00:49:47', '2019-03-02 05:47:42'),
(244, 107, 1, 1, 'Japan Country Guide', 'Japan-Country-Guide', '<h1>About Japan</h1>\r\n\r\n<p>The land of the Rising Sun!  Japan is written as ?? and translates as &#39;sun origin&#39;.  Here is an overview of Japan:</p>\r\n\r\n<div class="row">\r\n<div class="col-md-6">\r\n<table class="table">\r\n <thead>\r\n  <tr>\r\n   <th scope="col">#</th>\r\n   <th scope="col">First</th>\r\n  </tr>\r\n </thead>\r\n <tbody>\r\n  <tr>\r\n   <th scope="row">Flag</th>\r\n   <td>Mark</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Capital</th>\r\n   <td>Jacob</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Time Zone</th>\r\n   <td>UTC+9 </td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Curreny</th>\r\n   <td>Yen</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Driving Side</th>\r\n   <td>Left</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Calling Code</th>\r\n   <td>+81</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Population</th>\r\n   <td>126,700,000</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Language</th>\r\n   <td>Japanese</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n</div>\r\n\r\n<div class="col-md-6"> </div>\r\n</div>\r\n\r\n<h1>International Schools</h1>\r\n\r\n<p>Japan has a large number of International Schools, however, they are normally located in the metropolises of Japan.  Here is a short list of some.</p>\r\n\r\n<table class="table">\r\n <thead>\r\n  <tr>\r\n   <th scope="col">Name</th>\r\n   <th scope="col">Address</th>\r\n   <th scope="col">Website</th>\r\n   <th scope="col">Phone Number</th>\r\n  </tr>\r\n </thead>\r\n <tbody>\r\n  <tr>\r\n   <th scope="row">The American School in Japan</th>\r\n   <td>1-1-1 Nomizu, Chofu-shi, Tokyo, 182-0031 Japan</td>\r\n   <td><a href="http://www.asij.ac.jp" target="_blank">www.asij.ac.jp</a></td>\r\n   <td>+81-422-34-5300</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">British School in Tokyo (Shibuya Campus)</th>\r\n   <td>1-21-18, Shibuya, Shibuya-ku, Tokyo</td>\r\n   <td><a href="http://www.bst.ac.jp" target="_blank">www.bst.ac.jp</a></td>\r\n   <td>Age 3 - 8 years old (Year 3)</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">DAIZAWA INTERNATIONAL SCHOOL Oyama Campus (Maple - Oak)</th>\r\n   <td>39-20, Oyamacho, Shibuya-ku, Tokyo</td>\r\n   <td><a href="http://www.daizawaschool.com" target="_blank">www.daizawaschool.com</a></td>\r\n   <td>Maple 3-4 years old, Oak 4-6 years old</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">International School of the Sacred Heart ( Kindergarten )</th>\r\n   <td>4-3-1, Hiroo, Shibuya-ku, Tokyo</td>\r\n   <td><a href="https://www.issh.ac.jp/" target="_blank">www.issh.ac.jp</a></td>\r\n   <td>Ages 3 - 5 years old</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">International School of the Sacred Heart (ISSH)</th>\r\n   <td>4-3-1, Hiroo, Shibuya-ku, Tokyo</td>\r\n   <td><a href="https://www.issh.ac.jp/" target="_blank">www.issh.ac.jp</a></td>\r\n   <td>K3 - Grade 12</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Maria&#39;s Babies&#39; Society</th>\r\n   <td>Tomy&#39;s House #101, 3-36-20, Jingumae, Shibuya-ku, Tokyo</td>\r\n   <td><a href="http://www.mariasbabies.co.jp" target="_blank">www.mariasbabies.co.jp</a></td>\r\n   <td>Ages 0 - 6 yrs old</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Pegasus International School</th>\r\n   <td>2F., Ebisu Garden East Bldg., 3-9-20, Ebisu, Shibuya-ku, Tokyo</td>\r\n   <td><a href="http://www.ep-pegasus.com" target="_blank">www.ep-pegasus.com</a></td>\r\n   <td>Ages 15months - 6 years old</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row">Poppins Active Learning International School</th>\r\n   <td>Yebisu Garden Terrace Nibankan 1F., 4-20-2, Ebisu, Shibuya-ku, Tokyo</td>\r\n   <td><a href="http://www.poppins-palis.jp" target="_blank">www.poppins-palis.jp</a></td>\r\n   <td>11 month ~ 5 years old (Before elementary school)</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<h1>About Japan</h1>\r\n\r\n<p>japan it sk klaksd lasda</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<h1>About Japan</h1>\r\n\r\n<p>japan it sk klaksd lasda</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>', 'japan.png', '2019-03-02 00:54:04', '2019-03-02 05:51:03'),
(245, 107, 1, 1, 'Moving From Japan - Documents', 'Moving-From-Japan-Documents', '<p>asdasdasd</p>', 'Export-documents.png', '2019-03-02 03:06:01', '2019-03-02 05:55:54'),
(246, 108, 1, 1, 'aaaasdsdsd', 'aaaasdsdsd', '<p>aaaa</p>', '51696bdb13926a32a67598373b8ba50d.png', '2019-03-02 04:00:43', '2019-03-02 05:03:00'),
(247, 108, 1, NULL, 'aaaasdsdsdasdasdadasd', 'aaaasdsdsdasdasdadasd', '<p>aaaa</p>', '80861cb80715a0267c8c8b08cc965d96.png', '2019-03-02 04:00:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `file_categories`
--

CREATE TABLE IF NOT EXISTS `file_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `file_categories`
--

INSERT INTO `file_categories` (`id`, `user_id`, `name`, `created_timestamp`, `updated_timestamp`) VALUES
(106, 1, 'Moving to Japan - Documents', '2019-03-02 00:47:29', NULL),
(107, 1, 'Japan - Moving to and from ...', '2019-03-02 00:47:44', '2019-03-02 03:04:57'),
(108, 1, 'Corporate Brochures', '2019-03-02 00:47:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'Manager', 'Can do most functions'),
(4, 'xxxx', 'xxxxx'),
(5, 'xxxxxxx', 'xxxxxxx');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mh_fax`
--

CREATE TABLE IF NOT EXISTS `mh_fax` (
  `fax_id` int(7) NOT NULL AUTO_INCREMENT,
  `fax_user_id` int(7) NOT NULL,
  `fax_status` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fax_direction` varchar(50) DEFAULT NULL,
  `fax_from` varchar(100) DEFAULT NULL,
  `fax_to` varchar(100) DEFAULT NULL,
  `fax_price` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `fax_price_unit` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `fax_duration` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `fax_pages` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `fax_quality` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fax_media_url` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `fax_created_timestamp` datetime NOT NULL,
  `fax_updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`fax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mh_settings`
--

CREATE TABLE IF NOT EXISTS `mh_settings` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mh_settings`
--

INSERT INTO `mh_settings` (`key`, `value`) VALUES
('default_graph_version', 'v2.9'),
('facebook_app_id', '2045926465658731'),
('facebook_app_secret', 'b846d1cb2bf72bb2663e76f710b7a2d7'),
('mh_profiler_admin', '0'),
('mh_profiler_private', '0'),
('mh_profiler_public', '0'),
('mh_site_down_admin_message', 'The admin section of the website is down.'),
('mh_site_down_private_message', 'The member section of the website is down for maintenance.  Check back soon!'),
('mh_site_down_public_message', 'The website is down for maintenance.  Check back soon!'),
('mh_site_up_admin', '1'),
('mh_site_up_private', '1'),
('mh_site_up_public', '1'),
('mh_title_admin', 'MovingHaus | Admin'),
('mh_title_private', 'MovingHaus | Private'),
('mh_title_public', 'MovingHaus | Public'),
('mh_twilio_account_sid', 'AC7c428ef8606aee2e1a03d2964b21fe16'),
('mh_twilio_auth_token', '6be085a45cd7f4f37ae35bc16cb9b512'),
('mh_twilio_enable', '1'),
('mh_twilio_phone_number', '+81345402039');

-- --------------------------------------------------------

--
-- Table structure for table `mh_user_settings`
--

CREATE TABLE IF NOT EXISTS `mh_user_settings` (
  `mh_us_id` int(5) NOT NULL AUTO_INCREMENT,
  `mh_us_key` varchar(50) COLLATE utf8_bin NOT NULL,
  `mh_us_value` text COLLATE utf8_bin NOT NULL,
  `mh_us_updated_timestamp` datetime DEFAULT NULL,
  `mh_us_user_id` int(8) NOT NULL,
  PRIMARY KEY (`mh_us_id`),
  UNIQUE KEY `mh_us_id` (`mh_us_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mh_user_settings`
--

INSERT INTO `mh_user_settings` (`mh_us_id`, `mh_us_key`, `mh_us_value`, `mh_us_updated_timestamp`, `mh_us_user_id`) VALUES
(1, 'user_time_zone', 'UP9', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `number`
--

CREATE TABLE IF NOT EXISTS `number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) DEFAULT NULL,
  `number` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `sid` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) DEFAULT NULL,
  `created_timestamp` datetime NOT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_id_2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=230 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `user_id`, `updated_user_id`, `title`, `slug`, `body`, `post_image`, `created_timestamp`, `updated_timestamp`) VALUES
(221, 76, 1, 1, 'Japan has a Lot of Ports', 'Japan-has-a-Lot-of-Ports', '<p>Japan has an disporportionatly large number of ports for its size. Because much of the country is mountainous, that leaves very little land for its population to reside and its industry to grow and hence the extreme importance of its ports. Here is a comparision between land area, number of ports, its population and percent of the land taken up by mountains.</p>\r\n\r\n<table align="center">\r\n <thead>\r\n  <tr>\r\n   <th xss="removed"> </th>\r\n   <th xss="removed">Japan</th>\r\n   <th xss="removed">USA</th>\r\n   <th xss="removed">Australia</th>\r\n   <th xss="removed">China</th>\r\n  </tr>\r\n </thead>\r\n <tbody>\r\n  <tr>\r\n   <th scope="row" xss="removed"># Ports</th>\r\n   <td xss="removed">119</td>\r\n   <td xss="removed">360</td>\r\n   <td xss="removed">215</td>\r\n   <td xss="removed">130</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row" xss="removed">Land Area</th>\r\n   <td xss="removed">378,000 km<sup>2</sup></td>\r\n   <td xss="removed">9,200,000 km<sup>2</sup></td>\r\n   <td xss="removed">7,700,000 km<sup>2</sup></td>\r\n   <td xss="removed">9,400,000 km<sup>2</sup></td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row" xss="removed">Population</th>\r\n   <td xss="removed">127,000,000</td>\r\n   <td xss="removed">325,000,000</td>\r\n   <td xss="removed">25,000,000</td>\r\n   <td xss="removed">1,400,000,000</td>\r\n  </tr>\r\n  <tr>\r\n   <th scope="row" xss="removed">% Mountains</th>\r\n   <td xss="removed">60~80%</td>\r\n   <td xss="removed">20~40%</td>\r\n   <td xss="removed">< 20></td>\r\n   <td xss="removed">40~60%</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><br>\r\nThe Ministry of Land, Infrastucture, Transport and Tourism (MILT) lists 119 open ports in Japan. Japan is listed as the 67th largest country measured by land area with only 378km2 but 11th in poluation with about whopping 127 million. Japan is approximately 73% mountainous which in turn has led to industry and population to concentrate along the coasts and the scattered plains. Its ports are critical to daily life and industry in Japan. </p>', '04a8c0672494d0a38dbfbccd7ff1dcc3.jpg', '2018-02-05 21:30:04', '2019-03-02 04:35:02'),
(222, 76, 1, 1, 'this is a blog', 'this-is-a-blog', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '601ca887c5304b07e97e11b1e8887739.png', '2018-02-23 03:36:18', '2019-03-02 04:46:46'),
(223, 101, 1, NULL, 'yes anotehr post', 'yes-anotehr-post', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2018-02-23 03:36:40', NULL),
(224, 101, 1, NULL, 'yes anotehr post 34', 'yes-anotehr-post-34', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2019-02-23 03:36:51', NULL),
(225, 101, 1, NULL, 'yes anotehr post 34Aas', 'yes-anotehr-post-34Aas', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2019-02-23 03:37:15', NULL),
(226, 101, 1, NULL, 'yes anotehr post 34AasASas', 'yes-anotehr-post-34AasASas', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2019-02-23 03:37:23', NULL),
(227, 101, 1, NULL, 'yes anotehr post 34AasASas  AS as SAas', 'yes-anotehr-post-34AasASas-AS-as-SAas', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2019-01-23 03:37:32', NULL),
(228, 101, 1, NULL, 'ASas  AS as SAas', 'ASas-AS-as-SAas', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, '2019-01-23 03:37:37', NULL),
(229, 101, 1, NULL, 'Japan Customs Doc', 'Japan-Customs-Doc', '<p>This is the customs doc.</p>', 'dcdb67018ae05819fe9090cb614f86e8.png', '2019-03-02 00:20:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', NULL, NULL, NULL, NULL, 1268889823, 1551500610, 1, 'Admin', 'istrator', 'ADMIN', '+818032456954'),
(3, '60.43.40.217', 'test@example.com', '$2y$08$QnGdwSpiQj/X2wVwKmWWHeoDudpwQ1MhUiH6uhb.DHRqc0MdOz2Oy', NULL, 'test@example.com', NULL, NULL, NULL, NULL, 1534062669, NULL, 1, 'sdsdsds sadasd xxxx', 'test2', 'asdfasda', '+81-80-3245-6954'),
(11, '60.43.40.217', NULL, '$2y$08$DUGAUD09M0hRpXfQIX272.gY5acjNf3g8SOIl0VBoWP/G2zFa7anS', NULL, 'stinchcomber@yahoo.com', NULL, NULL, NULL, NULL, 1536055390, NULL, 1, 'Richard', 'Stinchcombe', 'Rickmers-Line (Japan) Inc', '+81-3-5419-1166'),
(12, '60.43.40.217', 'aaaaaa@example.com', '$2y$08$KPgRo9jJ31J4SxwHrdaMluWEYlWP238BUX.rfAQtl653FCBbyOQYi', NULL, 'aaaaaa@example.com', '0a723cf25caabd4f7b92be875d6342b03ee55f4a', NULL, NULL, NULL, 1537512262, NULL, 0, 'aaaaa', 'aaaaaa', 'aaaaaaaa', ''),
(13, '60.43.40.217', 'bbbbbb@example.com', '$2y$08$1tulAVXsQsEAhr7OWnoGAOmZb4ID2cW5RuSBqZouGSta6ZHKn6pam', NULL, 'bbbbbb@example.com', '97718e70ada06a2f0d444861ebae7b9ebb00fed6', NULL, NULL, NULL, 1537512301, NULL, 0, 'bbbbbbbbbbbbbb', 'bbbbbbbbbbbbbb', 'bbbbbbbbbbbbbbbbbbbbbbb', ''),
(14, '60.43.40.217', 'ccccccccccc@example.com', '$2y$08$sSJjupsDYu4iLP8T4dX9E.EF477YzOE6zPuR2.5lxp3QmHCAo9zj.', NULL, 'ccccccccccc@example.com', 'b5025fe64e00f19f159def0819d5b0be86f20530', NULL, NULL, NULL, 1537512328, NULL, 0, 'ccccccccccccccc', 'ccccccccccccccccc', 'ccccccccccccccccccccccccc', ''),
(15, '60.43.40.217', 'fffffffffff@example.com', '$2y$08$EpuQqZi4t8zeD/MGLaBWwO5vDJXncyTU.QA1IlKPN1s1RiMviYXVa', NULL, 'fffffffffff@example.com', 'bc7cb2904f5d3f63d5cf63e743bff425d2633529', NULL, NULL, NULL, 1537512897, NULL, 0, 'fffffffffffffffffffffff', 'fffffffffffffffffff', 'ffffffffffffffffffff', ''),
(16, '60.43.40.217', '111111111111@example.com', '$2y$08$A4ILdhWc0VEZX4EEOjLTgu0t52gdQKAxpwkeQ0g2Z9EKUXAvV0pVq', NULL, '111111111111@example.com', 'cd467181e50f5c0eb85171ebd27bc1022364e5ff', NULL, NULL, NULL, 1541382033, NULL, 0, '1111111111', '11111111111111', '11111111111111111', '111111111111'),
(17, '60.43.40.217', '111111111111@example.com11', '$2y$08$IYC75QQtwmCnjJ.wzkXPCuaQjyq4/THzMaVrRGYz4Jkh9X0O.Vx1S', NULL, '111111111111@example.com11', '96bf2cc788e246979ebdc2c7b6c6f38f0759d5de', NULL, NULL, NULL, 1541382258, NULL, 0, 'xxxxxxxxxx', 'xxxxxxxxxxxx', '11111111111111111', '354191166'),
(18, '60.43.40.217', '111111111111@example.com1111', '$2y$08$mGGAIsNZFO7orjsPe1Ua/OxJaSiM4RLEPm6Na05vlINBpAbW2VACS', NULL, '111111111111@example.com1111', '8c9fa30d64ac5239ef8d72c2f3ee00819b9f8f2a', NULL, NULL, NULL, 1541382275, NULL, 0, '11111111', '11111111111111', '11111111111111111', '354191166'),
(19, '60.43.40.217', '111111111111@example.com11111', '$2y$08$CGeayS.plzTLoIxVZsgvgu1ustQxfRLS2aNsj5eQcIrwZ2n87wkn.', NULL, '111111111111@example.com11111', '9bd28816cdf18bea6662932c25ced9b1b6f754f9', NULL, NULL, NULL, 1541382292, NULL, 0, '11111111', '11111111111111', '11111111111111111', '354191166'),
(20, '60.43.40.217', 'zzzz@example.com', '$2y$08$sN27GO0Ksqs8jNJO1kUW9.77zkPvpO6FYDMssS6aSNVnS3zTtwNIq', NULL, 'zzzz@example.com', NULL, NULL, NULL, NULL, 1541388922, NULL, 1, 'zzzzzzzzzzzzyz', 'zzzzzzzzzzzzzz', 'zzzzzzzzzzzzzzz', 'zzzz'),
(21, '60.43.40.217', '12121211212@example.com', '$2y$08$7aS570jlkXdNkhIaBbR0TO9CgjREnJu.Ftmcmb4xw.enYsrrCHO4K', NULL, '12121211212@example.com', '658f43a93946341e46669b19aefc0d0bec97462a', NULL, NULL, NULL, 1541389452, NULL, 0, '12121211212', '12121211212', '12121211212', '12121211212');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(64, 1, 1),
(65, 1, 2),
(34, 3, 3),
(66, 11, 2),
(45, 12, 2),
(46, 13, 2),
(47, 14, 2),
(48, 15, 2),
(49, 16, 2),
(55, 17, 2),
(51, 18, 2),
(52, 19, 2),
(60, 20, 2),
(61, 20, 3),
(62, 21, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
