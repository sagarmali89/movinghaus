    



<div class="bd-example">
  
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
  
<!-- Carousel Starts Here -->

    <div class="carousel-inner">
      
      <div class="carousel-item active">
        <img class="d-block w-100" src="/ckfinder/userfiles/images/submarine-800-400.jpg" alt="First slide" />
        <div class="carousel-caption d-none d-md-block">
          <h1>Japan Based Husbanding Service Provider for Military</h1>
          <p>Offer tier 2 support for services such as barges, CHT, water, pilots, tugs, lines, tents, generators, etc for all Japan ports</p>
        </div>
      </div>
      
      <div class="carousel-item">
        <img class="d-block w-100" src="/ckfinder/userfiles/images/commercial-husband.jpg" alt="Second slide" />
        <div class="carousel-caption d-none d-md-block">
          <h1>Commerical Husbanding</h1>
          <p>Covering all Japan ports!</p>
          
          Providing full agency and husbandry services in over 120 ports in Japan.  
          Local knowledge and expertise! From vessels and crew handling to cargo and CTM


        </div>
      </div>
      
      <div class="carousel-item">
        <img class="d-block w-100" src="/ckfinder/userfiles/images/tokyo-800-400.jpg" alt="Third slide" />
        <div class="carousel-caption d-none d-md-block">
          <h1>Relocation Services</h1>
          <p>Whether you are moving to Japan or from Japan, we have you covered!</p>
        </div>
      </div>
      
    </div>
    
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    
  </div>
</div>
<br><br>

<!-- Carousel Ends Here -->

<div class="container">
  <div class="row">
    
    <div class="col-sm">
      <h2>Welcome to MH-App</h2>
      <p>Extrodinary vessels, extrodinary missions, extrodinary destinations; 
      MH-App provides the complete range of logistics services, whether at sea, 
      in port, at land or in the air. 
      </p>
     
    </div>
    
    <div class="col-sm">
      It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


    </div>
    
    <div class="col-sm">
      Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
    </div>
  </div>
</div>



