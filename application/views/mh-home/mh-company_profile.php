
<div class="container">
  <div class="row">
    
    <div class="col-sm">
      <h2>Corporate Profile</h2>
      <p>
        
<table class="table table-hover">

  <tbody>
    <tr>
      <th scope="row">Company Name:</th>
      <td>Sailfish Inc</td>
    </tr>
    <tr>
      <th scope="row">HQ:</th>
      <td>TBA</td>
    </tr>
    <tr>
      <th scope="row">Established:</th>
      <td colspan="2">Apr 1, 2019</td>
    </tr>
    <tr>
      <th scope="row">Paid in Capital:</th>
      <td colspan="2">Yen XX,000,0000</td>
    </tr>
    <tr>
      <th scope="row">President</th>
      <td colspan="2">Craig Evans</td>
    </tr>
    <tr>
      <th scope="row">Main Bank:</th>
      <td colspan="2">Mitsui Sumitomo</td>
    </tr>

    <tr>
      <th scope="row">Fiscal Year:</th>
      <td colspan="2">Apr 1 ~ Mar 31</td>
    </tr>
    
    <tr>
      <th scope="row">Business:</th>
      <td colspan="2">
          - Military Husbanding<br>
          - Liner Agency</br>
          - Tramp (Dry) Agency</br>
          - Chartering</br>
          - Brokering</br>
          - Logisitics
      </td>
     </tr>
     
         <tr>
      <th scope="row">Main Shareholders:</th>
      <td colspan="2">
          - ABC Co Ltd (X%)<br>
          - DEF Co Ltd (Y%)</br>
          - GHI Co Ltd (Z%)</br>
          - JKW Co Ltd (A%)</br>
          - LMN Co Ltd (A%)</br>
          - OPQ Co Ltd (A%)</br>
      </td>
     </tr>
     
     <tr>
      <th scope="row">Main Phone:</th>
      <td colspan="2">
         +81-50-5555-5555
      </td>
     </tr>
     
     <tr>
      <th scope="row">Main Fax:</th>
      <td colspan="2">
         +81-50-5555-5555
      </td>
      </tr>
      
      <tr>
      <th scope="row">Email:</th>
      <td colspan="2">
         info@sailfishinc.com
      </td>
      </tr>
      
    
    




  </tbody>
</table>


      </p>
    </div>
    
    <div class="col-sm">
    
    <p><b>Sailfish grow very quickly</b>... and can reach weights of up to 90kgs!  They are 
    the fastest fish in the ocean! Its fighting ability and spectacular 
    aerial acrobatics, along with fast surface runs and agility, have 
    established its reputation as a true figting fish in the sea.  Their 
    enormous sail running down the full length of their back keeps allows them
    to keep their focus on their direction.  When sailfish hunt, they work in 
    teams.  </p>
    
    <h1>We are Sailfish Inc</h1>
    
    <p>
    The Sailfish's characteristics are embodied in Sailfish Inc.  We are 
    young, grow quickly, eager to succeed, fast and nimble and need an equally
    strong team in order to succeed.  We are Sailfish Inc. 
    </p>
    
    </div>
    
    <div class="col-sm">
      <h1>Our Vision</h1>
      <p>Sailfish Inc aims to be a leading independent ship agency in Japan and 
      to set a new standard of personal service.</p>
      
      <p></p>
      
      <h1>Our Mission</h1>
      <p><b><i>What we do?</i></b></p>
      <p>We provide best-in-class intergrated maritime solutions to our 
      esteemed clients and principals from port agency and chandling to 
      stevedoring and port logistics in <u>Japan</u>.</p>
      
       <p><b><i>How do we serve them?</i></b></p>
      <p> We serve with the highest standards of quality, professional 
      integrity and with innovative solutions.</p
    </div>
    
  </div>
</div>



