<div class="container">
 
   <div class="row">
     <h1>Military Agency</h1>
   </div>
  <!-- CHT and Greywater --> 
  <div class="row">
    
    <div class="col-sm"> 
      
      <p>
        <img src="/inc/themes/company-assets/military/cht-barge.jpg" 
        class="img-thumbnail rounded" alt="CHT and Greywater">
      </p> 
      
    </div>
    
    <div class="col-sm">
      
      <b>Collect Holding and Transfer (CHT) and Greywater </b>
      </br>
      </br>
      <p>
          We are able to arrange CHT (waste, sewage, etc) and Greywater (normally water from showers, sinks, etc) services by truck or barge and either at pier or anchor upon ship’s arrival.  We are able to arrange all necessary equipment, personnel and facilities to perform the services including pumps, hoses, and connections compatible with the ships being serviced.  For larger amounts, we are able to propose an offload schedule.  We accurately measure sewage removed from the ship.  We provide suitable selffendering barges to prevent damage to the vessel and with suitable mooring lines to secure alongside the ship.
      </p>

    </div>

  </div>

  <!-- space -->
  <div class="row"> 
  </br>
  </div>
  
  <!-- Trash Removal --> 
  <div class="row">
  
    <div class="col-sm">
      
      <b>Trash Removal</b>
      </br>
      </br>
      <p>
        We are able to arrange Trash Removal services by truck or barge and either at pier or anchor upon ship’s arrival.  We provide tugs & suitable self-fendering barges to prevent damage to the vessel.  We are also able to arrange self-propelled trash removal barges.
      </p>

    </div>
  
  <div class="col-sm">
    
      <p>
        <img src="/inc/themes/company-assets/military/trash-removal-barge.jpg" 
        class="img-thumbnail rounded" alt="Trash Removal by Barge">
      </p> 
      
  </div>
  
  
  </div>
  
  <!-- space -->
  <div class="row"> 
  </br>
  </div>
  
  <!-- Shore and Floating Cranes --> 
  <div class="row">
    
    <div class="col-sm"> 
      
      <p>
        <img src="/inc/themes/company-assets/military/floating-crane.jpg" 
        class="img-thumbnail rounded" alt="Shore Crane and Floating Crane">
      </p> 
      
    </div>
    
    <div class="col-sm">
      
      <b>Shore Cranes and Floating Cranes</b>
      </br>
      </br>
      <p>
          We are able to arrange both shore and floating cranes.  Shore cranes
          are typically limited to approximatley 300mt whereas floating cranes
          up to 700mt are readily available in main ports.  We are able to 
          mobilize both shore and floating cranes to smaller ports.  Cranes
          up to about 1400mt are avaiable but are very limited.  All cranes come
          with operators and a good selection of spreaders, gromets, etc.
          </p>

    </div>

  </div>

  <!-- space -->
  <div class="row"> 
  </br>
  </div>
  
  <!-- Breasting, Fendering and Landing Barges --> 
  <div class="row">
  
    <div class="col-sm">
      
      <b>Breasting, Fendering and Landing Barges </b>
      </br>
      </br>
      <p>
        <i>Breasting barges</i> are usually utilized to breast out water taxis when
        a ship is at anchor.  Suitable fenders are arrange between barge and 
        mother vessel and barge and water taxi.  Mooring lines are also 
        supplied.  We will arrange mobilization and demobilization by way of 
        tug boat.
      </p>
      <p>
        <i>Fendering barges</i> provides additional separation between a large vessel, 
        such as an aircraft carrier, and the pier.  This allows the 
        superstructure of the ship to remain a safe distance from the pier and 
        various pierside obsticles.  Fenedering barges are arranged in the same
        manner as breasting barges.  Various sizes are available.
      </p>
      
      <p>
        <i>Landing barges</i> are utilized when the ship goes to anchor.
        Suitable fendering to breast out from the pier will also be arranged.
        Brows are available.
      </p>
      

    </div>
  
  <div class="col-sm">
    
      <p>
        <img src="/inc/themes/company-assets/military/trash-removal-barge.jpg" 
        class="img-thumbnail rounded" alt="Trash Removal by Barge">
      </p> 
      
  </div>
  
  
  </div>
  
  
  <!-- space -->
  <div class="row"> 
  </br>
  </div>
  
  <!-- Other Services --> 
  <div class="row">
  
    <div class="col-sm">
      
      <b>Other Services Include</b>
      </br>
      </br>
      <p>
      <ul>
  <li>Oily Waste Removal - truck and barge</li>
  <li>Water Ferry Service - shuttle services to/from mother vessel and pier</li>
  <li>Car, Bus, Cargo Van - With/without drivers and English language</li>
  <li>Barriers - Various types of barriers including Jersey type and conex.  
  Shore and water side barriers available</li>
  <li>Pier Side Events - Tents, tables, chairs, podeums, portable toilets,
  lighting, generators</li>
  <li>Stores/Provisions</li>
  <li>Water Supply - potable water both at pier and anchor</li>
  <li>Unarmed Security Guards & Metal and Explosive Detectors</li>
  <li>Brows</li>
  <li>Forklifts & Manlifts</li>
  <li>Oil Booms</li>
  <li>Patrol Boats</li>
  <li>Cell Phones</li>
  <li>Crew Airport Transfer Service - Pickup, Dropoff and visa formalities</li>
  <li>Divers and underwater inspection services</li>
  
</ul>
      </p>
      

    </div>
  
  <div class="col-sm">
    
      <p>
        
      </p> 
      
  </div>
  
  
  </div>
  
  
  
</div> <!-- end of container -->
  