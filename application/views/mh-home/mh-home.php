<div class="bd-example">
  
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
  
<!-- Carousel Starts Here -->

    <div class="carousel-inner">
      
      <div class="carousel-item active">
        <img class="d-block w-100" src="/ckfinder/userfiles/images/submarine-800-400.jpg" alt="First slide" />
        <div class="carousel-caption d-none d-md-block">
          <h1>Japan Based Husbanding Service Provider for Military</h1>
          <p>We offer tier-2 support for services such as barges, CHT, water, pilots, tugs, lines, tents, generators, etc for all Japan ports</p>
        </div>
      </div>
      
      <div class="carousel-item">
        <img class="d-block w-100" src="/ckfinder/userfiles/images/commercial-husband.jpg" alt="Second slide" />
        <div class="carousel-caption d-none d-md-block">
          <h1>Commerical Husbanding</h1>
          <p>Covering all Japan ports!</p>
          
          Providing full agency and husbandry services in over 120 ports in Japan.  
          Local knowledge and expertise! From vessels and crew handling to cargo and CTM.

        </div>
      </div>
      

      
    </div>
    
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    
  </div>
</div>
<br><br>

<!-- Carousel Ends Here -->

<div class="container">
  <div class="row">
    
    <div class="col-sm">
      <h2>Welcome to Sailfish Inc</h2>
      <p>Extrodinary vessels, extrodinary missions, extrodinary destinations; 
      Sailfish provides the complete range of logistics services, whether at sea, 
      in port, at land or in the air for military and commericial interests covering all Japanese ports.
      </p>
      
       <img src="/ckfinder/userfiles/images/mh-logo/logo_transparent_background.png" class="img-thumbnail rounded" alt="Sailfish Inc">
    </div>
    
    
    <div class="col-sm">
    Japan has an disporportionatly large number of ports for its size.  Because much of the country is mountainous, that
    leaves very little land for its population to reside and its industry to grow and hence the extreme importance of its ports.  
    We are you go-to agents for Japan!  
    
     
<table class="table table-condensed">
  
  
      <thead> 
        <tr> 
            <th></th> 
            <th>Japan</th> 
            <th>USA</th> 
            <th>Australia</th> 
            <th>China</th> 
            </tr> 
      </thead>
      
      <tbody> 
        <tr> 
          <th scope=row># Ports</th> 
          <td>119  </td> 
          <td>360 </td> 
          <td>215</td> 
           <td>130</td> 
        </tr> 
        
        <tr> 
          <th scope=row>Land Area</th> 
          <td>378,000 km<sup>2</sup></td> 
          <td>9,200,000 km<sup>2</sup></td> 
          <td>7,700,000 km<sup>2</sup></td> 
           <td>9,400,000 km<sup>2</sup></td> 
        </tr> 
      
        <tr> 
          <th scope=row>Population</th> 
          <td>127,000,000</td> 
          <td>325,000,000</td> 
          <td>25,000,000</td> 
           <td>1,400,000,000</td> 
        </tr> 
        
        <tr> 
          <th scope=row>% Mountains</th> 
          <td>60~80%</td> 
          <td>20~40%</td> 
          <td>< 20%</td> 
           <td>40~60%</td> 
        </tr> 
</table>

 
      
      The Ministry of Land, Infrastucture, Transport and Tourism (MILT) lists 119 open ports in Japan.    
      Japan is listed as the 67th largest country measured by land area with only 378km2 but 11th in poluation 
      with about whopping 127 million.  Japan is approximately 73% mountainous which in turn has led to industry 
      and population to concentrate along the coasts and the scattered plains.  Its ports are critical to daily 
      life and industry in Japan.  
      
      Knowing the local market in the various regions of Japan is essential to the 
      quick despatch and success of any vessel port call. Will 


    </div>
    
    <div class="col-sm">
     
     
     
     
     <div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
  <div class="card-header"><?php echo $this->config->item('mh_city');  ?>, <?php echo $this->config->item('mh_country');  ?></div>
  <div class="card-body">
    <h4 class="card-title">Contact Us!</h4>
       <p class="card-text">
      
     <p><i class="fa fa-phone-square"></i><a href="tel:<?php echo $this->config->item('mh_phone_number');  ?>"><?php echo $this->config->item('mh_phone_number');  ?></a></p>
     <p><i class="fa fa-fax"></i><a href="tel:<?php echo $this->config->item('mh_fax_number');  ?>"><?php echo $this->config->item('mh_fax_number');  ?></a></p> 
     <p><i class="fa fa-envelope-square"></i> japan@sailfishinc.com</p>
     <p><i class="fa fa-clock-o"></i>
        <?php
        $now = new DateTime();
        $now->setTimezone(new DateTimezone('Asia/Tokyo'));
        echo $now->format('M d').' @ '. $now->format('H:i').' hrs';
        ?>
      </p>
     

  </div>
</div>


     
     
     
    </div>
  </div>
</div>



