<?php

/**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     07.28.2018
 *
 * Description: mh-default-template is used for the admin home page.  You can also
 *              use this template for other pages, too.
 *
 * Requirement:
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/xxx/xxx-xxx  // not implemented
 * @filesource
 */
 
defined('BASEPATH') OR exit('No direct script access allowed');

    $this->load->view('templates/private/_blocks/mh_default_header_private'); 
    $this->load->view('templates/private/_blocks/mh_default_navbar_private');
    echo '  <div class="container">
                <div class="row">';
    $this->load->view('templates/private/_blocks/mh_app_sidebar_private');
    $this->load->view($mh_private_view_file);
    echo '      </div>
            </div>';
    $this->load->view('templates/private/_blocks/mh_default_footer_private'); 