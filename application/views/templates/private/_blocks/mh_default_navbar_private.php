<!-- Public Navbar ================================================== -->
      
<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">

  <div class="container">
        
        <a href="../" class="navbar-brand"> <img src="<?php echo $this->config->item('mh_logo_menu');  ?>" alt="<?php echo $this->config->item('mh_company_name');  ?>" height="60" width="91.1"><?php echo $this->config->item('mh_company_name');  ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarResponsive">
        
        <ul class="navbar-nav">
            
          <li class="nav-item">
            <?php echo anchor('Home/company_profile', 'Corporate Profile', array('class' => 'nav-link')); ?>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/Home/military" id="themes">Military Agency<span class="caret"></span></a>
            <div class="dropdown-menu" aria-labelledby="themes">
              <a class="dropdown-item" href="/Home/military">For other services, pls contact us</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/Home/military">Trash Removal</a>
              <a class="dropdown-item" href="/Home/military">Mobile Cranes</a>
              <a class="dropdown-item" href="/Home/military">Oily Waste</a>
              <a class="dropdown-item" href="/Home/military">CHT</a>
              <a class="dropdown-item" href="/Home/military">Passenger Boats</a>
              <a class="dropdown-item" href="/Home/military">Buses, Vans</a>
              <a class="dropdown-item" href="/Home/military">Barriers</a>
              <a class="dropdown-item" href="/Home/military">Tents, Chairs</a>
              <a class="dropdown-item" href="/Home/military">Gangways</a>
              <a class="dropdown-item" href="/Home/military">Forklifts</a>
              <a class="dropdown-item" href="/Home/military">Fenders</a>
              <a class="dropdown-item" href="/Home/military">Potable Water</a>
              <a class="dropdown-item" href="/Home/military">Oil Booms</a>
              <a class="dropdown-item" href="/Home/military">Barges</a>
              <a class="dropdown-item" href="/Home/military">Cell Phones</a>

            </div>
          </li>
 
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/Home/commercial" id="themes">Commercial Agency<span class="caret"></span></a>
          <div class="dropdown-menu" aria-labelledby="themes">
            <a class="dropdown-item" href="/Home/commercial">For other services, pls contact us</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/Home/commercial">Cash to Master</a>
            <a class="dropdown-item" href="/Home/commercial">Crew Changes</a>
            <a class="dropdown-item" href="/Home/commercial">Husbandry</a>
            <a class="dropdown-item" href="/Home/commercial">Port Information</a>
            <a class="dropdown-item" href="/Home/commercial">Protective Agency</a>
            <a class="dropdown-item" href="/Home/commercial">Spare Parts</a>
            <a class="dropdown-item" href="/Home/commercial">Stevedoring</a>
            <a class="dropdown-item" href="/Home/commercial">Chandling</a>
            <a class="dropdown-item" href="/Home/commercial">Cargo Tallying</a>
            <a class="dropdown-item" href="/Home/commercial">Bunkering</a>
          </div>
        </li>
        
        <li class="nav-item">
           <?php echo anchor('Blog_public', 'Blog', array('class' => 'nav-link')); ?>
        </li>
            
        <li class="nav-item">
           <?php echo anchor('Faq_public', 'Faq', array('class' => 'nav-link')); ?>
        </li>
            
        <li class="nav-item">
           <?php echo anchor('File_public', 'Country Files', array('class' => 'nav-link')); ?>
        </li>
          
        <li class="nav-item">
           <?php echo anchor('MH_product_public', 'Shop', array('class' => 'nav-link')); ?>
        </li>    
      </ul>

      
      <ul class="nav navbar-nav ml-auto">
            
        <?php 
        if (!$this->ion_auth->logged_in()) { ?>
                
          <li class="nav-item">
            <?php echo anchor('MH_auth_public/register_user', 'Register', array('class' => 'nav-link')); ?>
          </li>
          <?php 
          
        } 
        ?>
              
        <li class="nav-item">
          <?php //echo anchor('auth/login', 'Sign in', array('class' => 'nav-link')); ?>
        </li>
        
      </ul>

      <ul class="nav navbar-nav navbar-right  ">
        
        <li class="nav-item dropdown">

          <?php 
          if ($this->ion_auth->logged_in()) { ?>

            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download">
              <i class="fa fa-user"></i> My Account <span class="caret"></span>
            </a>
              
            <div class="dropdown-menu animated flipInY" aria-labelledby="download">
              
              <a class="dropdown-item" href="">Welcome to MH-App
              </a>
                
              <div class="dropdown-divider">
              </div>
              
              <a class="dropdown-item" href="/MH_app_private/mh_app_dashboard"><i class="fa fa-user"></i> 
                Sailfish Dashboard
              </a>
              
              <a class="dropdown-item" href="../4/united/bootstrap.css"><i class="fa fa-cog"></i> 
                Sailfish App Settings
              </a>
                
              <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/auth/logout"><i class="fa fa-power-off"></i> Logout</a>
             
                  <?php  
                  if ($this->ion_auth->is_admin()) { ?>
                    <div class="dropdown-divider">
                    </div>
                    
                    <a class="dropdown-item" href="/Admin"><i class="fa fa-unlock"></i> Admin Menu
                    </a>
                  <?php  
                  } 
                  ?>
              </div>
              
              <?php 
            
              } else {  

                echo anchor('auth/login', 'Sign in', array('class' => 'nav-link'));
              
              } 
              ?>
        </li> <!-- end dropdown -->
        
      </ul>
    </div> <!-- end navbarresponsive -->
  </div> <!-- end container -->
</div>  <!-- end navbar --> 