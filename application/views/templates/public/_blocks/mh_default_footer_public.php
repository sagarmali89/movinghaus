<footer id="footer">
  <div class="row">
    <div class="col-lg-12">
      <ul class="list-unstyled">
        
        <li class="float-lg-right">
          <a href="#top">
            Back to top
          </a>
        </li>
            
        <li>  
          <a class="btn btn-social-icon btn-twitter">
            <span class="fa fa-twitter"></span>
          </a>
        </li>
        
        <li>  
          <a class="btn btn-social-icon btn-linkedin">
            <span class="fa fa-linkedin"></span>
          </a>
        </li>
        
        <li>  
          <a class="btn btn-social-icon btn-facebook">
            <span class="fa fa-facebook"></span>
          </a>
        </li>
              
      </ul>
      
      <p>
      <?php 
      echo $this->config->item('mh_company_name').', ';
      echo $this->config->item('mh_address').', ';
      echo $this->config->item('mh_city').', ';
      echo $this->config->item('mh_country').', ';
      echo $this->config->item('mh_postal_code');
      ?>
            
      <i class="fa fa-phone-square"></i> <a href="callto:<?php echo $this->config->item('mh_phone_number');  ?>"><?php echo $this->config->item('mh_phone_number');  ?></a>
      <i class="fa fa-fax"></i> <a href="callto:<?php echo $this->config->item('mh_fax_number');  ?>"><?php echo $this->config->item('mh_fax_number');  ?></a>
      <i class="fa fa-envelope-square"></i><a href="mailto:<?php echo $this->config->item('mh_email');  ?>?Subject=Request%20Estimate"> <?php echo $this->config->item('mh_email'); ?> </a>

      <div class="text-center">
        <?php 
        if (!empty($this->config->item('copyright_notice'))){
            echo $this->config->item('copyright_notice');
        } 
        ?>
      </div>
    </div>
  </div>
  
  <div class="col-sm">
    <a href="/Home/privacy">Privacy Policy</a>
  </div>
  
  <div class="col-sm">
    <a href="/Home/terms_conditions">Terms and Conditions</a>
  </div>

</footer>
      
</div> <!-- end of container -->

<?php
  echo $mh_scripts_public;
?>
</body>
</html>