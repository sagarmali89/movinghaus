<?php

/**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     07.28.2018
 *
 * Description: mh-default-template is used for the hom page.  You can also
 *              use this template for other pages, too.
 *
 * Requirement:
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/xxx/xxx-xxx  // not implemented
 * @filesource
 */
 
defined('BASEPATH') OR exit('No direct script access allowed');

    $this->load->view('templates/public/_blocks/mh_default_header_public'); 
    $this->load->view('templates/public/_blocks/mh_default_navbar_public');
    $this->load->view($mh_public_view_file);
    $this->load->view('templates/public/_blocks/mh_default_footer_public'); 