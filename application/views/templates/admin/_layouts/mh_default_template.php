<?php

/**
 * Name:        MH-App
 * Author:      Richard Stinchcombe
 *              ircnits@gmail.com
 *
 * Created:     07.28.2018
 *
 * Description: mh-default-template is used for the admin home page.  You can also
 *              use this template for other pages, too.
 *
 * Requirement:
 *
 * @package     MH-APP
 * @author      Richard Stinchcombe
 * @link        http://github.com/xxx/xxx-xxx  // not implemented
 * @filesource
 */
 
defined('BASEPATH') OR exit('No direct script access allowed');

    $this->load->view('templates/admin/_blocks/mh_default_header_admin'); 
    
    // hide the menu bar
    if ($this->config->item('mh_profiler_admin')==  FALSE) {
        $this->load->view('templates/admin/_blocks/mh_default_navbar_admin');
    }
    $this->load->view($mh_admin_view_file);
    $this->load->view('templates/admin/_blocks/mh_default_footer_admin'); 