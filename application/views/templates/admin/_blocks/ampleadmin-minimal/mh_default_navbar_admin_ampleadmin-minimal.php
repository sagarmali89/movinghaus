        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Toggle icon for mobile view -->
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="index.html">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon--><img src="/inc/themes/plugins/images/admin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="/inc/themes/plugins/images/admin-logo-dark.png" alt="home" class="light-logo" />
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><img src="/inc/themes/plugins/images/admin-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="/inc/themes/plugins/images/admin-text-dark.png" alt="home" class="light-logo" />
                     </span> </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="/inc/themes/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- .Task dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-check-circle"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="javascript:void(0)">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li>
                    <!-- .Megamenu -->
                    <li class="mega-dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Mega</span> <i class="icon-options-vertical"></i></a>
                        <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Header Title</li>
                                    <li><a href="javascript:void(0)">Link of page</a> </li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Header Title</li>
                                    <li><a href="javascript:void(0)">Link of page</a> </li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Header Title</li>
                                    <li><a href="javascript:void(0)">Link of page</a> </li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Header Title</li>
                                    <li> <a href="javascript:void(0)">Link of page</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- /.Megamenu -->
                </ul>
                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <!-- /.Task dropdown -->
                    <!-- /.dropdown -->
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="/inc/themes/plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Steave</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="/inc/themes/plugins/images/users/varun.jpg" alt="user" /></div>
                                    <div class="u-text"><h4>Steave Jobs</h4><p class="text-muted">varun@gmail.com</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                    <div class="user-profile">
                        <div class="dropdown user-pro-body">
                          <div><img src="/inc/themes/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"></div>
                          <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Steave Gection <span class="caret"></span></a>
                              <ul class="dropdown-menu animated flipInY">
                                <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                              </ul>
                        </div>
                    </div>
                <ul class="nav" id="side-menu">
                    <li><a href="<?php echo base_url('admin');?>" class="waves-effect active"><i data-icon="7" class="fas fa-tachometer-alt"></i><span class="hide-menu"> Dashboard </span></a> </li>
                    <li> <a href="<?php echo base_url('MH_auth_admin/index');?>" class="waves-effect"><i data-icon="/" class="far fa-id-card"></i><span class="hide-menu"> User Management<span class="fa arrow"></span><span class="label label-rouded label-purple pull-right">2</span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('MH_auth_admin/index');?>"><i data-icon=")" class="fas fa-list-ul"></i><span class="hide-menu"> List Users</span></a></li>
                            <li><a href="<?php echo base_url('MH_auth_admin/create_user');?>"><i data-icon=")" class="fas fa-user-plus"></i><span class="hide-menu"> Create New User</span></a></li>
                            <li><a href="<?php echo base_url('MH_auth_admin/index_groups');?>"><i data-icon=")" class="fas fa-list-ul"></i><span class="hide-menu"> List Groups</span></a></li>
                            <li><a href="<?php echo base_url('MH_auth_admin/create_group');?>"><i data-icon=")" class="fas fa-users"></i><span class="hide-menu"> Create New Group</span></a></li>
                             <li><a href="<?php echo base_url('MH_login_attempts_admin/index');?>"><i data-icon=")" class="fas fa-user-secret"></i><span class="hide-menu"> Login Attempts</span></a></li>
                        </ul>
                    </li>
                    
                    <li> <a href="<?php echo base_url('Blog_admin/index');?>" class="waves-effect"><i data-icon="/" class="fas fa-th"></i><span class="hide-menu"> Blog Management<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('Blog_admin/index');?>"><i data-icon=")" class="fas fa-list-ul"></i><span class="hide-menu"> List Posts</span></a></li>
                            <li><a href="<?php echo base_url('Blog_admin/blog_create');?>"><i data-icon=")" class="far fa-plus-square"></i><span class="hide-menu"> Create New Post</span></a></li>
                            <li><a href="<?php echo base_url('Blog_admin/blog_category_index');?>"><i data-icon=")" class="fas fa-list-ul"></i><span class="hide-menu"> List Categories</span></a></li>
                            <li><a href="<?php echo base_url('Blog_admin/blog_category_create');?>"><i data-icon=")" class="far fa-plus-square"></i><span class="hide-menu"> Create New Category</span></a></li>
                        
                        </ul>
                    </li>
                    
                                       <li> <a href="<?php echo base_url('MH_twilio_admin/index');?>" class="waves-effect"><i data-icon="/" class="fas fa-phone"></i><span class="hide-menu"> Phone Management<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('MH_twilio_admin/index');?>"><i data-icon=")" class="fas fa-phone"></i><span class="hide-menu"> Make a call</span></a></li>
                            
                        
                        </ul>
                    </li>
                    
                    <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="" class="fas fa-cogs"></i><span class="hide-menu">Settings<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?php echo base_url('MH_settings_admin/index');?>"><i data-icon=")"  class="fa fa-sliders-h"></i><span class="hide-menu"> Main Settings</span></a></li>
                            <li> <a href="javascript:void(0)"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Main Settings</span></a> </li>
                            <li> <a href="javascript:void(0)"><i data-icon="7" class="fas fa-th"></i><span class="hide-menu"> Blog Settings</span></a> </li>
                            <li> <a href="javascript:void(0)"><i data-icon="7" class="far fa-id-card"></i><span class="hide-menu"> Auth Settings</span></a> </li>
                        </ul>
                    </li>
                    
                    
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->