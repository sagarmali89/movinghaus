<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
    <br>
<br>  
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div> <?php
                        // display any success message
                    echo $this->session->flashdata('message'); ?>
                    </div>
                	<h2>Xrates - Provided by Currconv.com</h2>
                    <hr class="hr-panel-heading" />

                    Exchange rates are handled by <a href="https://currconv.com">CurrConv.com</a>.  
                    Restrictions as follows:
                    
                    * Currency values are refreshed every 60 minutes.
                    * Maximum of 100 requests/hr & 2 query conversions per request
                    * Shared with all users who are acccessing it for free
                    * Unmonitored
                    * Downtime when there's a need to restart the server for bug fixes and enhancements
                   </br> </br>




 
<hr class="hr-panel-heading" />
<?php // echo print_r($ccci_convert_many); 
        // echo '</br></br>'; 
        // echo'value of $i is: '.$i.' and '.$i.' mod 4 is'.($i%4).'</br>';
    ?>
    
    
    
<div class="row">
    <?php
    $i = 0;
    foreach ($ccci_convert_many as $key => $value) {
        $flagA = explode('_', $currency_flag_array[$i]);
        $flag1 = $flagA[0];
        $flagB = explode('_', $currency_flag_array[$i]);
        $flag2 = $flagA[1];
        ?>
        <div class="col-sm-3">
            <div class="counter" align="center">
                    <?php                    
                    echo '<img src="https://www.countryflags.io/'.$flag1.'/flat/64.png">'; echo ' ';
                    echo '<img src="https://www.countryflags.io/'.$flag2.'/flat/64.png">';
                    ?>
                <h2 class="timer count-title count-number" data-to="<?php echo $value; ?>" data-speed="1500"><?php echo $value; ?> </h2>
                <p class="count-text "><b><?php echo $flag1.' to '.$flag2; ?></b></p>
            </div>
        </div>
        <?php
        $i++;
        if (($i%4) == 0){
            echo '</div> </br> <div class="row">';    
        }
    }
    ?>
</div>


<hr class="hr-panel-heading" />
<div class="callout callout-default">
  <h4>Update ccci_xrates Table</h4>
 The ccci_xrates table holds xrates for the above listed currencies.  It is only updated 
 once per day.  The latest xrates are: </br></br>
 
 


  <p>The .table-striped class adds zebra-stripes to a table:</p>   

<div class="col-md-12">  
  <div class="col-md-5"> 
  <table class="table table-striped" id="table1">
    <thead>
      <tr>
        <th>From To Curreny</th>
        <th>Hourly Xrate (not saveed in DB)</th>
        <th>Date/Time</th>
      </tr>
    </thead>
    <tbody>
        <?php
        foreach ($ccci_convert_many as $key => $value) {
        ?> 
      <tr>

        <td><?php echo $key; ?></td>
        <td><?php echo $value; ?></td>
        <td>Now</td>

      </tr>
   
        <?php
        }
        ?>  
    </tbody>
  </table>
  </div>
  
    <div class="col-md-1"> 
    </div> 
  
    <div class="col-md-5">
        
        
          <table class="table table-striped" id="table2">
    <thead>
      <tr>
        <th>From To Curreny</th>
        <th>Xrate Stored in DB</th>
        <th>Date/Time</th>
      </tr>
    </thead>
    <tbody>
        <?php
        foreach ($ccci_get_xrates as $key => $value) {
        ?> 
      <tr>

        <td><?php echo $value['ccci_symbol_to_from']; ?></td>
        <td><?php echo $value['ccci_rate_of_exchange']; ?></td>
        <td><?php 
        
        echo $value['ccci_updated']; ?></td>

      </tr>
   
        <?php
        }
        ?>  
    </tbody>
  </table>
        
        
        
     </div> 
  
</div>








If you want to update the ccci_xrates table with the displayed xrates under their respective flags, 
click the button below.  

    </br></br>
    <a href="/MH_xrates/ccci_update_xrates/" class="btn btn-large btn-primary">Click to Update Xrates table</a>
    
</div>

</br>

<hr class="hr-panel-heading" />
<div class="callout callout-default">
  <h4>Update ccci_contries Table</h4>
 The ccci_contries table holds informaiton about *alpha3, *currenyid, *currencyName, *currencySymbol, 
    *id and *name.  Clicking this button will insert new currencies or update anything 
    for existing currencies.
    </br></br>
    <a href="/MH_xrates/ccci_country_list_update" class="btn btn-large btn-primary">Click to Update Countries table</a>
    
</div>



<hr class="hr-panel-heading" />
<div class="callout callout-default">
  <h4>Usage Statistics</h4>
  You usage this hour is: <b><?php echo $ccci_usage['usage']; ?> polls </b>. (Based on GMT).  You get a maximum of 100 requests/hr & 2 query conversions per request</b>; 
</div>


                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
        <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
