<?php
// echo the pagination links
//echo $links;
?>
<!-- ============================================================== -->
<!-- File Content -->
<!-- ============================================================== -->


<h1>
Country Files <?php if (!empty($category_name)) { echo 'from "'.$category_name.'"'; } ?>
</h1>
    
<div class="row">
    

    <div class="col-9">
        <?php 
        foreach ($files as $file) : 
            if (empty($file['post_image'])) {
                $file['post_image'] = 'noimage.png';
                
        }
        ?>
        <div class="row">
            <div class="col-12"><h3><?php echo $file['title']; ?></h3></div>
            <div class="col-3"><img  class="img-thumbnail" src="<?php if (!empty($file['post_image'])) { echo site_url(); echo  $this->config->item('file_image_path'); echo $file['post_image']; } ?>"> </div>
            <div class="col-9">
                 <small class="created_timestamp">Created on: 
                    <?php 
                    echo $file['created_timestamp']; 
                    if (isset($file['updated_timestamp'])){
                        echo " and Updated on: ";
                        echo $file['updated_timestamp'];
                    }
                    ?>
                    in <strong><?php echo $file['name']; ?></strong>
                </small>
        
                </br>
                    <?php echo word_limiter(strip_tags($file['body']), $this->config->item('number_of_words_to_display')); ?>
                </br>

                </br>
                
                <p>
                    <a class="btn btn-default" href="<?php echo site_url('File_public/file_view_single/'.$file['slug']); ?>">Read More </a>
                </p>
                
            </div>
            
        </div>
        </br></br>
        <?php endforeach ?>
    </div>
    
    
    
    <div class="col-3">
        <div class="row">
            <div class="col-12">
            
            <p>
            <?php
            // If category is set then a filter is applied.
            if (!empty($this->session->userdata('file_category'))){
                echo '<h3>Filter Applied: '.$filter_category_name[0]->name.'</h3>'; 
                ?>
                <button type="button"  onclick="window.location.href='/File_public/clear_file'" class="btn btn-outline-primary">Remove Filter</button>
                <?php
            }
            ?>
            </p>    
            
            <h3>Select Country</h3>
            
 <?php 
                $i = 0;
                
               // print_r($link_data);
                foreach ($link_data as $row) {
                        // if there are faq in the category, show the category
                        // or if no faqs for that category, don't display it.
                        if (!empty($link_data[$i]['number_of_posts'])) {
                            echo '<span class="badge badge-secondary">'.$link_data[$i]['number_of_posts'].'</span>';
                            ?>
                            <a href="<?php echo site_url('File_public/list_file_category/'.$link_data[$i]['id']) ?>">
                            <?php
                            echo $link_data[$i]['name'];  
                            echo '  </a>';
                            
                            echo '</br>';
                        }
                        $i++;
                }
                ?>
                
            </div>
            <div class="col-12"></br></br></br>
            </div>
            <div class="col-12"><h3></h3>
            </div>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- File Content -->
<!-- ============================================================== -->
<?php
// echo the pagination links
echo $links;
?>