
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
    
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">File Management</h4> 
            </div>
           
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">File Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All Files <?php if (!empty($category_name)) { echo 'from "'.$category_name.'"'; } ?></h1>
                    <?php
                    
                	$message = $this->session->flashdata('message');
                	if (isset($message['message'])) {
                			echo '<br>';
                			echo '<div class="alert alert-'.$message['class'].'">';
                			echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  			echo $message['message'];
                			echo '</div>';
                	}?>
                	
                	<p>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('File_admin/file_create/'); ?>"><i class="fa fa-plus"></i> New File</a>
                	</p>
                	
                	
                	<div>
                	
                        <table id="table-mh-admin-file" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				            <thead>
						        <tr>
								    <th>Category</th>
								    <th>Author</th>
								    <th>Title / Post</th>
								    <th>Created</th>
								    <th>Updated</th>
								    <th></th>
							    </tr>
		                    </thead>
						
						    <tbody>	
							<?php foreach ($files as $file) : ?>
							
							                    
                   <?php  if (empty($file['post_image'])) {
                        $file['post_image'] = 'noimage.png';
                    }
                    ?>
							<tr>
							    <?php 
					                // look up user by id from the files table
							        $user = $this->ion_auth->user($file['user_id'])->row_array(); 
							    ?>
							    
							    <td>
							        <?php if (!empty($file['name'])) { echo $file['name']; } ?>
					            </td>
					            
							    <td>
							        <?php if (!empty($user['username'])) { echo $user['username']; } ?>
					            </td>
					            
							    <td>
							        
							        <div class="row">
        
                                    <div class="col-md-3">
                                        <img  class="img-thumbnail" src="<?php if (!empty($file['post_image'])) { echo site_url(); echo  $this->config->item('file_image_path'); echo $file['post_image']; } ?>"> 
                                    </div>
                        
                                    <div class="col-md-9">
							        
				                    <p> 
				                        <b> 
				                            <?php if (!empty($file['title'])) { echo $file['title']; }?> 
				                        </b> 
				                    </p>
							        
							        <p>
					                    <?php echo word_limiter(strip_tags($file['body']), $this->config->item('number_of_words_to_display')); ?> <span class="label label-default label-rouded">
							                 
					                    <a href="<?php echo site_url('File_admin/file_view_single/'.$file['slug']); ?>">Read More ...</a>  
							                 
							         </p>
							         </div>
							         </div>
							    </td>
							    
					           <?php
							        /** created time **/
    							    if (isset($file['created_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($file['created_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>
							   

								<?php
							        /** updated time **/
    							    if (isset($file['updated_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($file['updated_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>
							   
							    
							    
							    <td  style="white-space:nowrap;">
							        <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/File_admin/file_update/slug/<?php echo $file['slug']; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="<?php echo site_url('File_admin/file_view_single/'.$file['slug']); ?>"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('File_admin/file_delete/id/'.$file['id'])?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>
						
						
                    </div>

                    <div class="row">
 
                    </div>    <!-- end of row -->

                    </div> <!-- close white box -->
                
                </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
        <!-- close up page content -->
	    </div> <!-- close fluid container -->
    </div> <!-- close page wrapper -->
