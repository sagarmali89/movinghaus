
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">File Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">File Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                        
                         
                        
                        <?php
                        if (empty($file['post_image'])) {
                            $file['post_image'] = 'noimage.png';
                        }
                        ?>
                        
                        <div class="col-xs-12 col-sm-5">
                            <img class="<?php echo 'img-thumbnail img-responsive'; ?>" src="<?php echo site_url(); echo  $this->config->item('file_image_path'); echo $file['post_image']; ?>" > 
                        </div>                          
                           
                     
                        <h2><b><?php echo $file['title']; ?></b></h2> 
                        
                        <small class="created_timestamp">Created on: 
                            <?php 
                            echo $file['created_timestamp']; 
                            if (isset($file['updated_timestamp'])){
                                echo " and Updated on: ";
                                echo $file['updated_timestamp'];
                            }
                            ?>
                            in <strong><?php echo $file['name']; ?></strong>
                        </small>
                        
                        <br>
                                    
                        <p class="">
                            
                            <?php echo $file['body']; ?>
                        </p>
                            
                            
                        
                            <?php 
                                echo form_open('File/file_delete/id/'.$file['id']); ?> 
                                <a class="btn btn-default" href="<?php echo site_url('File_admin'); ?>">List Posts </a>
                                
                                <a class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit" href="/File_admin/file_update/slug/<?php echo $file['slug']; ?>"><i class="fa fa-edit"></i> Edit</a> 

                                <a href="<?php echo site_url('File_admin/file_delete/id/'.$file['id'])?>" class="btn btn-danger" value="Delete" data-toggle="tooltip" data-placement="top" title="Delete"   onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i> Delete</a>
                            </form>
                            
                            
    


                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
	
	
	
</div> <!-- close page wrapper -->



