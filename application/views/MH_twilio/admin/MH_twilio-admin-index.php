
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Phone Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Phone Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->




<h1>New Phone Call</h1>

<?php
	if (isset($this->data['alert'])){
		echo $this->data['alert'];
	}
?>
										
										
<?php echo form_open('MH_twilio_admin/make_call');?>
   
    <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        Enter Phone Number
	            </label>  
	        
	            <div class="input-group">
	  		    <?php
  			    $phone_number = array(
			        'phone_number'=> 'phone_number',
			        'id'          => 'phone_number',
			        'value'       => set_value('phone_number'),
			        'style'       => '',
			        'class'       => 'form-control',
			        'placeholder' => ''
			    );	
				?>
 		 	    <?php echo form_error('phone_number'); ?>
			    <?php echo form_input($phone_number); ?>
                </div>
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
    
    
        <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        
	            </label>  
	        
	            
	               
	  		    <?php
                echo form_submit('blog_categories_create', 'Save', 'class="btn btn-default btn-lg "');
                
                
                echo form_close();
                ?>
                
                
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
  <?php
  
  //print_r($info);
  ?>
    
    
    
    
    
                

    
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->
