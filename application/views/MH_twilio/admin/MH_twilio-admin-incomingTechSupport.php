<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Technical Support Call Center</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Technical Support Call Center</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>Technical Support Call Center</h1>
                    <!-- ============================================================== -->
                    <!-- White Page -->
                    <!-- ============================================================== -->
                    
    

    <div class="controls">
      
      <div class="client-status">
        Not yet ready.
      </div>

      <p>
        <button class="call" onclick="call();">
          Call
        </button>
      </p>
      

      <p>
        <button class="hang-up">
          Hang up
        </button>
      </p>
      
    </div>

    <p>When a call comes in, you'll receive a popup, allowing you to accept
    the call if you're available.</p>

    <script src="//static.twilio.com/libs/twiliojs/1.3/twilio.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script>
      Twilio.Device.setup("<?php echo $token; ?>");

      Twilio.Device.ready(function(device) {
        $(".client-status").text("Ready.");
      });

      function call(){
        Twilio.Device.connect({
          phone: +818032456954
        });
      }
      
      Twilio.Device.error(function(error) {
        console.debug(error);
        $(".client-status").text("Error: " + error.message);
      });

      Twilio.Device.connect(function(connection) {
        $(".client-status").text("Connected to customer.");
        $("button.hang-up").css("visibility", "visible");
      });

      Twilio.Device.disconnect(function(connection) {
        $(".client-status").text("Ready.");
        $("button.hang-up").css("visibility", "hidden");
      });

      Twilio.Device.incoming(function(connection) {
        if (confirm('Would you like to accept the incoming call?')) {
          connection.accept();
        }
      });

      $("a.hang-up").on("click", function(event) {
        var connection = Twilio.Device.activeConnection();
        connection.disconnect();
      });
    </script>


  
                    <!-- ============================================================== -->
                    <!-- End White Page -->
                    <!-- ============================================================== -->
					</div>	

                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
        </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
