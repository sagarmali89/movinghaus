<div class="controls">
      <div class="client-status">
        Not yet ready.
      </div>

      <p><button class="hang-up">Hang up</button></p>
    </div>
<?php //echo 'token: '.$token; ?>

    <h2>How can we help you?</h2>
    <p>Choose an option and we'll connect you to an agent right away.</p>

    <button class="call" data-customer-support-option="billing">Billing</button>
    <button class="call" data-customer-support-option="tech">Technical support</button>
    <button class="call" data-customer-support-option="sales">Sales</button>

    <script src="//static.twilio.com/libs/twiliojs/1.3/twilio.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script>
      Twilio.Device.setup("<?php echo $token; ?>");

      Twilio.Device.ready(function(device) {
        $(".client-status").text("Ready.");
      });

      Twilio.Device.error(function(error) {
        console.debug(error);
        $(".client-status").text("Error: " + error.message);
      });

      Twilio.Device.connect(function(connection) {
        $(".client-status").text("Connected to customer support.");
        $("button.hang-up").css("visibility", "visible");
      });

      Twilio.Device.disconnect(function(connection) {
        $(".client-status").text("Ready.");
        $("button.hang-up").css("visibility", "hidden");
      });

      $("button.call").on("click", function(event) {
        var option = $(this).attr("data-customer-support-option");
        Twilio.Device.connect({ option: option });
      });

      $("button.hang-up").on("click", function(event) {
        var connection = Twilio.Device.activeConnection();
        connection.disconnect();
      });
    </script>