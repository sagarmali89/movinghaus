<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Login Attempts</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Login Attempts</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>Login Attempts </h1>
                    <!-- ============================================================== -->
                    <!-- White Page -->
                    <!-- ============================================================== -->
                    
                    <!-- this is where our success of failure message is delivered -->
                    <div class="row">
                        <div class="container">
                            <div class="col-sm-12">
                                <div id="status">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
        
     
                        
        
               
        <table class="table table-condensed table-bordered table-striped table-hover" style="margin-bottom: 10px"  id="table-login-attempts">
            <thead>
            <tr>
                <th>No</th>
		        <th>Ip Address</th>
		        <th>Login</th>
		        <th>Time</th>
		        <th></th>
		    </tr>
            </thead>
            
            <tbody>	
            <?php
             $start = '';
            foreach ($MH_login_attempts as $MH_login_attempt)
            {
                ?>
                <tr>
    		      <td><?php echo ++$start ?></td>
    		      <td><?php echo $MH_login_attempt->ip_address ?></td>
    		      <td><?php echo $MH_login_attempt->login ?></td>
    		      <td><?php echo date('l Y/m/d H:i:s', $MH_login_attempt->time); ?></td>
  			    <td  style="white-space:nowrap;">
			       
                <?php $id = $MH_login_attempt->id; ?>
                    <a href="<?php echo site_url('MH_login_attempts_admin/delete/id/'.$id)?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
			    </td>
							    
                </tr>
                <?php
            }
            ?>
            </tbody>	
        </table>
        

                    
            
                    
                    <!-- ============================================================== -->
                    <!-- End White Page -->
                    <!-- ============================================================== -->
					</div>	

                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
        </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
