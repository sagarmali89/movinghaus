<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Login Attempts</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Login Attempts</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All Posts </h1>
                    <!-- ============================================================== -->
                    <!-- White Page -->
                    <!-- ============================================================== -->
                    
                    <!-- this is where our success of failure message is delivered -->
                    <div class="row">
                        <div class="container">
                            <div class="col-sm-12">
                                <div id="status"> 
                                <!-- js message -->
                                junk
                                <!-- end js message -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Open the form to create -->
                    <?php echo form_open('MH_login_attempts_admin/create', 'class="form-horizontal" id="MH_login-attempts-admin-form', $hidden=array() );?>
                   
                    <div class="container">
                        <div class="row">        
                        
                            
                        
                            <!-- id -->
                            <div class="form-group">
                                
                                <label for="id">
                                    Reference Id
                                </label>
                                
                                <div class="input-group">
                                    <?php
                      		        $id = array(
                    		            'name'        => 'id',
                    		            'id'          => 'id',
                    		            'value'        => $this->form_validation->set_value('id'),
                    		            'style'       => '',
                    		            'class'       => 'form-control',
                    		            'placeholder' => '',
                    		            'disabled'    => 'disabled'
                    		        );	
                                    echo form_input($id); 
                                    ?>
                                    
                        		    <div class="input-group-addon">
                        		        <i class="ti-info">
                        		        </i>
                        		    </div>
                        		    
                                </div> <!-- end input group -->
                                
                                <span class="help-block">
                                    The Reference Id is created automatically.
                                </span>
                                
                            </div> <!-- end form-group Id -->
                            

                            
                            <!-- ip_address -->
                            <div class="form-group">
                                
                                <label for="ip_address">
                                    IP Address
                                </label>
                                
                                <div class="input-group">
                                <?php
                  		        $ip_address = array(
                		            'name'        => 'ip_address',
                		            'id'          => 'ip_address',
                		            'value'        => $this->form_validation->set_value('ip_address'),
                		            'style'       => 'max-width:100%',
                		            'class'       => 'form-control input-xlarge',
                		            'placeholder' => '',
                		        );	
                                echo form_input($ip_address); 
                                ?>
                                    <div class="input-group-addon">
                        		        IP
                        		    </div>
                        		    
                                </div>
                                
                            </div> <!-- end form-group ip_address -->
                            
                            
                            <!-- login -->
                            <div class="form-group">
                                
                                <label for="login">
                                    Login Name Used
                                </label>
                                
                                <div class="input-group">
                                <?php
                      		        $login = array(
                    		            'name'        => 'login',
                    		            'id'          => 'login',
                    		            'value'        => $this->form_validation->set_value('login'),
                    		            'style'       => '',
                    		            'class'       => 'form-control',
                    		            'placeholder' => '',
                    		        );	
                                    echo form_input($login); 
                                    ?>
                                    
                                    <div class="input-group-addon">
                        		        <i class="ti-user">
                        		        </i>
                        		    </div>
                        		    
                                </div> <!-- end input group -->
                                
                            </div> <!-- end form-group login -->
                            
                            <!-- submit -->
                           <div class="form-group">
                                <?php echo form_submit('submit', 'Submit'); ?>
                                
                            </div>
                            
                        </div> <!-- end of row -->        
                    </div> <!-- end of container. -->
                    
                    <?php echo form_close(); ?>
                    
                    <!-- ============================================================== -->
                    <!-- End White Page -->
                    <!-- ============================================================== -->
					</div>	

                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
        </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
