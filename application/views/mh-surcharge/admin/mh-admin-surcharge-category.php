
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Surcharge Category Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Surcharge Category Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    	
                    <h2>All Surcharge Categories</h2>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_auth_admin/create_user'); ?>"><i class="fa fa-plus"></i> Add Surcahrges to Surcharge Category</a>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('Surcharge_admin/surcharge_category_create'); ?>"><i class="fa fa-plus"></i> New Surcharge Category</a>
                	
                    
	<hr class="hr-panel-heading" />
                    
                        <?php
                            if ($this->session->flashdata('msg_noti') != '') {
                                echo 
                                    '<div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <p>' . $this->session->flashdata('msg_noti') . '</p>
                                    </div>';
                            } 
                            if ($this->session->flashdata('msg_error') != '') {
                                echo 
                                    '<div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <p>'. $this->session->flashdata('msg_error') . '</p>   
                                    </div>';
                            }
                            
                        ?>
                
                        <table class="table table-condensed table-bordered table-striped table-hover" id="table-mh-admin-surcharge-categoty-index" width="100%">
						   	<thead>
						    <tr>
						        <th></th>
								<th>Surcharge Category Name</th>
								<th>Surcharge Category Comment</th>
								<th>Surcharges</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Action</th>
							</tr>
							</thead>
							
							<tbody>
							<?php foreach ($surcharge_categories as $surcharge_category) : ?>
							<tr>
		
		                        <td></td>
		
							    <!-- surcharge_category_name -->
							    <td>
                                    <?php echo $surcharge_category['surcharge_category_name']; ?>
                                    
                                      <span class="badge badge-secondary"> 
							    			<?php	
							    			if (!empty($surcharge_category['number_of_surcharges'])) { 
							       				echo $surcharge_category['number_of_surcharges']; 
							       				
							       			} elseif (($surcharge_category['number_of_surcharges'])==0) {
							       				echo '0';
							       			}
							       			?> 
							       			Surcharges
							       	</span>
							       	
							       	
							    </td>
							    
				                <!-- surcharge_category_comment -->
							    <td>
                                    <?php echo $surcharge_category['surcharge_category_comment']; ?>
							    </td>
							    
							     <!-- surcharges -->
							    <td>
                                    
							    </td>
							    
							    <!-- created -->
						        <?php
						        /** created time **/
							    if (isset($surcharge_category['created_timestamp'])){
                                    // returns a datetime in gmt
    					            $mydate = strtotime($surcharge_category['created_timestamp']);
    					            
    					            //converts to epoch time in user's timezone.
    					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
    					             
    					            // creates a human readable time    
                                    $user_timezone_human = unix_to_human($user_timezone_epoch);
                                    
                                    echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                               
                                } else {
                                    echo "<td>N/A</td>";
                                }
							    ?>

							    <!-- updated -->
				                <?php
						        /** created time **/
							    if (isset($surcharge_category['updated_timestamp'])){
                                    // returns a datetime in gmt
    					            $mydate = strtotime($surcharge_category['updated_timestamp']);
    					            
    					            //converts to epoch time in user's timezone.
    					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
    					             
    					            // creates a human readable time    
                                    $user_timezone_human = unix_to_human($user_timezone_epoch);
                                    
                                    echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                               
                                } else {
                                    echo "<td>N/A</td>";
                                }
							    ?>


					            <!-- Action -->
							    <td  style="white-space:nowrap;">
                                        <a data-toggle="tooltip" data-placement="top" title="Delete" href="<?php echo site_url('Surcharge_admin/Surcharge_category_delete/id/'.$surcharge_category['surcharge_category_id'])?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    		<a data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-warning btn-sm" href="/Surcharge_admin/surcharge_category_update/<?php echo $surcharge_category['surcharge_category_id']; ?>"><i class="fa fa-edit"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>	
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
        </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
