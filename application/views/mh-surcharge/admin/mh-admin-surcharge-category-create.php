
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Surcharge Category Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Surcharge Category Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->




<h1>New Surcharge Category</h1>

<?php
	if (isset($this->data['alert'])){
		echo $this->data['alert'];
	}
	
?>
										
										
<?php echo form_open('Surcharge_admin/surcharge_category_create');?>
   
    <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        Enter New Surcharge Category
	            </label>  
	        
	            <div class="input-group">
	  		    <?php
  			    $surcharge_category_name = array(
			        'name'        => 'surcharge_category_name',
			        'id'          => 'surcharge_category_name',
			        'value'       => set_value('surcharge_category_name'),
			        'style'       => '',
			        'class'       => 'form-control',
			        'placeholder' => ''
			    );	
				?>
 		 	    <?php echo form_error('surcharge_category_name'); ?>
			    <?php echo form_input($surcharge_category_name); ?>
                </div>
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
        <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        Comment
	            </label>  
	        
	            <div class="input-group">
	  		    <?php
  			    $surcharge_category_comment = array(
			        'name'        => 'surcharge_category_comment',
			        'id'          => 'surcharge_category_comment',
			        'value'       => set_value('surcharge_category_comment'),
			        'style'       => '',
			        'class'       => 'form-control',
			        'placeholder' => ''
			    );	
				?>
 		 	    <?php echo form_error('surcharge_category_comment'); ?>
			    <?php echo form_input($surcharge_category_comment); ?>
                </div>
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
    
    
        <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        
	            </label>  
	        
	            
	               <a class="btn btn-default btn-lg" href="<?php echo site_url('Surcharge_admin/surcharge_category_index'); ?>">List Categories </a> 
	  		    <?php
                echo form_submit('surcharge_categories_create', 'Save', 'class="btn btn-default btn-lg "');
                
                
                echo form_close();
                ?>
                
                
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
    
    
    
    
    
    
                

    
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->
