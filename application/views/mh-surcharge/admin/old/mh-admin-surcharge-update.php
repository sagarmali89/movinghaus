
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Faq Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Faq Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->

                        <h2>Update Faq Entry</h2>        
                    
                        <?php  
                            if (isset($this->data['alert'])) {
                                echo $this->data['alert'];
                            }
                        ?>
                    
                    
                        <?php echo form_open_multipart('Faq_admin/faq_update/slug/'.$this->data['faq']['slug']) ?> 
                        <input type="hidden" name="faq_id" value="<?php echo $this->data['faq']['id']; ?>">
                        <input type="hidden" name="slug" value="<?php echo $this->data['faq']['slug']; ?>">
                        <div class="form-group">
                        <?php
                        $title = array(
                            'name'        => 'title',
                            'id'          => 'title',
                            'value'       => set_value('title', $this->data['faq']['title']),
                            'style'       => '',
                            'class'       => 'form-control input-md',
                            'placeholder' => 'Add Title'
                        );	
                    	?>
                        <?php echo form_error('title'); ?>
                        <?php echo form_input($title); ?>
                        </div>
                      
                        <div class="form-group">
                        <?php
                        $body = array(
                                'name'        => 'body',
                                'id'          => 'editor1',
                                'value'       => html_entity_decode(set_value('body',  $this->data['faq']['body'])), // html_entity_decode renders html in html rather than showing the tags.
                                                                                                                      // this is used because of the rendering of CKEDITOR
                                'rows'        => '3',
                                'cols'        => '',
                                'style'       => '',
                                'class'       => 'form-control input-md',
                                'placeholder' => 'Add Body'
                            );	
                    	        ?>
                     		 	<?php echo form_error('body'); ?>
                    			<?php echo form_textarea($body); ?>
                        </div>
                      
                        <div class="form-group">
                            <?php
                            $options = array();
                            
                            foreach ($categories as $category) {
                                $options[$category['id']] =  $category['name'];
                            }
                            ?>
                            <?php echo form_dropdown('category_id', $options, $this->data['faq']['category_id'], 'class="form-control input-md"' );  ?> 
                        </div>
                        
       
                        
         
                        
                
                        
                         <br><br>
                        
                        <div class="form-group">
                        <?php 
                        echo form_submit('Faq_admin/faq_update', 'Submit', 'class="btn btn-default btn-lg "');
                        echo form_close();
                        ?>
                        </div>

                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->




