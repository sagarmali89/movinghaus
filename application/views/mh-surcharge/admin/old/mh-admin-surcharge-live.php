
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
    
        <div class="row bg-title">
        	
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Surcharge Management</h4> 
            </div>
           
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Surcharge Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All Surcharges</h1>
    	
                	<div>
                	
                        <table id="table-mh-admin-surcharge-index" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				      
				            <thead>
						        <tr>
						        	<th>surcharge_id</th>
								    <th>surcharge_curr</th>
								    <th>surcharge_name</th>
								    <th>surchage_unit </th>
								    <th>surcharge_value</th>
								    <th>surcharge_category_id</th>
								    <th>Action</th>
							    </tr>
		                    </thead>
						
						
						
						    <tbody>	
						    
							</tbody>
							
						</table>
						
						
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                    </div> <!-- close white box -->
                
                </div> <!-- close 12 column width -->
            
            </div> 
            
		</div><!-- close row -->
      
  	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->

