<!-- title -->
<div class="row">
    <div class="col-md-12">
            <h3>Forgot Password</h3> 
    </div>
</div>
<p>Please enter your 
      <?php       
      echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></p>

<!-- show error messages here -->
<?php if($message): ?>
<div class="alert alert-warning">
<?php 
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $message; 
?>
</div>
<?php endif; ?>

<!-- form here --> 
<?php echo form_open("MH_auth_public/forgot_password");?>

      <p>
      	<label for="identity">
      	      <?php 
      	      echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?>                                    
            </label> <br />
            
            
      	 <?php

		        echo form_input($identity);
		      
		 ?>
      </p>

      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),  "class='btn btn-primary'" );?></p>

<?php echo form_close();?>
