
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
<br>
<br>      
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h2>Lead Management</h2>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_auth_admin/create_user'); ?>"><i class="fa fa-plus"></i> New Employee</a>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_auth_admin/create_user'); ?>"><i class="fa fa-plus"></i> New Employee Group</a>
                	
                    
	<hr class="hr-panel-heading" />
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
		  	
							<div class="container text-center">
								<div class="row">
									<div class="col-sm-12">
										<div id="status">
											
										</div>
									</div>
								</div>
							</div>
							


							<?php if (!empty($message)) { ?>
							<?php echo $message;?>
							<?php } ?>
							
							<table class="table table-bordered display responsive nowrap table-striped  table-hover"  id="table-list-users" style="width:100%">
							<!-- table  table-bordered table-hover -->
							
							<?php // <table cellpadding=0 cellspacing=10> ?>
							<thead>
								<tr>
									<th></th>
									<th>ID</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Company</th>
									<th>Last Login</th>
									<th>Created</th>
									<th>Groups</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							
							 <tbody>
								<?php foreach ($users as $user):?>
									<tr>
										<td></td>
										<td id="id">
							            	<?php echo htmlspecialchars($user->id,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="first_name">
							            	<?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="last_name" >
							            	<?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="email">
							            	<?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?>
							            </td>
										<td class="phonenumber" id="phonenumber">
											<?php echo '<a href="ffff">'.htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8').'</a>';?>
								
										</td>
										<td id="company">
											<?php echo htmlspecialchars($user->company,ENT_QUOTES,'UTF-8');?>
										</td>
										
									
										<!-- add tooltip showing how long ago logged in if not null -->
										<?php 
											if (empty($user->last_login)){
										?>		
											<td>
											Never
											</td>
											
										<?php
											} else {
										?>										
										
											<td data-original-title="
												<?php 
													if (isset($user->last_login) ){
														$the_date = unix_to_human(htmlspecialchars($user->last_login,ENT_QUOTES,'UTF-8'));
														$the_text = new Cokidoo_Datetime($the_date);
														$last_login_tooltip = $the_text ;
														echo $the_text;
													}
												?>  
											" data-container="body" data-toggle="tooltip" data-placement="bottom" title="">
												<?php 
												if (!empty($user->last_login)){
													echo unix_to_human(htmlspecialchars($user->last_login,ENT_QUOTES,'UTF-8'));
												}
												?>
										</td>
										
										<?php
											}
										?>
										<!-- end tooltip last login-->
										
										
<!-- CREATED_ON - add tooltip showing how long ago created if not null -->
<?php 
	if (empty($user->created_on)){
?>		
	<td>
	<!-- empty -->
	</td>
	
<?php
	} else {
?>										

	<td data-original-title="
		<?php 
			if (isset($user->created_on) ){
				$the_date_created = unix_to_human(htmlspecialchars($user->created_on,ENT_QUOTES,'UTF-8'));
				$the_text_created = new Cokidoo_Datetime($the_date_created);
				$created_on_tooltip = $the_text_created ;
				echo $created_on_tooltip;
			}
		?>  
	" data-container="body" data-toggle="tooltip" data-placement="bottom" data-order="<?php echo $user->created_on; ?>" title="">
		<?php 
		if (!empty($user->created_on)){
			echo unix_to_human(htmlspecialchars($user->created_on,ENT_QUOTES,'UTF-8'));
		}
		?>
</td>

<?php
	}
?>
<!-- end tooltip created_on-->
										
										<td>
											<?php foreach ($user->groups as $group):?>
									<span class="label label-success label-as-badge">	<?php echo anchor("MH_auth_admin/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?></span><br />
											
							                <?php endforeach?>
										</td>
										<td data-order="<?php echo $user->active; ?>">
							
									

										 <?php
										 // echo form_open("MH_auth_admin/create_user");
										 /*
										 echo $user->active;
				                            $active = array(
				                                'name'          => 'active',
				                                'id'            => 'active',
				                                'value'         => '1',
				                                'checked'       => $user->active,
				                                'style'         => '',
				                                'data-toggle'   => 'toggle',
				                                'data-on'       => 'Active',
				                                'data-off'      => 'Inactive',
				                                'data-onstyle'	=> 'success',
				                                'data-size'     => 'mini',
				                                'data-style'	=> 'ios',
				                            	'onclick'   	=> '<href="/home',
				                            	
				                            	
				                              
				                            );	
				                            */
				                        	?>
				                          
				                            <?php // echo form_checkbox($active); ?>
				                            
				                           	<?php // echo form_close();?>
											
											<?php 
										
											if ($user->active == 1) { 
											?> 
											<a href="/MH_auth_admin/toggle_activate/<?php echo $user->id.'/'.$user->active; ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Deactivate" onclick="return confirm('Are you sure?  This will only deactivate the account.')"><i class="far fa-smile"></i></a>
											
											<?php
											} else {
											?>
											<a href="/MH_auth_admin/toggle_activate/<?php echo $user->id.'/'.$user->active; ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Activate" onclick="return confirm('Are you sure?  This will activate the account.')"><i class="far fa-frown"></i></a>
											<?php
											}
											?>
											
											<?php
												//echo 
												//($user->active) 
												//? 
												//anchor("MH_auth_admin/deactivate/".$user->id, ' ', ('class="far fa-circle"')) 
												//: 
												//anchor("MH_auth_admin/activate/". $user->id, ' ', ('class="far fa-times-circle"'))
												//;
											?>
											
											
										</td>
										
										
										<td>
											
									<a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/MH_auth_admin/edit_user/<?php echo $user->id; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="/MH_auth_admin/view_user/<?php echo $user->id; ?>"><i class="fa fa-eye"></i></a>
                                    
                                    	
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
							</table>
							
							<p>
							<?php
							echo $this->pagination->create_links();
							?>	
							</p>
							
							
					<!-- ============================================================== -->
					<!-- Close ion-auth Content -->
					<!-- ============================================================== -->
<style>
.console-line {
  font-family: monospace;
  margin: 2px;
}
</style>

 



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
