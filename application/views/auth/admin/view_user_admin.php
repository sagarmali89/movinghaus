
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">User Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">User Details</h3> 
	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
					
					
					
                    <!-- <h1><?php //echo lang('edit_user_heading');?> User Details</h1> -->
                    <!-- <p><?php //echo lang('edit_user_subheading');?></p> -->

                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2>Name : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo $user->first_name." ".$user->last_name; ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2>Company : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo $user->company ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2>Phone : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo $user->phone ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2> Moving From : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo $user->loading_zones ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2> Moving To : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo $user->rate_to ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2> Expected Date : </h2>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                            <h3><?php echo date('D, d M Y', strtotime($user->expected_date)) ?></h3>
                        </div>
                    </div>

                       
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12"> 
                            <h2> User Groups : </h2>
                        </div>
                        <!-- groups -->
                            <?php if ($this->ion_auth->is_admin()): ?>

                            <?php foreach ($groups as $group):?>
                        <?php
                            $gID=$group['id'];
                            $checked = null;
                            $item = null;
                            foreach($currentGroups as $grp) {
                                if ($gID == $grp->id) {
                                    $checked= ' checked="checked" readonly';
                                    break;
                                }
                            }
                        ?>
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="checkbox checkbox-success">   
                                    <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                                    <label class="checkbox">
                                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                                    </label>
                                </div>
                            </div> <!-- end of groups -->
                        </div>
                        <?php endforeach?>
                        <?php endif ?>                                                
                    </div>
                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
