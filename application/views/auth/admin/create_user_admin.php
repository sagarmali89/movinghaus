<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">User Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">User Management</h3> 

                    <!-- ============================================================== -->
                    <!-- ion-auth Content -->
                    <!-- ============================================================== -->

                    <h1><?php echo lang('create_user_heading');?></h1>
                    <p><?php echo lang('create_user_subheading');?></p>


                    <!-- show error messages -->
                    <?php if($message): ?>
                        <div class="alert alert-warning">
                            <?php 
                            echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $message; 
                            ?>
                        </div>
                    <?php endif; ?>






                    <?php echo form_open("MH_auth_admin/create_user");?>
                    <div class="row">
                        <!-- first_name -->
                        <div class="col-md-6 col-sm-12 col-xs-12">   
                            <div class="form-group">
                                
                                <label for="first_name">
                                    First Name (*)
                                </label>
                                
                                <div class="input-group">
                                <?php
                                    $first_name = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'        => $this->form_validation->set_value('first_name'),
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'First Name',
                                    );  
                                    echo form_input($first_name); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-user"></i>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <!-- last_name -->
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                
                                <label for="last_name">
                                    Last Name (*)
                                </label>
                                
                                <div class="input-group">
                                    <?php
                                    $last_name = array(
                                    'name'        => 'last_name',
                                    'id'          => 'last_name',
                                    'value'        => $this->form_validation->set_value('last_name'),
                                    'style'       => '',
                                    'class'       => 'form-control',
                                    'placeholder' => 'Last Name',
                                    );  
                                    echo form_input($last_name); 
                                    ?>
                                    <div class="input-group-addon">
                                        <i class="ti-user">
                                        </i>
                                    </div>
                                </div>
                            </div>
                                              
                            <?php
                            if($identity_column!=='email') {
                                echo '<p>';
                                echo lang('create_user_identity_label', 'identity');
                                echo '<br />';
                                echo form_error('identity');
                                echo form_input($identity);
                                echo '</p>';
                            }
                            ?>
                        </div>

                        <!-- company -->
                        <div class="col-md-6 col-sm-12 col-xs-12 col-off">                
                            <div class="form-group">
                                <label for="company">
                                    Company Name
                                </label>
                                <div class="input-group">
                                <?php
                                    $company = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'        => $this->form_validation->set_value('company'),
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'Company Name',
                                    );  
                                    echo form_input($company); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-home"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!-- Moving from -->
                        <div class="col-md-6 col-sm-12 col-xs-12">                
                            <div class="form-group">
                                <label for="moving_from">
                                    Moving From (*)
                                </label>
                                <div class="form-group">
                                <?php

                                    $loadingZonesData = $this->data['loadingZones']; 
                                    $loadingZones = array();
                                    $loadingZones[""] = "select loading zone";
                                    foreach ($loadingZonesData as $zones) {
                                        $loadingZones[$zones->loading_zone_id] = $zones->loading_zone_name;
                                    }

                                    $moving_from = array(
                                        'name'        => 'moving_from',
                                        'id'          => 'moving_from',
                                        'style'       => '',
                                        'value'        => $this->form_validation->set_value('moving_from'),
                                        'class'       => 'form-control',
                                        'placeholder' => 'Moving From',
                                        'options' => $loadingZones
                                    );  
                                    echo form_dropdown($moving_from); 
                                ?>
                                </div>
                            </div>
                        </div> 

                        <!-- Moving from -->
                        <div class="col-md-6 col-sm-12 col-xs-12">                
                            <div class="form-group">
                                <label for="moving_from">
                                    Moving To (*)
                                </label>
                                <div class="form-group">
                                <?php


                                    $movingToOptsData = $this->data['movingTo']; 
                                    $movingToOpts = array();
                                    $movingToOpts[""] = "select moving to";
                                    foreach ($movingToOptsData as $moveto) {
                                        $movingToOpts[$moveto->rate_id] = $moveto->rate_to;
                                    }

                                    

                                    $moving_to = array(
                                        'name'        => 'moving_to',
                                        'id'          => 'moving_to',
                                        'style'       => '',
                                        'value'        => $this->form_validation->set_value('moving_to'),
                                        'class'       => 'form-control',
                                        'placeholder' => 'Moving From',
                                        'options' => $movingToOpts
                                    );  
                                    echo form_dropdown($moving_to); 
                                ?>
                                </div>
                            </div>
                        </div>

                        <!-- Moving from -->
                        <div class="col-md-6 col-sm-12 col-xs-12">                
                            <div class="form-group">
                                <label for="moving_from">
                                    When Do You Expect To Move (*)
                                </label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <?php

                                                $monthsOpts = array(
                                                    ''      => "Month",
                                                    'Jan'   => "Jan",
                                                    'Feb'   => "Feb",
                                                    'Mar'   => "Mar",
                                                    'Apr'   => "Apr",
                                                    'May'   => "May",
                                                    'Jun'   => "Jun",
                                                    'Jul'   => "Jul",
                                                    'Aug'   => "Aug",
                                                    'Sep'   => "Sep",
                                                    'Oct'   => "Oct",
                                                    'Nov'   => "Nov",
                                                    'Dec'   => "Dec"
                                                );

                                                $expected_month = array(
                                                    'name'        => 'expected_month',
                                                    'id'          => 'expected_month',
                                                    'style'       => '',
                                                    'value'        => $this->form_validation->set_value('expected_month'),
                                                    'class'       => 'form-control',
                                                    'placeholder' => 'Month',
                                                    'options' => $monthsOpts
                                                );  
                                                echo form_dropdown($expected_month); 
                                            ?>  
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <?php
                                                $yearsOpts = array();
                                                $yearsOpts[''] = 'Year';
                                                for($i=date('Y');$i<=date('Y', strtotime('+10 years'));$i++){
                                                    $yearsOpts[$i] = $i;
                                                }
                                                $expected_year = array(
                                                    'name'        => 'expected_year',
                                                    'id'          => 'expected_year',
                                                    'style'       => '',
                                                    'value'        => $this->form_validation->set_value('expected_year'),
                                                    'class'       => 'form-control',
                                                    'placeholder' => 'Month',
                                                    'options' => $yearsOpts
                                                );  
                                                echo form_dropdown($expected_year); 
                                            ?>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="row">
                        <!-- email -->
                        <div class="col-md-6 col-sm-12 col-xs-12">                
                            <div class="form-group">
                                <label for="email">
                                    Email (*)
                                </label>
                                <div class="input-group">
                                <?php
                                    $email = array(
                                        'name'        => 'email',
                                        'id'          => 'email',
                                        'value'        => $this->form_validation->set_value('email'),
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'Email',
                                    );  
                                    echo form_input($email); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-email"></i>
                                    </div>
                                </div>
                            </div>
                        </div>         

                        <!-- phone -->
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="phone">
                                    Phone 
                                </label>
                                <div class="input-group">
                                <?php
                                    $phone = array(
                                        'name'        => 'phone',
                                        'id'          => 'phone',
                                        'value'       => $this->form_validation->set_value('phone'),
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'Phone',
                                    );  
                                    echo form_input($phone); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-mobile"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- password -->
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="phone">
                                    Password (*)
                                </label>
                                <div class="input-group">
                                <?php
                                    $password = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'type'        => 'password',
                                        'value'       => '',
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'Password',
                                    );  
                                    echo form_input($password); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-lock"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- password_confirm-->
                        <div class="col-md-6 col-sm-12 col-xs-12">                              
                            <div class="form-group">
                                <label for="phone">
                                    Confirm Password (if changing password) (*)
                                </label>
                                <div class="input-group">
                                <?php
                                    $password_confirm = array(
                                        'name'        => 'password_confirm',
                                        'id'          => 'password_confirm',
                                        'type'        => 'password',
                                        'value'       => '',
                                        'style'       => '',
                                        'class'       => 'form-control',
                                        'placeholder' => 'Password (if changing password)',
                                    );  
                                    echo form_input($password_confirm); 
                                    ?>
                                    <div class="input-group-addon"><i class="ti-lock"></i>
                                    </div>
                                </div>
                            </div>
                            <p><?php echo form_submit('submit', lang('create_user_submit_btn'), "class='btn btn-primary'" ); ?></p>
                        </div>
                    </div>                  
                    <?php echo form_close();?>

                    <!-- ============================================================== -->
                    <!-- Close ion-auth Content -->
                    <!-- ============================================================== -->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->

        <!-- close up page content -->
    </div> <!-- close fluid container -->
</div> <!-- close page wrapper -->


