
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">User Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">User Management</h3> 
	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
					
					
					
<h1><?php echo lang('edit_user_heading');?></h1>
<p><?php echo lang('edit_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open(uri_string());?>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12"> 
        <!-- first_name -->
        <div class="form-group">
            <label for="first_name">
                First Name
            </label>
            <div class="input-group">
            <?php
  		        $first_name = array(
		            'name'        => 'first_name',
		            'id'          => 'first_name',
		            'value'        => $user->first_name,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'First Name',
		        );	
                echo form_input($first_name); 
                ?>
    		    <div class="input-group-addon"><i class="ti-user"></i>
    		    </div>
            </div>
        </div>
    </div>

    <!-- last_name -->
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="last_name">
                Last Name
            </label>
            <div class="input-group">
            <?php
  		        $last_name = array(
		            'name'        => 'last_name',
		            'id'          => 'last_name',
		            'value'        => $user->last_name,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Last Name',
		        );	
                echo form_input($last_name); 
                ?>
    		    <div class="input-group-addon"><i class="ti-user"></i>
    		    </div>
            </div>
        </div>
    </div>
        
    <!-- company -->
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="company">
                Company Name
            </label>
            <div class="input-group">
            <?php
  		        $company = array(
		            'name'        => 'company',
		            'id'          => 'company',
		            'value'        => $user->company,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Company Name',
		        );	
                echo form_input($company); 
                ?>
    		    <div class="input-group-addon"><i class="ti-home"></i>
    		    </div>
            </div>
        </div>
    </div>

    <!-- phone -->
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="phone">
                Phone 
            </label>
            <div class="input-group">
            <?php
  		        $phone = array(
		            'name'        => 'phone',
		            'id'          => 'phone',
		            'value'        => $user->phone,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Phone',
		        );	
                echo form_input($phone); 
                ?>
    		    <div class="input-group-addon"><i class="ti-mobile"></i>
    		    </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Moving from -->
    <div class="col-md-6 col-sm-12 col-xs-12">                
        <div class="form-group">
            <label for="moving_from">
                Moving From (*)
            </label>
            <div class="form-group">
            <?php

                $loadingZonesData = $this->data['loadingZones']; 
                $loadingZones = array();
                $loadingZones[""] = "select loading zone";
                foreach ($loadingZonesData as $zones) {
                    $loadingZones[$zones->loading_zone_id] = $zones->loading_zone_name;
                }

                $moving_from = array(
                    'name'        => 'moving_from',
                    'id'          => 'moving_from',
                    'style'       => '',
                    'selected'        => [$user->fk_loading_zones],
                    'class'       => 'form-control',
                    'placeholder' => 'Moving From',
                    'options' => $loadingZones
                );  
                echo form_dropdown($moving_from); 
            ?>
            </div>
        </div>
    </div> 

    <!-- Moving from -->
    <div class="col-md-6 col-sm-12 col-xs-12">                
        <div class="form-group">
            <label for="moving_from">
                Moving To (*)
            </label>
            <div class="form-group">
            <?php


                $movingToOptsData = $this->data['movingTo']; 
                $movingToOpts = array();
                $movingToOpts[""] = "select moving to";
                foreach ($movingToOptsData as $moveto) {
                    $movingToOpts[$moveto->rate_id] = $moveto->rate_to;
                }

                

                $moving_to = array(
                    'name'        => 'moving_to',
                    'id'          => 'moving_to',
                    'style'       => '',
                    'selected'        => [$user->fk_rates],
                    'class'       => 'form-control',
                    'placeholder' => 'Moving From',
                    'options' => $movingToOpts
                );  
                echo form_dropdown($moving_to); 
            ?>
            </div>
        </div>
    </div>

    <!-- Moving from -->
    <div class="col-md-6 col-sm-12 col-xs-12">                
        <div class="form-group">
            <label for="moving_from">
                When Do You Expect To Move (*)
            </label>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <?php

                            $monthsOpts = array(
                                ''      => "Month",
                                'Jan'   => "Jan",
                                'Feb'   => "Feb",
                                'Mar'   => "Mar",
                                'Apr'   => "Apr",
                                'May'   => "May",
                                'Jun'   => "Jun",
                                'Jul'   => "Jul",
                                'Aug'   => "Aug",
                                'Sep'   => "Sep",
                                'Oct'   => "Oct",
                                'Nov'   => "Nov",
                                'Dec'   => "Dec"
                            );

                            $expected_month = array(
                                'name'        => 'expected_month',
                                'id'          => 'expected_month',
                                'style'       => '',
                                'selected'        => [date('M', strtotime($user->expected_date))],
                                'class'       => 'form-control',
                                'placeholder' => 'Month',
                                'options' => $monthsOpts
                            );  
                            echo form_dropdown($expected_month); 
                        ?>  
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <?php
                            $yearsOpts = array();
                            $yearsOpts[''] = 'Year';
                            for($i=date('Y');$i<=date('Y', strtotime('+10 years'));$i++){
                                $yearsOpts[$i] = $i;
                            }
                            $expected_year = array(
                                'name'        => 'expected_year',
                                'id'          => 'expected_year',
                                'style'       => '',
                                'selected'        => [date('Y', strtotime($user->expected_date))],
                                'class'       => 'form-control',
                                'placeholder' => 'Month',
                                'options' => $yearsOpts
                            );  
                            echo form_dropdown($expected_year); 
                        ?>  
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">    
        <!-- password -->
        <div class="form-group">
            <label for="phone">
                Password 
            </label>
            <div class="input-group">
            <?php
  		        $password = array(
		            'name'        => 'password',
		            'id'          => 'password',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password',
		        );	
                echo form_input($password); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
    </div>
    <!-- password_confirm-->
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="phone">
                Confirm Password (if changing password)
            </label>
            <div class="input-group">
            <?php
  		        $password_confirm = array(
		            'name'        => 'password_confirm',
		            'id'          => 'password_confirm',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password (if changing password)',
		        );	
                echo form_input($password_confirm); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
    </div>
</div>    
<div class="row">
    <!-- groups -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">        
        <?php if ($this->ion_auth->is_admin()): ?>
            <label for="phone">
                Select Group.  
            </label>
    </div>
        <?php foreach ($groups as $group):?>
    <?php
        $gID=$group['id'];
        $checked = null;
        $item = null;
        foreach($currentGroups as $grp) {
            if ($gID == $grp->id) {
                $checked= ' checked="checked"';
                break;
            }
        }
    ?>
    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="checkbox checkbox-success">   
                <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                <label class="checkbox">
                    <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                </label>
            </div>
        </div> <!-- end of groups -->
    </div>
    <?php endforeach?>
    <?php endif ?>                                                
</div>
                                    


      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <p><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></p>

<?php echo form_close();?>
				
<!--------------------------->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
