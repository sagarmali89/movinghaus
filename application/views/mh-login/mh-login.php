<div class="container">

  <div class="row">
    <div class="col">
        <h1><?php echo lang('login_heading');?></h1>
        <p><?php echo lang('login_subheading');?></p>
    </div> <!-- end of left side -->
    
    <div class="col">
      <?php
        $helper         = $this->fb->getRedirectLoginHelper();
        $permissions    = ['public_profile','email']; // these are the permissions we ask from the Facebook user's profile
        $theUrl         = base_url();
        // <!-- show the facebook login logo -->
        // You can read about getLoginUrl here:  http://fbdevwiki.com/wiki/PHP_getLoginUrl
        echo anchor($helper->getLoginUrl($theUrl.'Auth_facebook/facebook', $permissions), 
                img(array(
                    'src'       =>'inc/themes/default/images/facebook-login-button.png',
                    'border'    =>'0',
                    'alt'       =>'Login with Facebook', 
                    'class'     => 'img-fluid', 
                    'width'     => '50%',
                    'height'    => '50%'
              )));
        ?>
    </div>  <!-- end of right -->
    
  </div> <!-- end of row -->
  
  <div class="row">
    
    <div class="col">  <!-- left side -->
     Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
     when an unknown printer took a galley of type and scrambled it to make a 
     type specimen book. It has survived not only five centuries, but also the 
     leap into electronic typesetting, remaining essentially unchanged. It was 
     popularised in the 1960s with the release of Letraset sheets containing 
     Lorem Ipsum passages, and more recently with desktop publishing software 
     like Aldus PageMaker including versions of Lorem Ipsum.
    </div> <!-- end of left side -->
    
    <div class="col"> <!-- right side -->

        <?php echo form_open("auth/login");?>
        
        <div id="infoMessage">
          
          <?php
          if (isset($message)) {
          ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Holy guacamole!</strong> <?php echo $message;?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php
          } 
          ?>

        </div>
             
        <!-- text input -->   
        <div class="form-group">
          <label for="identity" >Email address</label>
          <?php
            $identity = array(
              'name'        => 'identity',
              'id'          => 'identity',
              'type'        =>'email',
              'aria-describedby' => 'emailHelp',
              'value'       => $this->input->post('identity'),
              'style'       => '',
              'class'       => 'form-control',
              'placeholder' => 'Enter email'
            );	
            echo form_input($identity)
          ?>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        
        <!-- text input / password -->   
        <div class="form-group">
          <label for="identity" >Password</label>
          <?php
            $password = array(
              'name'        => 'password',
              'id'          => 'password',
              'type'        =>'password',
              'aria-describedby' => 'passwordHelp',
              'value'       => $this->input->post('password'),
              'style'       => '',
              'class'       => 'form-control',
              'placeholder' => 'Password'
            );	
            echo form_input($password)
          ?>
         
        </div>
        
        
        
        <div class="form-group">
            <?php echo lang('login_remember_label', 'remember');?>
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
        </div>
        
        <div>
        <?php
          echo form_submit(array(
            'id'      => 'submit', 
            'value'   => lang('login_submit_btn'), 
            'class'   => 'btn btn-primary'
          ));
        ?>
        </div>        

        
        <?php echo form_close();?>
        
        <div>
          <a href="/MH_auth_public/forgot_password"><?php echo lang('login_forgot_password');?></a>
        </div>
        
    </div>  <!-- end of right -->
    
  </div> <!-- end of row -->
  
</div><!-- end of container -->
