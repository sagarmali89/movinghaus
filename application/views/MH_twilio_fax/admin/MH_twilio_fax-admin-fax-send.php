
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Fax Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Fax Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->
                        
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Send Fax
                        </div>
                        
                        
                           <!-- show error messages -->
                            <?php if($message): ?>
                            <div class="alert alert-warning">
                                <?php 
                                    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                                    echo $message; 
                                ?>
                            </div>
                            <?php endif; ?>
                            
                            
                        <?php 
                        // Open the form
                        echo form_open("MH_twilio_fax_admin/fax_send");
                        ?>
                    
                        </br>
                        
                        <!-- fax_to -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-fax"></i>
                                </div>
                                <?php
                          		$fax_to = array(
            		                'name'        => 'fax_to',
            		                'id'          => 'fax_to',
            		                'value'       => $this->form_validation->set_value('fax_to'),
            		                'style'       => '',
            		                'class'       => 'form-control',
            		                'placeholder' => 'Fax To...',
            		            );	
                                echo form_input($fax_to); 
                                ?>
                            </div>
                        </div>
                                                  
                        <!-- fax_from -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-fax"></i>
                                </div>
                                <?php
                          		$fax_from = array(
            		                'name'        => 'fax_from',
            		                'id'          => 'fax_from',
            		                'value'       => $this->form_validation->set_value('fax_from'),
            		                'style'       => '',
            		                'class'       => 'form-control',
            		                'placeholder' => 'Fax From...',
            		            );	
                                echo form_input($fax_from); 
                                ?>
                            </div>
                        </div>
                        
                        <!-- fax_media_url -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-folder-open"></i>
                                </div>
                                
                                <?php 
                                    $userFileData = array(
                        
                                    'id'        => 'userfile',
                                    'name'      => 'userfile',
                                    'class'     => 'btn btn-primary',
                        
                                ); ?>
                                <?php echo form_error('userfile'); ?>
                                <?php echo form_upload($userFileData); ?>
                            </div>
                        </div>
                        
                        
                       <?php 
                       // Add submit button and close form
                        echo form_submit('fax_send', 'Send', 'class="btn btn-default btn-lg"');
                        echo form_close();
                        ?>
                        
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->
