

<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Fax Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Fax Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->
                        
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Fax History
                        </div>
                        
                        <table id="table-mh-admin-blog" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				            <thead>
						        <tr>
								    <th>No. Pages</th>
								    <th>status</th>
								    <th>direction</th>
								    <th>from / to</th>
								    <th>date_updated</th>
								    <th>price / duration</th>
							    </tr>
		                    </thead>
						
						    <tbody>	
						        <?php
						        $i = 0;
                                foreach ($faxes as $fax) : 
                                ?>
                            
						        <tr>
						            <td>
                                        <?php
                                        echo $fax['num_pages'];
                                        ?>
                                    </td>
                                    <td>
                                       <?php
                                        echo $fax['status'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $fax['direction'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $fax['from'].'</br>'.$fax['to'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $fax['date_updated'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $fax['price_unit'].' ';
                                        echo $fax['price'].'</br>'.$fax['duration'].' seconds';
                                        ?>
                                    </td>
                        </tr>
                        
                        <?php 
                        $i = $i +1;
                        endforeach     
                        ?>
                        
                        </tbody>
					</table>
						
						
                        
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->
