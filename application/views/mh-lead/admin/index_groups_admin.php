
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Group Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Group Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Group Management</h3> 
	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
		  	
							<h1><?php // echo lang('index_heading');?></h1>
							<p><?php // echo lang('index_subheading');?></p>
							
							<div id="infoMessage"><?php echo $message;?></div>
							
							<table class="table table-condensed table-bordered table-striped table-hover" id="table-list-groups" width="100%">
								
							<?php // <table cellpadding=0 cellspacing=10> ?>
							<thead>
								<tr>
									<th>id</th>
									<th>Name</th>
									<th>Description</th>
									<th></th>
								</tr>
							</thead>
							
							 <tbody>
								<?php foreach ($groups as $group):?>
									<tr>
							            <td><?php echo anchor("MH_auth_admin/edit_group/".$group->id, htmlspecialchars($group->id,ENT_QUOTES,'UTF-8')) ;?></td>
							         
							            <td><?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8');
							            
							            
							            ?></td>
							         
							            <td><?php echo htmlspecialchars($group->description,ENT_QUOTES,'UTF-8');?></td>

										<td>
											<?php 
											echo anchor("MH_auth_admin/edit_group/". $group->id, ' ', ('class="fas fa-edit"'));
											echo anchor("MH_auth_admin/delete_group/". $group->id, ' ', ('class="fas fa-delete"'));
											?>
										</td>
									</tr>
								<?php endforeach;?>
							</table>
							

							
							<p><?php echo anchor('MH_auth_admin/create_group', lang('index_create_group_link'))?></p>
					<!-- ============================================================== -->
					<!-- Close ion-auth Content -->
					<!-- ============================================================== -->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
