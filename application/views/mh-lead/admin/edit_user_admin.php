
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">User Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">User Management</h3> 
	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
					
					
					
<h1><?php echo lang('edit_user_heading');?></h1>
<p><?php echo lang('edit_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open(uri_string());?>

<div class="row">
    <div class="col-sm-6 col-xs-6">
        
        <!-- first_name -->
        <div class="form-group">
            <label for="first_name">
                First Name
            </label>
            <div class="input-group">
            <?php
  		        $first_name = array(
		            'name'        => 'first_name',
		            'id'          => 'first_name',
		            'value'        => $user->first_name,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'First Name',
		        );	
                echo form_input($first_name); 
                ?>
    		    <div class="input-group-addon"><i class="ti-user"></i>
    		    </div>
            </div>
        </div>
        
        <!-- last_name -->
        <div class="form-group">
            <label for="last_name">
                Last Name
            </label>
            <div class="input-group">
            <?php
  		        $last_name = array(
		            'name'        => 'last_name',
		            'id'          => 'last_name',
		            'value'        => $user->last_name,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Last Name',
		        );	
                echo form_input($last_name); 
                ?>
    		    <div class="input-group-addon"><i class="ti-user"></i>
    		    </div>
            </div>
        </div>
        
        <!-- company -->
        <div class="form-group">
            <label for="company">
                Company Name
            </label>
            <div class="input-group">
            <?php
  		        $company = array(
		            'name'        => 'company',
		            'id'          => 'company',
		            'value'        => $user->company,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Company Name',
		        );	
                echo form_input($company); 
                ?>
    		    <div class="input-group-addon"><i class="ti-home"></i>
    		    </div>
            </div>
        </div>
        
        <!-- phone -->
        <div class="form-group">
            <label for="phone">
                Phone 
            </label>
            <div class="input-group">
            <?php
  		        $phone = array(
		            'name'        => 'phone',
		            'id'          => 'phone',
		            'value'        => $user->phone,
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Phone',
		        );	
                echo form_input($phone); 
                ?>
    		    <div class="input-group-addon"><i class="ti-mobile"></i>
    		    </div>
            </div>
        </div>
        
        <!-- password -->
        <div class="form-group">
            <label for="phone">
                Password 
            </label>
            <div class="input-group">
            <?php
  		        $password = array(
		            'name'        => 'password',
		            'id'          => 'password',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password',
		        );	
                echo form_input($password); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
        
        <!-- password_confirm-->
        <div class="form-group">
            <label for="phone">
                Confirm Password (if changing password)
            </label>
            <div class="input-group">
            <?php
  		        $password_confirm = array(
		            'name'        => 'password_confirm',
		            'id'          => 'password_confirm',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password (if changing password)',
		        );	
                echo form_input($password_confirm); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
        
        
        <!-- groups -->
        
 <?php if ($this->ion_auth->is_admin()): ?>
 
             <label for="phone">
                Select Group.  
            </label>
            
<?php foreach ($groups as $group):?>

<div class="form-group">
    
    <div class="checkbox checkbox-success">   
    
            
    
              
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              
              <label class="checkbox">
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
              
              
              
              
    </div>
</div> <!-- end of groups -->

<?php endforeach?>
<?php endif ?>
                                        
        
    </div>
</div>
                                    


      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <p><?php echo form_submit('submit', lang('edit_user_submit_btn'));?></p>

<?php echo form_close();?>
				
<!--------------------------->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
