
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
<br>
<br>      
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                	<h2>Lead Management</h2>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_auth_admin/create_user'); ?>"><i class="fa fa-plus"></i> New Employee</a>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_auth_admin/create_user'); ?>"><i class="fa fa-plus"></i> New Employee Group</a>
                	
                    
	<hr class="hr-panel-heading" />
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
		  	
							<div class="container text-center">
								<div class="row">
									<div class="col-sm-12">
										<div id="status">
											
										</div>
									</div>
								</div>
							</div>
							


							<?php if (!empty($message)) { ?>
							<?php echo $message;?>
							<?php } ?>
							
							<table class="table table-bordered display responsive nowrap table-striped  table-hover"  id="table-list-users" style="width:100%">
							<!-- table  table-bordered table-hover -->
							
							<?php // <table cellpadding=0 cellspacing=10> ?>
							<thead>
								<tr>
									<th></th>
									<th>ID</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Company</th>
									<th>Last Login</th>
									<th>Created</th>
									<th>Groups</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							
							 <tbody>
								<?php foreach ($users as $user):?>
									<tr>
										<td></td>
										<td id="id">
							            	<?php echo htmlspecialchars($user->id,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="first_name">
							            	<?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="last_name" >
							            	<?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?>
							            </td>
							            <td id="email">
							            	<?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?>
							            </td>
										<td class="phonenumber" id="phonenumber">
											<?php echo '<a href="ffff">'.htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8').'</a>';?>
								
										</td>
										<td id="company">
											<?php echo htmlspecialchars($user->company,ENT_QUOTES,'UTF-8');?>
										</td>
										
									
										<!-- last login $user->last_login -->
									<?php
							        /** lastlogin  **/
    							    if (isset($user->last_login)){
                                        
                                        // gives epoch time
        					            $mydate = $user->last_login;
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo '<td></td>';
                                    }
							    ?>
							 
									
										<!-- end tooltip last login-->
										
										
<!-- CREATED_ON - add tooltip showing how long ago created if not null -->

							<!-- last login $user->last_login -->
									<?php
							        /** created_on  **/
    							    if (isset($user->created_on)){
                                        
                                        // gives epoch time
        					            $mydate = $user->created_on;
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                          echo '<td data-sort="0">N/A</td>';
                                    }
							    ?>


<!-- end tooltip created_on-->
										
										<td>
											<?php foreach ($user->groups as $group):?>
									<span class="label label-success label-as-badge">	<?php echo anchor("MH_auth_admin/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?></span><br />
											
							                <?php endforeach?>
										</td>
										<td data-order="<?php echo $user->active; ?>">
							
									

										 <?php
										 // echo form_open("MH_auth_admin/create_user");
										 /*
										 echo $user->active;
				                            $active = array(
				                                'name'          => 'active',
				                                'id'            => 'active',
				                                'value'         => '1',
				                                'checked'       => $user->active,
				                                'style'         => '',
				                                'data-toggle'   => 'toggle',
				                                'data-on'       => 'Active',
				                                'data-off'      => 'Inactive',
				                                'data-onstyle'	=> 'success',
				                                'data-size'     => 'mini',
				                                'data-style'	=> 'ios',
				                            	'onclick'   	=> '<href="/home',
				                            	
				                            	
				                              
				                            );	
				                            */
				                        	?>
				                          
				                            <?php // echo form_checkbox($active); ?>
				                            
				                           	<?php // echo form_close();?>
											
											<?php 
										
											if ($user->active == 1) { 
											?> 
											<a href="/MH_auth_admin/toggle_activate/<?php echo $user->id.'/'.$user->active; ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Deactivate" onclick="return confirm('Are you sure?  This will only deactivate the account.')"><i class="far fa-smile"></i></a>
											
											<?php
											} else {
											?>
											<a href="/MH_auth_admin/toggle_activate/<?php echo $user->id.'/'.$user->active; ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Activate" onclick="return confirm('Are you sure?  This will activate the account.')"><i class="far fa-frown"></i></a>
											<?php
											}
											?>
											
											<?php
												//echo 
												//($user->active) 
												//? 
												//anchor("MH_auth_admin/deactivate/".$user->id, ' ', ('class="far fa-circle"')) 
												//: 
												//anchor("MH_auth_admin/activate/". $user->id, ' ', ('class="far fa-times-circle"'))
												//;
											?>
											
											
										</td>
										
										
										<td>
											
									<a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/MH_auth_admin/edit_user/<?php echo $user->id; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="/MH_auth_admin/view_user/<?php echo $user->id; ?>"><i class="fa fa-eye"></i></a>
                                    
                                    	
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
							</table>
							
							<p>
							<?php
							echo $this->pagination->create_links();
							?>	
							</p>
							
							
					<!-- ============================================================== -->
					<!-- Close ion-auth Content -->
					<!-- ============================================================== -->
<style>
.console-line {
  font-family: monospace;
  margin: 2px;
}
</style>

 



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
