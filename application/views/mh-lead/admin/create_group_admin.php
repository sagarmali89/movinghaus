<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Groups Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Create Group</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Create Group</h3> 
                  
                  	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->


<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("MH_auth_admin/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><?php echo form_submit('submit', lang('create_group_submit_btn'));?></p>

<?php echo form_close();?>

	                        <!-- ============================================================== -->
					<!-- Close ion-auth Content -->
					<!-- ============================================================== -->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
