
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">User Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">User Management</h3> 
	
					<!-- ============================================================== -->
					<!-- ion-auth Content -->
					<!-- ============================================================== -->
		  	
							<h1><?php // echo lang('index_heading');?></h1>
							<p><?php // echo lang('index_subheading');?></p>
							
							<div id="infoMessage"><?php echo $message;?></div>
							
							<table class="table table-condensed table-bordered table-striped table-hover" width="100%">
								
							<?php // <table cellpadding=0 cellspacing=10> ?>
								<tr>
									<th><?php echo lang('index_fname_th');?></th>
									<th><?php echo lang('index_lname_th');?></th>
									<th><?php echo lang('index_email_th');?></th>
									<th>Phone</th>
									<th>Company</th>
									<th>Last Login</th>
									<th>Created On</th>
									<th><?php echo lang('index_groups_th');?></th>
									<th><?php echo lang('index_status_th');?></th>
									<th><?php echo lang('index_action_th');?></th>
								</tr>
								<?php foreach ($users as $user):?>
									<tr>
							            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
							            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
							            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
										<td><?php echo htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8');?></td>
										<td><?php echo htmlspecialchars($user->company,ENT_QUOTES,'UTF-8');?></td>
										
										<td>
										<?php 
										if (!empty($user->last_login)){
											echo unix_to_human(htmlspecialchars($user->last_login,ENT_QUOTES,'UTF-8'));
										}
										?>
										</td>
										
										<td>
										<?php 
										if (!empty($user->created_on)){
											echo unix_to_human(htmlspecialchars($user->created_on,ENT_QUOTES,'UTF-8'));
										}
										?>
										
										</td>
										<td>
											<?php foreach ($user->groups as $group):?>
												<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
							                <?php endforeach?>
										</td>
										<td>
											<?php echo 
												($user->active) 
												? 
												anchor("auth/deactivate/".$user->id, ' ', ('class="far fa-circle"')) 
												: 
												anchor("auth/activate/". $user->id, ' ', ('class="far fa-times-circle"'))
												;
											?>
										</td>
										<td><?php 
										echo anchor("auth/edit_user/". $user->id, ' ', ('class="fas fa-edit"'));
										// echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
									</tr>
								<?php endforeach;?>
							</table>
							
							<p>
							<?php
							echo $this->pagination->create_links();
							?>	
							</p>
							
							<p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
					<!-- ============================================================== -->
					<!-- Close ion-auth Content -->
					<!-- ============================================================== -->



                </div> <!-- close white box -->
            </div> <!-- close 12 column width -->
        </div> <!-- close row -->
  
<!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
