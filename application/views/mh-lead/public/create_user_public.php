<!-- title -->
<div class="row">
    <div class="col-md-12">
            <h3>Registration</h3> 
    </div>
</div>
                    
<!-- show error messages here -->
<?php if($message): ?>
<div class="alert alert-warning">
<?php 
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $message; 
?>
</div>
<?php endif; ?>
                            
<!-- registration form here -->                           
<?php echo form_open("MH_auth_public/register_user");?>
<div class="row">
    <div class="col-sm-6 col-xs-6">   
    
        <!-- first_name -->
        <div class="form-group">
            
            <label for="first_name">
                First Name
            </label>
            
            <div class="input-group">
            <?php
  		        $first_name = array(
		            'name'        => 'first_name',
		            'id'          => 'first_name',
		            'value'        => $this->form_validation->set_value('first_name'),
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'First Name',
		        );	
                echo form_input($first_name); 
                ?>
    		    <div class="input-group-addon"><i class="ti-user"></i>
    		    </div>
            </div>
            
        </div>
                    
         <!-- last_name -->
        <div class="form-group">
            
            <label for="last_name">
                Last Name
            </label>
            
            <div class="input-group">
                <?php
      		    $last_name = array(
                'name'        => 'last_name',
                'id'          => 'last_name',
                'value'        => $this->form_validation->set_value('last_name'),
                'style'       => '',
                'class'       => 'form-control',
                'placeholder' => 'Last Name',
                );	
                echo form_input($last_name); 
                ?>
        		<div class="input-group-addon">
        		    <i class="ti-user">
        		    </i>
                </div>
            </div>
        </div>
                          
        <?php
        if($identity_column!=='email') {
            echo '<p>';
            echo lang('create_user_identity_label', 'identity');
            echo '<br />';
            echo form_error('identity');
            echo form_input($identity);
            echo '</p>';
        }
        ?>
                    
        <!-- company -->
        <div class="form-group">
            <label for="company">
                Company Name
            </label>
            <div class="input-group">
            <?php
  		        $company = array(
		            'name'        => 'company',
		            'id'          => 'company',
		            'value'        => $this->form_validation->set_value('company'),
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Company Name',
		        );	
                echo form_input($company); 
                ?>
    		    <div class="input-group-addon"><i class="ti-home"></i>
    		    </div>
            </div>
        </div>
                    
        <!-- email -->
        <div class="form-group">
            <label for="email">
                Email
            </label>
            <div class="input-group">
            <?php
  		        $email = array(
		            'name'        => 'email',
		            'id'          => 'email',
		            'value'        => $this->form_validation->set_value('email'),
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Email',
		        );	
                echo form_input($email); 
                ?>
    		    <div class="input-group-addon"><i class="ti-email"></i>
    		    </div>
            </div>
        </div>
                    
        <!-- phone -->
        <div class="form-group">
            <label for="phone">
                Phone 
            </label>
            <div class="input-group">
            <?php
  		        $phone = array(
		            'name'        => 'phone',
		            'id'          => 'phone',
		            'value'       => $this->form_validation->set_value('phone'),
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Phone',
		        );	
                echo form_input($phone); 
                ?>
    		    <div class="input-group-addon"><i class="ti-mobile"></i>
    		    </div>
            </div>
        </div>
                    
        <!-- password -->
        <div class="form-group">
            <label for="phone">
                Password 
            </label>
            <div class="input-group">
            <?php
  		        $password = array(
		            'name'        => 'password',
		            'id'          => 'password',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password',
		        );	
                echo form_input($password); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
                                        
        <!-- password_confirm-->
        <div class="form-group">
            <label for="phone">
                Confirm Password (if changing password)
            </label>
            <div class="input-group">
            <?php
  		        $password_confirm = array(
		            'name'        => 'password_confirm',
		            'id'          => 'password_confirm',
		            'type'        => 'password',
		            'value'       => '',
		            'style'       => '',
		            'class'       => 'form-control',
		            'placeholder' => 'Password (if changing password)',
		        );	
                echo form_input($password_confirm); 
                ?>
    		    <div class="input-group-addon"><i class="ti-lock"></i>
    		    </div>
            </div>
        </div>
                    
                    
        <p>
            <?php 

                echo form_submit('submit', lang('create_user_submit_btn'), "class='btn btn-primary'" );?>
        </p>
 
 
    </div>
</div>                   
<?php echo form_close();?>

