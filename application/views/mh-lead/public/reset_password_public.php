<!-- title -->
<div class="row">
    <div class="col-md-12">
            <h3>Reset Password</h3> 
    </div>
</div>

<!-- show error messages here -->
<?php if($message): ?>
<div class="alert alert-warning">
<?php 
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $message; 
?>
</div>
<?php endif; ?>

<?php echo form_open('MH_auth_public/reset_password/' . $code);?>

	<p>
		<label for="new_password">
			<?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?>
		</label> <br />

		<?php echo form_input($new_password);?>
	</p>

	<p>
		<?php 
		echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');
		?> 
		<br />
		<?php 

		            
		echo form_input($new_password_confirm);
		?>
	</p>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	<p><?php echo form_submit('submit', lang('reset_password_submit_btn'), "class='btn btn-primary'");?></p>

<?php echo form_close();?>