<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Product Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Product Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->

                        <h2>Create Product</h2>        
                    
                        <?php  // display any alert messag
                    	if (isset($this->data['alert'])){
                    		echo $this->data['alert'];
                    	}
                        ?>

                        <?php
                    	$do_upload = array(
                            'name'        => 'do_upload',
                            'id'          => 'do_upload',
                            'style'       => '',
                    	    'class'       => 'form',
                    	);
                        echo form_open_multipart('MH_product_admin/product_create', $do_upload); 
                        ?> 
    
                        <!-- product_name -->
                        <div class="form-group">
                        <?php
                        $product_name = array(
                            'name'        => 'product_name',
                            'id'          => 'product_name',
                            'value'       => set_value('product_name'),
                            'style'       => '',
                            'class'       => 'form-control input-md',
                            'placeholder' => 'Add Product Name'
                        );	
                    	?>
                        <?php echo form_error('product_name'); ?>
                        <?php echo form_input($product_name); ?>
                        </div>
                  
                        <!-- product_description -->
                        <div class="form-group">
                        <?php
                        $product_description = array(
                                'name'        => 'product_description',
                                'id'          => 'editor1',
                                'value'       =>  html_entity_decode(set_value('product_description')),
                                'rows'        => '3',
                                'cols'        => '',
                                'style'       => '',
                                'class'       => 'form-control input-md',
                                'placeholder' => 'Add Product Description'
                            );	
                            echo form_error('product_description'); 
                    		echo form_textarea($product_description); 
                    		?>
                        </div>
  
                        <!-- product_price -->
                        <div class="form-group">
                        <?php
                        $product_price = array(
                            'name'          => 'product_price',
                            'id'            => 'product_price',
                            'value'         => set_value('product_price'),
                            'style'         => '',
                            'class'         => 'form-control input-md',
                            'placeholder'   => 'Add Product Price in '.$this->config->item('mh_default_code_alpha'),
                        );	
                    	?>
                        <?php echo form_error('product_price'); ?>
                        <?php echo form_input($product_price); ?>
                        </div>
    
                        <!-- product_category -->
                        <div class="form-group">
                            <?php
                            $options = array();
                            
                            foreach ($product_categories as $product_category) {
                                $options[$product_category['id']] =  $product_category['name'];
                            }
                            ?>
                            <?php echo form_dropdown('product_category_id', $options, set_value('product_category_id'), 'class="form-control input-md"' );  ?> 
                        </div>
    
                        <!-- product_image -->
                        <div class="form-group">
                            <label>Set Product Image</label>
                            <?php 
                             $uderFileData = array(
                                            
                                            'id'        => 'userfile',
                                            'name'      => 'userfile',
                                            'class'     => 'btn btn-primary',
                                            
                                        ); ?>
                            <?php echo form_error('userfile'); ?>
                            <?php echo form_upload($uderFileData); ?>
                        </div>
    
                        <!-- product_enable -->
                        <div class="form-group">  
                            <?php
                            $product_enable = array(
                                'name'          => 'product_enable',
                                'id'            => 'product_enable',
                                'value'         => '1',
                                'checked'       => (bool) set_value('product_enable'),
                                'style'         => '',
                                'data-toggle'   => 'toggle',
                                'data-on'       => 'Enabled',
                                'data-off'      => 'Disabled',
                                'data-size'     => 'large',
                                'class'         => 'form-control input-md',
                            );	
                        	?>
                            <?php echo form_error('product_enable'); ?>
                            <?php echo form_checkbox($product_enable); ?>
                        </div>

                        <a class="btn btn-default btn-lg" href="<?php echo site_url('MH_product_admin'); ?>">List Products </a>
    
                        <?php 
                        echo form_submit('product_create', 'Submit', 'class="btn btn-default btn-lg "');
                        echo form_close();
                        ?>

                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->




