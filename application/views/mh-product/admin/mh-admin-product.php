
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
    
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Product Management</h4> 
            </div>
           
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Product Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All Products <?php if (!empty($category_name)) { echo 'from "'.$category_name.'"'; } ?></h1>
                    <?php
                    
                	$message = $this->session->flashdata('message');
                	if (isset($message['message'])) {
                			echo '<br>';
                			echo '<div class="alert alert-'.$message['class'].'">';
                			echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  			echo $message['message'];
                			echo '</div>';
                	}?>
                	
                	<p>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('MH_product_admin/product_create/'); ?>"><i class="fa fa-plus"></i> New Product</a>
                	</p>
                	
                	
                	<div>
                	
                        <table id="table-mh-admin-product" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				            <thead>
						        <tr>
								    <th>Category</th>
								    <th>Author</th>
								    <th>Product Name / Description</th>
								    <th>Price</th>
								    <th>Enabled?</th>
								    <th>Created</th>
								    <th>Updated</th>
								    <th></th>
							    </tr>
		                    </thead>
						
						    <tbody>	
							
							<?php foreach ($products as $product) : ?>
							
						                    
                        	<?php  
                        	// check to see if there is an image and if no assign noimage.png as the image
                        	if ( (empty($product['product_image'])) || ($product['product_image'] == NULL) ){
                                $product['product_image'] = 'noimage.png';
                        	}
                            ?>
                            
                            
							<tr>
							    <?php 
					                // look up user by id from the posts table
							        $user = $this->ion_auth->user($product['user_id'])->row_array(); 
							    ?>
							    
							    <!-- category_name-->
							    <td>
							        <?php if (!empty($product['name'])) { echo $product['name']; } ?>
					            </td>
					            
					            <!--username-->
							    <td>
							        <?php if (!empty($user['username'])) { echo $user['username']; } ?>
					            </td>
					            
					            <!--
					            product_image
					            product_name
					            product_description
					            -->
							    <td>
							        <div class="row">
                                    <div class="col-md-3">
                                    	<?php echo 'image '.$product['product_image'];   ?>
                                        <img  class="img-thumbnail" src="<?php if ( (!empty($product['product_image'])) || $product['product_image'] === NULL ) { echo site_url(); echo  $this->config->item('product_image_path'); echo $product['product_image']; } ?>"> 
                                    </div>
                                    <div class="col-md-9">
							        
    				                    <p> 
    				                        <b> 
    				                            <?php if (!empty($product['product_name'])) { echo $product['product_name']; }?> 
    				                        </b> 
    				                    </p>
							        
							            <p>
					                    <?php echo word_limiter(strip_tags($product['product_description']), $this->config->item('product_number_of_words_to_display')); ?> 
					                    
					                    <span class="label label-default label-rouded">
					                       <a href="<?php echo site_url('MH_product_admin/product_view_single/'.$product['product_slug']); ?>">Read More ...</a>  
							             </span>    
							             </p>
							         </div>
							         </div>
							    </td>
							    
							    <td>
							        <?php 
							        echo $this->config->item('mh_default_code_alpha'). ' '.$product['product_price'];
							        ?>
							    </td>
							    
							    <td>Enabled</td>
							    
					           <?php
							        /** created time **/
    							    if (isset($product['created_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($product['created_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo '<td data-sort="0">N/A</td>';
                                    }
							    ?>

	    							<!-- updated -->
						            <?php
							        /** updated time **/
    							    if (isset($product['updated_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($product['updated_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo '<td data-sort="0">N/A</td>';
                                    }
							    ?>
							    
							    <!-- menu choices to right -->
							    <td  style="white-space:nowrap;">
							        <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/MH_product_admin/product_update/produc_slug/<?php echo $product['product_slug']; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="<?php echo site_url('MH_product_admin/product_view_single/'.$product['product_slug']); ?>"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('MH_product_admin/product_delete/product_id/'.$product['product_id'])?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>
						
						
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                    </div> <!-- close white box -->
                
                </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
        <!-- close up page content -->
	    </div> <!-- close fluid container -->
    </div> <!-- close page wrapper -->
