        <?php
		print_r($mh_template);
		echo '<pre>';
		var_dump($this->data);
		echo '</pre>';
    	die();        
    	?>
    	<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
    
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Ports Management</h4> 
            </div>
           
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Ports Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All Ports</h1>
                    <?php
                    
                	$message = $this->session->flashdata('message');
                	if (isset($message['message'])) {
                			echo '<br>';
                			echo '<div class="alert alert-'.$message['class'].'">';
                			echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  			echo $message['message'];
                			echo '</div>';
                	}?>
                	
                	<p>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('Ports_admin/ports_create/'); ?>"><i class="fa fa-plus"></i> New Port</a>
                	</p>
                	
                	
                	<div>
                	
                        <table id="table-mh-admin-ports-index" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				            <thead>
						        <tr>
								    <th>Port Id</th>
								    <th>City Name</th>
								    <th>Sub Region</th>
								    <th>Latitude</th>
								    <th>Longitude</th>
								    <th>Country</th>
								    <th></th>
							    </tr>
		                    </thead>
						
						    <tbody>	
							<?php foreach ($ports as $port) : ?>
							
							                    
                  
							<tr>
							    
							    <td>
							        <?php if (!empty($port['port_id'])) { echo $port['port_id']; } ?>
					            </td>
					            
							    <td>
							        <?php if (!empty($port['city_name'])) { echo $port['city_name']; } ?>
					            </td>
					            
							    <td>
							      <?php if (!empty($port['sub_region'])) { echo $port['sub_region']; }?> 
							    </td>
							    
							     <td>
							      <?php if (!empty($port['country'])) { echo $port['country']; }?> 
							    </td>
							    
							    
					             <td>
							      <?php if (!empty($port['latitude'])) { echo $port['latitude']; }?> 
							    </td>
							    
							    <td>
							      <?php if (!empty($port['longitude'])) { echo $port['longitude']; }?> 
							    </td>
							    
							    
							    <td  style="white-space:nowrap;">
							        <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/Faq_admin/faq_update/slug/<?php echo $faq['slug']; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="<?php echo site_url('Faq_admin/faq_view_single/'.$faq['slug']); ?>"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('Faq_admin/faq_delete/id/'.$faq['id'])?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>
						
						
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                    </div> <!-- close white box -->
                
                </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
        <!-- close up page content -->
	    </div> <!-- close fluid container -->
	    
    </div> <!-- close page wrapper -->
