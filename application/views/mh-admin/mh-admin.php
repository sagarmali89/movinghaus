<?php
    // **********  TOTAL USAGE
    // get the FROM billing date
    $from   = strtotime($this->data['usage_records']->usage_records[0]->start_date);
    
    // get the TO billing date
    $to     = strtotime($this->data['usage_records']->usage_records[0]->end_date);
    
    // Calculate the number of days between FROM and TO
    $date_diff = $to - $from;
    $num_days  = round($date_diff / (60 * 60 * 24));
    
    // The the amount of money spent
    $price      =  round($this->data['usage_records']->usage_records[0]->price);
    $price_unit =  $this->data['usage_records']->usage_records[0]->price_unit;
    
    // Write our the verbiage  
    $price_verbiage =  'Total spent on all Twilio services '.$this->data['usage_records']->usage_records[0]->start_date.' to '.$this->data['usage_records']->usage_records[0]->end_date.' </br>('.$num_days.' days)';
   
   
   
    // **********  CALLS USAGE
    $calls_from   = strtotime($this->data['call_records']->usage_records[0]->start_date);
    
    // get the TO billing date
    $calls_to     = strtotime($this->data['call_records']->usage_records[0]->end_date);
    
    // Calculate the number of days between FROM and TO
    $calls_date_diff = $calls_to - $calls_from;
    $calls_num_days  = round($calls_date_diff / (60 * 60 * 24));
    
    // The the amount of money spent
    $calls_price        =  round($this->data['call_records']->usage_records[0]->price);
    $calls_unit         =  $this->data['call_records']->usage_records[0]->price_unit;
    
    // The number of calls
    $calls_count = $this->data['call_records']->usage_records[0]->count;
    
    // The number of minutes
    $calls_usage        = $this->data['call_records']->usage_records[0]->usage;
    $calls_usage_units  = $this->data['call_records']->usage_records[0]->usage_unit;
    
    // Write our the verbiage  
    $calls_verbiage =  'No. of calls made and received '.$this->data['call_records']->usage_records[0]->start_date.' to '.$this->data['call_records']->usage_records[0]->end_date.' </br>('.$num_days.' days)';
  
    $calls_price_verbiage = 'Total spent on voice '.$this->data['call_records']->usage_records[0]->start_date.' to '.$this->data['call_records']->usage_records[0]->end_date.' </br>('.$num_days.' days)';
   
   
    ?>
        
        
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4> 
            </div>
            
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Main KPIs</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->
                        
<div class="container">
    <div class="row">
        
        <br/>
        
        <div class="col text-center">
		    <h2>Dashboard</h2>
		</div>
		
    </div>
    	
    <div class="row"> <!-- first row KPI -->
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-users fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo $num_customers; ?>" data-speed="1500"></h2>
                <p class="count-text ">Customers</p>
            </div>
        </div>
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-phone fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>
                <p class="count-text "># Phone Calls Made and Received</p>
            </div>
        </div>
      
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-fax fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="11900" data-speed="1500"></h2>
                <p class="count-text "># Faxes Made and Received</p>
            </div>
        </div>
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-newspaper fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo $num_articles; ?>" data-speed="1500"></h2>
                <p class="count-text ">Articles Written</p>
            </div>
        </div>
      
    </div> <!-- end first row KPI -->
    
    </br>
    
    <div class="col text-center">
		    <h2>Communications</h2>
	</div>
		
    <div class="row"> <!-- Second row KPI -->
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-yen-sign fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo $price; ?>" data-speed="1500"></h2>
                <p class="count-text "><?php echo $price_verbiage; ?></p>
            </div>
        </div>
        
        

        
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-phone fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo $calls_count; ?>" data-speed="1500"></h2>
                <p class="count-text "><?php echo $calls_verbiage; ?> </p>
            </div>
        </div>
      
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-yen-sign fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo  $calls_usage; ?>" data-speed="1500"></h2>
                <p class="count-text "><?php echo $calls_price_verbiage; ?></p>
            </div>
        </div>
        
        <div class="col-sm-3">
            <div class="counter">
                <i class="fa fa-newspaper fa-2x"></i>
                <h2 class="timer count-title count-number" data-to="<?php echo $num_articles; ?>" data-speed="1500"></h2>
                <p class="count-text ">Articles Written</p>
            </div>
        </div>
      
    </div> <!-- end Second row KPI -->
    
    
</div>


        <?php 
        //echo '<pre>';
        //print_r($this->data['usage_records']); 
        // echo '</pre>';
        
        //echo '<pre>';
        //print_r($this->data['call_records']); 
        //echo '</pre>';
        
        ?>
        
        
<?php //print_r ($this->data['usage_records']); ?>
                        
                        
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->