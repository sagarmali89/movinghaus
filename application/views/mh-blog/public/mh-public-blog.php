<?php
// echo the pagination links
//echo $links;
?>
<!-- ============================================================== -->
<!-- Blog Content -->
<!-- ============================================================== -->


<h1>
    Latest Posts <?php if (!empty($category_name)) { echo 'from "'.$category_name.'"'; } ?>
</h1>
    
<div class="row">
    

    <div class="col-9">
        <?php 
        foreach ($blogs as $blog) : 
            if (empty($blog['post_image'])) {
                $blog['post_image'] = 'noimage.png';
        }
        ?>
        <div class="row">
            <div class="col-12"><h3><?php echo $blog['title']; ?></h3></div>
            <div class="col-3"><img  class="img-thumbnail" src="<?php if (!empty($blog['post_image'])) { echo site_url(); echo  $this->config->item('image_path'); echo $blog['post_image']; } ?>"> </div>
            <div class="col-9">
                 <small class="created_timestamp">Created on: 
                    <?php 
                    echo $blog['created_timestamp']; 
                    if (isset($blog['updated_timestamp'])){
                        echo " and Updated on: ";
                        echo $blog['updated_timestamp'];
                    }
                    ?>
                    in <strong><?php echo $blog['name']; ?></strong>
                </small>
        
                </br>
                    <?php echo word_limiter(strip_tags($blog['body']), $this->config->item('number_of_words_to_display')); ?>
                </br>

                </br>
                
                <p>
                    <a class="btn btn-default" href="<?php echo site_url('Blog_public/blog_view_single/'.$blog['slug']); ?>">Read More </a>
                </p>
                
            </div>
            
        </div>
        </br></br>
        <?php endforeach ?>
    </div>
    
    
    
    <div class="col-3">
        <div class="row">
            <div class="col-12">
            
            <p>
            <?php
            if (!empty($this->session->userdata('year'))){
                
                
                echo '<h3>Filter Applied: '.$this->session->userdata('year').'-'.$this->session->userdata('month').'</h3>'; 
                ?>
                <button type="button"  onclick="window.location.href='/Blog_public/clear_year_month'" class="btn btn-outline-primary">Remove Filter</button>
                <?php
            }
            ?>
            </p>    
            
            <h3>Blog Archive</h3>
            
                <?php 
                $i = 0;
                foreach ($link_data as $row) {
                     echo '<span class="badge badge-secondary">'.$link_data[$i]['post_count'].'</span>';
                        ?>
                        <a href="<?php echo site_url('Blog_public/list_blog_month_year/'.$link_data[$i]['year'].'/'.$link_data[$i]['month'].'/'.$link_data[$i]['post_count']) ?>">
                          <?php
                            echo $link_data[$i]['year'].' - '.$link_data[$i]['monthname'];  
                            echo '  </a>';
                           
                            echo '</br>';
                            $i++;
                }
                ?>
                
            </div>
            <div class="col-12"></br></br></br>
            </div>
            <div class="col-12"><h3></h3>
            </div>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- Blog Content -->
<!-- ============================================================== -->
<?php
// echo the pagination links
echo $links;
?>