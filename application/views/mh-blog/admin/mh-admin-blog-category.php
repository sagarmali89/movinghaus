
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Category Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Category Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    	
                    <h1>All Categories</h1>
                    
    <?php
        if ($this->session->flashdata('msg_noti') != '') {
            echo 
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>' . $this->session->flashdata('msg_noti') . '</p>
                </div>';
        } 
        if ($this->session->flashdata('msg_error') != '') {
            echo 
                '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>'. $this->session->flashdata('msg_error') . '</p>   
                </div>';
        }
        
    ?>
                	<div>
             
                	</div>
                        <table class="table table-condensed table-bordered table-striped table-hover" id="table-mh-admin-blog-category" width="100%">
						   	<thead>
						    <tr>
								<th>Category Name</th>
								<th>Created</th>
								<th>Created by</th>
								<th>Updated</th>
								<th>Action</th>
							</tr>
							</thead>
							
							<tbody>
							<?php foreach ($categories as $category) : ?>
							<tr>
							    <?php 
					                // look up user by id from the posts table
							        $user = $this->ion_auth->user($category['user_id'])->row_array(); 
							    ?>
							    <!-- name -->
							    <td>
							        <?php if (!empty($category['name'])) { echo $category['name']; } ?>
							        
							       <span class="badge badge-secondary"> 
							    			<?php	
							    			if (!empty($category['number_of_posts'])) { 
							       				echo $category['number_of_posts']; 
							       				
							       			} elseif (($category['number_of_posts'])==0) {
							       				echo '0';
							       			}
							       			?> 
							       			Posts
							       	</span>
							        
							        
							    </td>
							    <!-- created -->
							    
							        
				                <?php
							        /** created time **/
    							    if (isset($category['created_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($category['created_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>

				
					            <!-- created by -->
							   	<td>
							        <?php if (!empty($user['username'])) { echo $user['username']; } ?>
					            </td>
							  
							   
							    <!-- updated -->
                                    <?php
							        /** updated time **/
    							    if (isset($category['updated_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($category['updated_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>



					            
					            <!-- Action -->

							    <td  style="white-space:nowrap;">
                                        <a data-toggle="tooltip" data-placement="top" title="Delete" href="<?php echo site_url('Blog_admin/blog_category_delete/id/'.$category['id'])?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    		<a data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-warning btn-sm" href="/Blog_admin/blog_category_update/<?php echo $category['id']; ?>"><i class="fa fa-edit"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>	
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
        </div> <!-- close row -->
    <!-- close up page content -->
	</div> <!-- close fluid container -->
</div> <!-- close page wrapper -->
