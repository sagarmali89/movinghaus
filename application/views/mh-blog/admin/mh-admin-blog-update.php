
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Blog Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Blog Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->

                        <h2>Update Blog Entry</h2>        
                    
                        <?php  
                            if (isset($this->data['alert'])) {
                                echo $this->data['alert'];
                            }
                        ?>
                    
                    
                        <?php echo form_open_multipart('Blog_admin/blog_update/slug/'.$this->data['blog']['slug']) ?> 
                        <input type="hidden" name="blog_id" value="<?php echo $this->data['blog']['id']; ?>">
                        <input type="hidden" name="slug" value="<?php echo $this->data['blog']['slug']; ?>">
                        <div class="form-group">
                        <?php
                        $title = array(
                            'name'        => 'title',
                            'id'          => 'title',
                            'value'       => set_value('title', $this->data['blog']['title']),
                            'style'       => '',
                            'class'       => 'form-control input-md',
                            'placeholder' => 'Add Title'
                        );	
                    	?>
                        <?php echo form_error('title'); ?>
                        <?php echo form_input($title); ?>
                        </div>
                      
                        <div class="form-group">
                        <?php
                        $body = array(
                                'name'        => 'body',
                                'id'          => 'editor1',
                                'value'       => html_entity_decode(set_value('body',  $this->data['blog']['body'])), // html_entity_decode renders html in html rather than showing the tags.
                                                                                                                      // this is used because of the rendering of CKEDITOR
                                'rows'        => '3',
                                'cols'        => '',
                                'style'       => '',
                                'class'       => 'form-control input-md',
                                'placeholder' => 'Add Body'
                            );	
                    	        ?>
                     		 	<?php echo form_error('body'); ?>
                    			<?php echo form_textarea($body); ?>
                        </div>
                      
                        <div class="form-group">
                            <?php
                            $options = array();
                            
                            foreach ($categories as $category) {
                                $options[$category['id']] =  $category['name'];
                            }
                            ?>
                            <?php echo form_dropdown('category_id', $options, $this->data['blog']['category_id'], 'class="form-control input-md"' );  ?> 
                        </div>
                        
                        <div class="form-group">
                            <label>Set New Blog Image</label>
                            <?php 
                             $userFileData = array(
                                            
                                            'id'        => 'userfile',
                                            'name'      => 'userfile',
                                            'class'     => 'btn btn-primary',
                                            
                                        ); ?>
                            <?php echo form_error('userfile'); ?>
                            <?php echo form_upload($userFileData); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Current Blog Image is set to:</label>
                                <br>
                                <?php
                                if (empty($blog['post_image'])) {
                                    $blog['post_image'] = 'noimage.png';
                                }
                                
                                // $this->config->item('upload_path')
                                ?>
                                



                                <img class="<?php if (empty($blog['post_image'])) { echo 'img-thumbnail img-fluid'; }?>" src="<?php echo site_url(); echo  $this->config->item('image_path'); echo $blog['post_image']; ?>" > 
                        </div>
                        
                        <?php if  ($blog['post_image'] !== 'noimage.png' ) { ?>
                        
                        <a href="/Blog_admin/blog_image_delete/id/<?php echo $this->data['blog']['id']; ?>" class="btn btn-danger btn-xs">
                            Delete Current Blog Image
                        </a>
                        
                        <?php } ?>
                        
                         <br><br>
                        
                        <div class="form-group">
                        <?php 
                        echo form_submit('Blog_admin/blog_update', 'Submit', 'class="btn btn-default btn-lg "');
                        echo form_close();
                        ?>
                        </div>

                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->




