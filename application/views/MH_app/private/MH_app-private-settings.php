<div class="col-sm-9">
  
  <div class="row">
    <div class="col-sm-12">
      <h1>App Settings</h1>
      <hr>
    </div>
  </div> <!-- end of row -->
  
  <div class="row">
    
    <div class="col-sm-8">
    
      <div>
        <?php 
        //date_default_timezone_set('UM9');
        //echo timezones('UM9');
        //echo $date=gmdate("F j, Y H:i:s").'<br>';
        
        // users time zone
        //  echo date('Y-m-d H:i:s');
        // echo bootstrap formatted error
        echo validation_errors(
          '<div class="alert alert-warning alert-dismissible fade show" role="alert">', 
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>'); 
          
        // display any success message
        echo $this->session->flashdata('message');
        ?>
      </div>
      
      <!-- open the form -->
      <?php echo form_open('MH_app_private/mh_app_settings');?>
  
      <!-- dropdown input-->
      <div class="form-group">
        
        <label for="newcategory">
  	  	  Select Your Timezone:
        </label>  
      
        <div class="input-group">
          <?php 
            echo timezone_menu($app_settings[0]['mh_us_value'], 'form-control');
          ?>
        </div>
      
      </div>  
        
      <!-- Submit Button-->
      <div class="form-group">
        <label for="newcategory">
        </label>  
        
  		  <?php
        echo form_submit('mh_app_settings', 'Save', 'class="btn btn-default btn-lg "');
        echo form_close();
        ?>
      </div> <!-- end form group -->

    </div>

    <div class="col-sm-4">
     There are many variations of passages of Lorem Ipsum available, but the 
     majority have suffered alteration in some form, by injected humour, or 
     randomised words which don't look even slightly believable. If you are going 
     to use a passage of Lorem Ipsum, you need to be sure there isn't anything 
     embarrassing hidden in the middle of text. All the Lorem Ipsum generators on 
     the Internet tend to repeat predefined chunks as necessary, making this the 
     first true generator on the Internet. It uses a dictionary of over 200 Latin 
     words, combined with a handful of model sentence structures, to generate 
     Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore 
     always free from repetition, injected humour, or non-characteristic words 
     etc.
    </div>

  </div> <!-- end of row -->

</div>


  
