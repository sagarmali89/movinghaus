

<div class="col-3 float-left pr-4">
  <nav class="menu" aria-label="Personal settings" data-pjax>
    <h3 class="menu-heading">
      Personal settings
    </h3>

    <a class="js-selected-navigation-item selected menu-item" aria-current="page" data-selected-links="avatar_settings /settings/profile" href="/settings/profile">Profile</a>
    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/admin" href="/settings/admin">Account</a>

    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/emails" href="/settings/emails">
      Emails
</a>
    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/notifications" href="/settings/notifications">Notifications</a>

      <a class="js-selected-navigation-item menu-item" data-selected-links="user_billing_settings /settings/billing" href="/settings/billing">Billing</a>

    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/keys" href="/settings/keys">SSH and GPG keys</a>

    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/security" href="/settings/security">Security</a>

    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/sessions" href="/settings/sessions">Sessions</a>

      <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/blocked_users" href="/settings/blocked_users">Blocked users</a>

    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/repositories" href="/settings/repositories">Repositories</a>
    <a class="js-selected-navigation-item menu-item" data-selected-links=" /settings/organizations" href="/settings/organizations">Organizations</a>
    <a class="js-selected-navigation-item menu-item" data-selected-links="edit_saved_reply /settings/replies" href="/settings/replies">Saved replies</a>

      <a class="js-selected-navigation-item menu-item" data-selected-links="applications_settings /settings/installations" href="/settings/installations">Applications</a>

  </nav>

  <nav class="menu" aria-label="Developer settings">
    <a class="menu-item" href="/settings/developers">
      Developer settings
</a>  </nav>

</div>


  <div class="col-9 float-left">
    
  <!-- Public Profile -->
  <div class="Subhead mt-0 mb-0">
    <h2 class="Subhead-heading">Public profile</h2>
  </div>
  <form class="columns js-uploadable-container js-upload-avatar-image is-default" id="profile_3161379" data-upload-policy-url="/upload/policies/avatars" data-upload-policy-authenticity-token="lt4xzBmTrx2Qw8xZILUuNt8lXJHLDVHPxJjNePIYogOB0SUe7PXnkIYCznFDXGVnxjys6RF+5lEoK4lsxYaq5Q==" novalidate="novalidate" data-alambic-owner-id="3161379" data-alambic-owner-type="User" action="/users/spreaderman" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="put" /><input type="hidden" name="authenticity_token" value="+UXHyZI8hXHXISSc96VlXTE8y1vsXHNvlzzJd2+ORSxrCgWgzsv5UgNWGUKjY3RlUTPooGqVeXZGD4rzKIT1Hg==" />
    <dl class="form-group edit-profile-avatar mr-4 float-right">
  <dt><label>Profile picture</label></dt>
  <dd class="avatar-upload-container clearfix">
    <img class="avatar rounded-2" src="https://avatars1.githubusercontent.com/u/3161379?s=400&amp;u=ee5eddc407f3609f2b8bad31e9ad236533ed2fd4&amp;v=4" width="200" height="200" alt="@spreaderman" />
    <div class="avatar-upload">
      <label class="position-relative btn button-change-avatar mt-3 width-full text-center">
        Upload new picture
        <input type="file" class="manual-file-chooser width-full height-full ml-0 js-manual-file-chooser">
      </label>

      <div class="upload-state loading">
        <button type="button" class="btn mt-3 width-full text-center" disabled>
          <img width="16" height="16" alt="" class="v-align-text-bottom" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" /> Uploading...
        </button>
      </div>

      <div class="upload-state text-red file-empty">
        This file is empty.
      </div>

      <div class="upload-state text-red too-big">
        Please upload a picture smaller than 1 MB.
      </div>

      <div class="upload-state text-red bad-dimensions">
        Please upload a picture smaller than 10,000x10,000.
      </div>

      <div class="upload-state text-red bad-file">
        We only support PNG, GIF, or JPG pictures.
      </div>

      <div class="upload-state text-red failed-request">
        Something went really wrong and we can’t process that picture.
      </div>

      <div class="upload-state text-red bad-format">
        File contents don’t match the file extension.
      </div>
    </div> <!-- /.avatar-upload -->
  </dd>
</dl>

    <div class="column two-thirds">
        <dl class="form-group">
          <dt><label for="user_profile_name">Name</label></dt>
          <dd><input class="form-control" type="text" value="Spreader" name="user[profile_name]" id="user_profile_name" /></dd>
        </dl>
      <dl class="form-group">
        <dt><label for="user_profile_email">Public email</label></dt>
        <dd class="d-inline-block">
            <select class="form-select select" name="user[profile_email]" id="user_profile_email"><option value="">Select a verified email to display</option>
<option value="stinchcomber@yahoo.com">stinchcomber@yahoo.com</option></select>
              <!-- '"` --><!-- </textarea></xmp> --></option></form><form class="d-inline-block ml-2" id="unset_profile_email" data-autosubmit="true" action="/settings/unset_profile_email.spreaderman" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="put" /><input type="hidden" name="authenticity_token" value="PiOfkWEaOHtQFqR2SJfpxk+2krnfAdHn2GEboRzGjRglMWbSiDxOLZhAY3wsVJKQqD9Efw3zshSnPdvKFZ+KeQ==" />
                <button type="submit" class="btn-link text-gray text-small" aria-label="Remove email from profile">
                  <svg class="octicon octicon-x" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"/></svg>
                  Remove from profile
                </button>
</form>            <p class="note">You can manage verified 
              email addresses in your <a href="/settings/emails">email settings</a>.
            </p>
        </dd>
      </dl>
      <dl class="form-group">
        <dt><label for="user_profile_bio">Bio</label></dt>
        <dd class="user-profile-bio-field-container js-suggester-container js-length-limited-input-container">
          <textarea class="form-control user-profile-bio-field js-suggester-field js-length-limited-input" placeholder="Tell us a little bit about yourself" data-input-max-length="160" data-warning-text="{{remaining}} remaining" name="user[profile_bio]" id="user_profile_bio">
</textarea>
          <p class="note">
            You can <strong>@mention</strong> other users and
            organizations to link to them.
          </p>
          <p class="js-length-limited-input-warning user-profile-bio-message d-none"></p>
          <div class="suggester-container">
            <div class="suggester js-suggester js-navigation-container"
                 data-url="/autocomplete/user-suggestions"
                 hidden>
            </div>
          </div>
        </dd>
      </dl>
      <dl class="form-group">
        <dt><label for="user_profile_blog">URL</label></dt>
        <dd><input type="url" class="form-control" value="www.42goals.com" name="user[profile_blog]" id="user_profile_blog" /></dd>
      </dl>
        <dl class="form-group">
          <dt><label for="user_profile_company">Company</label></dt>
          <dd class="user-profile-company-field-container js-suggester-container">
            <input class="form-control js-suggester-field" autocomplete="off" type="text" value="42goals.com" name="user[profile_company]" id="user_profile_company" />
            <p class="note">
              You can <strong>@mention</strong> your company’s GitHub
              organization to link it.
            </p>
            <div class="suggester-container">
              <div class="suggester js-suggester js-navigation-container"
                   data-url="/autocomplete/organizations"
                   hidden>
              </div>
            </div>
          </dd>
        </dl>
      <hr>
      <dl class="form-group">
        <dt><label for="user_profile_location">Location</label></dt>
        <dd><input class="form-control" type="text" value="Tokyo" name="user[profile_location]" id="user_profile_location" /></dd>
      </dl>

      <input type="text" name="required_field_80a5" id="required_field_80a5" hidden="hidden" class="form-control" />
<input type="hidden" name="timestamp" value="1543823039731" class="form-control" />
<input type="hidden" name="timestamp_secret" value="e3862ed012668bf19407de9df69ae868a8f2109f238cdc89d8e3fe86da94eda3" class="form-control" />


        <p class="note mb-2">
          All of the fields on this page are optional and can be deleted at any
          time, and by filling them out, you're giving us consent to share this
          data wherever your user profile appears. Please see our
          <a href="https://github.com/site/privacy">privacy statement</a>
          to learn more about how we use this information.
        </p>

      <p><button type="submit" class="btn btn-primary">Update profile</button></p>
    </div>
</form>  <div class="Subhead Subhead--spacious">
    <h2 class="Subhead-heading">Contributions</h2>
  </div>
  <form class="edit_user" id="user_profile_contributions_3161379" action="/users/spreaderman/set_private_contributions_preference" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="put" /><input type="hidden" name="authenticity_token" value="r58StvTv99z14IwSV9Kc99HTm5+NxM6cGuC2ZwqnxTqwhktEI3A2+7mhgnzn3MXtIyQiRtcgbnx57oF+s5wK8g==" />
    <div class="form-checkbox mt-0">
      <input type="hidden" name="user[show_private_contribution_count]" value="0">
      <input type="checkbox" name="user[show_private_contribution_count]" value="1" id="user_show_private_contribution_count" checked>
      <label for="user_show_private_contribution_count">Include private contributions on my profile</label>
      <span class="note"> Get credit for all your work by showing the number of contributions to private repositories on your profile without any repository or
        organization information. <a href="https://help.github.com/articles/viewing-contributions-on-your-profile-page/">Learn how we count contributions</a>.</span>
    </div>
    <button type="submit" class="btn">Update contributions</button>
</form>  <!-- GitHub Developer Program Membership -->

<div class="Subhead Subhead--spacious mb-0 border-bottom-0">
  <h2 class="Subhead-heading">GitHub Developer Program</h2>
</div>

<div id="github-developer-program" class="Box Box-body clearfix">
    <p class="mb-0">
      Building an application, service, or tool that integrates with GitHub?
      <a href="/developer/register?account=spreaderman">Join the GitHub Developer Program</a>,
      or read more about it at our <a href="https://developer.github.com">Developer site</a>.
    </p>
</div>

<p class="note mb-6 mt-3"><a href="https://developer.github.com">Check out the Developer site</a> for guides, our API
reference, and other resources for building applications that
integrate with GitHub. Make sure your contact information is
up-to-date below. Thanks for being a member!
</p>


    <!-- Jobs Profile -->
    <div class="Subhead Subhead--spacious">
      <h2 class="Subhead-heading">Jobs profile</h2>
    </div>
    <form class="edit_user" id="jobs_profile_3161379" action="/users/spreaderman" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="put" /><input type="hidden" name="authenticity_token" value="Gq/lXkKIDpYeMZmSz0rjZ5ioyLvKLyTBjwhpUrsXMCiI4Cc3Hn9ytcpGpEybjPJf+KfrQEzmLtheOyrW/B2AGg==" />
      <p>
        <input name="user[profile_hireable]" type="hidden" value="0" /><input type="checkbox" value="1" checked="checked" name="user[profile_hireable]" id="user_profile_hireable" />
        <label for="user_profile_hireable">Available for hire</label>
      </p>
      <span class="success" style="display: none"><svg class="octicon octicon-check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"/></svg> Saved</span>
      <button type="submit" class="btn">Save jobs profile</button>
</form>
  </div>
</div>

