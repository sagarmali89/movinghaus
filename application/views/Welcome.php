 
<!-- Sign In Option 2 -->
    <div id="sign_in2">
        <div class="container">
            
            <div class="section_header">
                <h3>Sign in</h3>
            </div>
            
            <div class="row login">
            
                <div class="col-sm-5 left_box">
                    <h4>Log in to your account</h4>

                    <div class="perk_box">
            
                        <div class="perk">
                            <span class="icos ico1"></span>
                            <p><strong>Lorem alteration</strong> in some form  injected humour these randomised words .</p>
                        </div>
            
                        <div class="perk">
                            <span class="icos ico2"></span>
                            <p><strong>There are many variations</strong> of passages of Lorem alteration in some form  injected humour these randomised words.</p>
                        </div>
            
                        <div class="perk">
                            <span class="icos ico3"></span>
                            <p><strong>Alteration in some form</strong> injected humour these randomised words.</p>
                        </div>
            
                    </div>
            
                </div>

                <div class="col-sm-6 signin_box">
                    <div class="box">
                        <div class="box_cont">
                        
                            <div class="social">
 
                                <?php
                              
                                $helper         = $this->fb->getRedirectLoginHelper();
                                $permissions    = $this->config->item('fb_permissions'); 
                                // ['public_profile','email']; // these are the permissions we ask from the Facebook user's profile
                                $theUrl         = base_url();
                                
                                // <!-- show the facebook login logo -->
                                // You can read about getLoginUrl here:  http://fbdevwiki.com/wiki/PHP_getLoginUrl
                                echo anchor($helper->getLoginUrl($theUrl.'Social/facebook', $permissions), 
                                        img(array(
                                            'src'       =>'themes/main_theme/b3/img/social/facebook_login_1.png',
                                            'border'    =>'0',
                                            'alt'       =>'Login with Facebook', 
                                            'width'     => '75%',
                                            'height'    => '75%'
                                      )));
                                ?>

                            </div>
    
                            <!-- show error messages -->
                            <?php if($message): ?>
                            <div class="alert alert-warning">
                                <?php 
                                    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                                    echo $message; 
                                ?>
                            </div>
                            <?php endif; ?>
                            asdasdasd    

                            <div class="form">
                                
                                <form action="<?php echo site_url('auth/login'); ?>" method="post" >
                                
                                  <?php echo form_input($identity, null, 'class="form-control" placeholder="'.lang('login_identity_label').'"');?>
                             
                         
                                  <?php echo form_input($password, null, 'class="form-control" placeholder="'.lang('login_password_label').'"');?>
                         
                                  <?php // echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                                  
                                  <?php // echo lang('login_remember_label', 'remember');?>
                                <div class="forgot">
                                    </br>
                                    <span>Don’t have an account?</span>
                                    <a href="/auth/signup">Sign up</a>
                                </div>
                          
                                <?php echo form_submit('submit', lang('login_submit_btn'));?>
                                  
                                <div class="division">
                                    </br>
                                        <a href="<?php echo site_url('auth/forgot_password'); ?>"><?php echo lang('login_forgot_password');?></a>
                                </div>
                        
                                </form>
                             
                            </div>

                        </div>
                    </div>
                </div> <!-- col-sm-6 signin_box -->
                
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div>  