<?php header ("Content-Type:text/xml"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<Response>
    <Gather timeout="10" numDigits="1" action="/secure_controller/MH_twilio/phone_tree">
        <Say>Choose an option from the menu.  Press 1 for sales.  Press
        2 for customer services.  Press 3 for billing</Say>
    </Gather>
        <Say>You didn't enter an option.  Good bye.</Say>
</Response>