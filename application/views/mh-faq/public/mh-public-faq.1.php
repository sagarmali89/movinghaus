<?php
// echo the pagination links
//echo $links;
?>
<!-- ============================================================== -->
<!-- Faq Content -->
<!-- ============================================================== -->


<h1>
    Latest FAQs 
        <?php 
        if (!empty($this->session->userdata('category'))) { 
            echo 'from "'.$filter_category_name[0]->name.'"'; 
        } 
        ?>
</h1>
    
<div class="row">
    
    <div class="col-9">
        <dl id="faqs">
            
        <?php 
        foreach ($faqs as $faq) : 
        ?>
        
        <div class="row">
         
          <dt><?php echo $faq['title']; ?></dt>
          <dd><?php echo $faq['body']; ?></dd>
 
        </div>
       
        <?php endforeach ?>
        
        </dl>
    </div>
    

    <div class="col-3">
        
        <div class="row">
            
            <div class="col-12">
            
                <p>
                <?php
                // If category is set then a filter is applied.
                if (!empty($this->session->userdata('category'))){
                    echo '<h3>Filter Applied: '.$filter_category_name[0]->name.'</h3>'; 
                    ?>
                    <button type="button"  onclick="window.location.href='/Faq_public/clear_faq'" class="btn btn-outline-primary">Remove Filter</button>
                    <?php
                }
                ?>
                </p>  
            
                <h3>Faq Category</h3>
            
                <?php 
                $i = 0;
                foreach ($link_data as $row) {
                        // if there are faq in the category, show the category
                        // or if no faqs for that category, don't display it.
                        if (!empty($link_data[$i]['number_of_faqs'])) {
                            echo '<span class="badge badge-secondary">'.$link_data[$i]['number_of_faqs'].'</span>';
                            ?>
                            <a href="<?php echo site_url('Faq_public/list_faq_category/'.$link_data[$i]['id']) ?>">
                            <?php
                            echo $link_data[$i]['name'];  
                            echo '  </a>';
                            
                            echo '</br>';
                        }
                        $i++;
                }
                ?>
                
            </div> <!-- end of 12-->
            
            <div class="col-12"></br></br></br>
            </div>
            
            <div class="col-12"><h3></h3>
            </div>
            
        </div>
        
    </div>
    
</div>


<!-- ============================================================== -->
<!-- Faq Content -->
<!-- ============================================================== -->
<?php
// echo the pagination links
echo $links;
?>

