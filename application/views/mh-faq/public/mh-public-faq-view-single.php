<?php
if (empty($faq['post_image'])) {
    $faq['post_image'] = 'noimage.png';
}
?>

<div class="col-xs-12 col-sm-5">
    <img class="<?php echo 'img-thumbnail img-responsive'; ?>" src="<?php echo site_url(); ?>inc/themes/faq/faq_pics/<?php echo $faq['post_image']; ?>" > 
</div>                          
   
 </br>
 
<h2><b><?php echo $faq['title']; ?></b></h2> 

<small class="created_timestamp">Created on: 
    <?php 
    echo $faq['created_timestamp']; 
    if (isset($faq['updated_timestamp'])){
        echo " and Updated on: ";
        echo $faq['updated_timestamp'];
    }
    ?>
    in <strong><?php echo $faq['name']; ?></strong>
</small>

<br>
            
<p class="">
    
    <?php echo $faq['body']; ?>
</p>
                            
                            

                            
                            
    

