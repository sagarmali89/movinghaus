
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Faq Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Faq Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

            <div class="row">
            
                <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">

                        
                           
                     
                        <h2><b><?php echo $faq['title']; ?></b></h2> 
                        
                        <small class="created_timestamp">Created on: 
                            <?php 
                            echo $faq['created_timestamp']; 
                            if (isset($faq['updated_timestamp'])){
                                echo " and Updated on: ";
                                echo $faq['updated_timestamp'];
                            }
                            ?>
                            in <strong><?php echo $faq['name']; ?></strong>
                        </small>
                        
                        <br>
                                    
                        <p class="">
                            
                            <?php echo $faq['body']; ?>
                        </p>
                            
                            
                        
                            <?php 
                                echo form_open('Faq/faq_delete/id/'.$faq['id']); ?> 
                                <a class="btn btn-default" href="<?php echo site_url('Faq_admin'); ?>">List Posts </a>
                                
                                <a class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit" href="/Faq_admin/faq_update/slug/<?php echo $faq['slug']; ?>"><i class="fa fa-edit"></i> Edit</a> 

                                <a href="<?php echo site_url('Faq_admin/faq_delete/id/'.$faq['id'])?>" class="btn btn-danger" value="Delete" data-toggle="tooltip" data-placement="top" title="Delete"   onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i> Delete</a>
                            </form>
                            
                            
    


                </div> <!-- close white box -->
                
            </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
    
	        </div> 

    </div>
    
</div>


