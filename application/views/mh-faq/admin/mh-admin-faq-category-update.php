
<!-- ============================================================== -->
<!-- Header Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Category Management</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Category Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
        <div class="row">
                
            <div class="col-md-12">
            
                <div class="white-box">
                
                    <div class="row ">

                        <!-- ============================================================== -->
                        <!-- The Page Content -->
                        <!-- ============================================================== -->




<h1>New Faq Category</h1>


    <?php
        if ($this->session->flashdata('msg_noti') != '') {
            echo 
                '<div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>' . $this->session->flashdata('msg_noti') . '</p>
                </div>';
        } 
        if ($this->session->flashdata('msg_error') != '') {
            echo 
                '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>'. $this->session->flashdata('msg_error') . '</p>   
                </div>';
        }
        
    ?>

<?php echo form_open('Faq_admin/faq_category_update/'.$category->id);?>


   <input type="hidden" name="id" value="<?php echo $category->id; ?>">
    <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        Enter New Category
	            </label>  
	        
	            <div class="input-group">
	  		    <?php
  			    $name = array(
			        'name'        => 'name',
			        'id'          => 'name',
			        'value'       => set_value('name', $category->name),
			        'style'       => '',
			        'class'       => 'form-control',
			        'placeholder' => ''
			    );	
				?>
 		 	    <?php echo form_error('name'); ?>
			    <?php echo form_input($name); ?>
                </div>
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
    
    
        <!-- Features Row -->
    <div class="row">
        
        <div class="col-sm-10 col-xs-10"> 
        
            <!-- Text input-->
            <div class="form-group">
	        
	            <label for="newcategory">
	  		        
	            </label>  
	        
	            
	               <a class="btn btn-default btn-lg" href="<?php echo site_url('Faq_admin/faq_category_index'); ?>">List Categories </a> 
	  		    <?php
                echo form_submit('faq_category_update', 'Save', 'class="btn btn-default btn-lg "');
                
                
                echo form_close();
                ?>
                
                
            
            </div> <!-- end form group -->
            
        </div> 
    
    </div> <!-- end row -->
    
    
    
    
    
    
    
                

    
                        <!-- ============================================================== -->
                        <!-- End of The Page Content -->
                        <!-- ============================================================== -->

                    </div><!-- end row -->

                </div> <!-- close white box -->
            
            </div> <!-- close 12 column width -->
        
        </div> <!-- close row -->
            <!-- close up page content -->
    </div> <!-- close fluid container -->
        
</div> <!-- close page wrapper -->
