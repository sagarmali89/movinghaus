
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    
    <div class="container-fluid">
    
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Faq Management</h4> 
            </div>
           
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Faq Management</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="white-box">
                    
                    <div class="row ">
                    <h1>All FAQs <?php if (!empty($category_name)) { echo 'from "'.$category_name.'"'; } ?></h1>
                    <?php
                    
                	$message = $this->session->flashdata('message');
                	if (isset($message['message'])) {
                			echo '<br>';
                			echo '<div class="alert alert-'.$message['class'].'">';
                			echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  			echo $message['message'];
                			echo '</div>';
                	}?>
                	
                	<p>
                	<a class="btn btn-primary btn-sm" href="<?php echo site_url('Faq_admin/faq_create/'); ?>"><i class="fa fa-plus"></i> New FAQ</a>
                	</p>
                	
                	
                	<div>
                	
                        <table id="table-mh-admin-faq-index" class="table table-condensed table-bordered table-striped table-hover" width="100%">
				            <thead>
						        <tr>
								    <th>Category</th>
								    <th>Author</th>
								    <th>Title / Post</th>
								    <th>Created</th>
								    <th>Updated</th>
								    <th></th>
							    </tr>
		                    </thead>
						
						    <tbody>	
							<?php foreach ($faqs as $faq) : ?>
							
							                    
                  
							<tr>
							    <?php 
					                // look up user by id from the posts table
							        $user = $this->ion_auth->user($faq['user_id'])->row_array(); 
							    ?>
							    
							    <td>
							        <?php if (!empty($faq['name'])) { echo $faq['name']; } ?>
					            </td>
					            
							    <td>
							        <?php if (!empty($user['username'])) { echo $user['username']; } ?>
					            </td>
					            
							    <td>
							        
							        <div class="row">
        
                                  
                        
                                    <div class="col-md-12">
							        
				                    <p> 
				                        <b> 
				                            <?php if (!empty($faq['title'])) { echo $faq['title']; }?> 
				                        </b> 
				                    </p>
							        
							        <p>
					                    <?php echo word_limiter(strip_tags($faq['body']), $this->config->item('number_of_words_to_display')); ?> <span class="label label-default label-rouded">
							                 
					                    <a href="<?php echo site_url('Faq_admin/faq_view_single/'.$faq['slug']); ?>">Read More ...</a>  
							                 
							         </p>
							         </div>
							         </div>
							    </td>
							    
					          <!-- created --> 
					          <?php
							       
    							    if (isset($faq['created_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($faq['created_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>
							 
					          
					          

							   <!-- updated -->
							      <?php
							       
    							    if (isset($faq['updated_timestamp'])){
                                        // returns a datetime in gmt
        					            $mydate = strtotime($faq['updated_timestamp']);
        					            
        					            //converts to epoch time in user's timezone.
        					            $user_timezone_epoch = gmt_to_local($mydate, $this->session->userdata('user_time_zone') );
        					             
        					            // creates a human readable time    
                                        $user_timezone_human = unix_to_human($user_timezone_epoch);
                                        
                                        echo '  <td data-sort="'. $user_timezone_epoch .'">'.$user_timezone_human .'</td>';
                                   
                                    } else {
                                        echo "<td>N/A</td>";
                                    }
							    ?>
							    
							    
							    <td  style="white-space:nowrap;">
							        <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit" href="/Faq_admin/faq_update/slug/<?php echo $faq['slug']; ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View" href="<?php echo site_url('Faq_admin/faq_view_single/'.$faq['slug']); ?>"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('Faq_admin/faq_delete/id/'.$faq['id'])?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></a> 
							    </td>
							     
							</tr>
						<?php endforeach ?>
						</tbody>
						</table>
						
						
                    </div>

                    <div class="row">
                        <div class="pagination-links">
                        <?php   
                        echo $this->pagination->create_links();
                        ?>  
                        </div>
                    </div>    <!-- end of row -->

                    </div> <!-- close white box -->
                
                </div> <!-- close 12 column width -->
            
            </div> <!-- close row -->
        <!-- close up page content -->
	    </div> <!-- close fluid container -->
    </div> <!-- close page wrapper -->
