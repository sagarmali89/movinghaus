<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_model extends CI_model {

    // returns the number of file entries
    function file_count(){
        $this->db->from('files');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    // $per_page is the limit.
    function files_get($slug = FALSE, $per_page = 5, $offset = 0, $year='', $month='', $category='' ){

        // pull many files
        if ($slug === FALSE) {

            $this->db->select('
                file_categories.*,
                files.id,
                files.category_id,
                files.user_id,
                files.updated_user_id,
                files.title,
                files.slug,
                files.body,
                files.post_image,
                files.created_timestamp,
                files.updated_timestamp
            ');
            $this->db->join('file_categories','file_categories.id = files.category_id');

            // if user has requested to filter my month and year
            if ( (!empty($year)) && (!empty($month)) ) {
                $this->db->where('year(files.created_timestamp)', $year);
                $this->db->where('month(files.created_timestamp)', $month);
		    }
		    if (!empty($category))   {
		        $this->db->where('category_id', $category);
		    }
		    $this->db->limit($per_page, $offset);
            $query = $this->db->get('files');
            return $query->result_array();
        } else {
            // pull one file
            $this->db->select('file_categories.*,files.*');
            $this->db->join('file_categories','file_categories.id = files.category_id');
            $query = $this->db->get_where('files', array('slug' => $slug));
            return $query->row_array();
        }
    }


    function file_create($submitted_data_array){
        return $this->db->insert('files', $submitted_data_array);
    }
    
    function get_category_name($category_id){
        // take in a category_id and returns a name
        $this->db->select('file_categories.name');
        $this->db->from('file_categories');
        $this->db->where('file_categories.id', $category_id); 
        return $this->db->get()->result(); 
        
    }
    
    
    function file_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('files');
    }
    
    // returns FALSE on FAIL and and the number of affected rows is successful.
    function file_image_delete($id){
        $data = array('post_image' => '');
        $this->db->where('id', $id);        
        $this->db->update('files', $data);
    }

    // Return FALSE on FAIL or the number of updated records, eg 1, 2, etc.
    function file_update($submitted_data_array){
        $this->db->where('id', $submitted_data_array['id']);
        $result = $this->db->update('files', $submitted_data_array);
    }
    
    function file_get_categories(){
        $this->db->select('
            file_categories.id, 
            file_categories.name, 
            file_categories.user_id,
            file_categories.created_timestamp,
            file_categories.updated_timestamp,
            COUNT(files.id) as number_of_posts
        ');
        $this->db->from('file_categories');
        $this->db->join('files', 'file_categories.id = files.category_id', 'left');
        $this->db->group_by('file_categories.name');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function file_category_create($data) {
        if ($this->db->insert('file_categories', $data)){
            return TRUE;
        } else { 
            return FALSE;
        }
    }
    
    // takes in a category id
    function file_category_name_get($id){
        $query = $this->db->get_where('file_categories', array('id'=>$id));
        return $query->row();
    }
    
    // takes in a category id
    function file_posts_by_category($id){
        $this->db->select('file_categories.*,files.*');
        $this->db->join('file_categories','file_categories.id = files.category_id');
        $query = $this->db->get_where('files', array('category_id'=>$id));
        return $query->result_array();
    }
    
    function file_category_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('file_categories');
    }
    
    // returns the number of files using $id as category
    function count_posts_by_category($id) {
        $this->db->select('*');
        $this->db->from('files');
        $this->db->where('category_id', $id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    // takes a new slug and sees if it is unique
    // if unique TRUE else FALSE
    function is_slug_unique_on_update($new_slug) {
        $result = $this->db->where(['slug'=>$new_slug])->from("files")->count_all_results();
        if ($result == '' || $result =='0') {
            return TRUE;
        } elseif ($result >= 1) {
            return FALSE;
        }
    }
    

    /** category update **/   
    function category_update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('file_categories', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
           return false;
        }
        else {
            return true;
        }

    }
    
    function file_archive(){
        
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('files');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $file_archive = $this->db->get()->result_array();;
        return $file_archive;
    }
         
         

}