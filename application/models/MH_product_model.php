<?php
class MH_product_model extends CI_Model{
 
    // legacy function for front end.
    function get_all_product(){
        $result=$this->db->get('product');
        return $result;
    }
    
    // returns the number of product entries
    function product_count(){
        $this->db->from('product');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    // $per_page is the limit.
    function products_get($slug = FALSE, $per_page = 5, $offset = 0, $year='', $month='', $category=''){
        // pull many products
        if ($slug === FALSE) {
            $this->db->select('
                product_categories.*,
                product.product_id,
                product.category_id,
                product.user_id,
                product.updated_user_id,
                product.product_description,
                product.product_price,
                product.product_slug,
                product.product_name,
                product.product_image,
                product.created_timestamp,
                product.updated_timestamp
            ');
            $this->db->join('product_categories','product_categories.id = product.category_id');

            // if user has requested to filter my month and year
            if ( (!empty($year)) && (!empty($month)) ) {
                $this->db->where('year(product.created_timestamp)', $year);
                $this->db->where('month(product.created_timestamp)', $month);
		    }
		    if (!empty($category))   {
		        $this->db->where('category_id', $category);
		    }
		    $this->db->limit($per_page, $offset);
            $query = $this->db->get('product');
            return $query->result_array();
        } else {
            // pull one product
            $this->db->select('product_categories.*,product.*');
            $this->db->join('product_categories','product_categories.id = product.category_id');
            $query = $this->db->get_where('product', array('product_slug' => $slug));
            return $query->row_array();
        }
    }


    function product_create($submitted_data_array){
        return $this->db->insert('product', $submitted_data_array);
    }
    
    function product_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('product');
    }
    
    // returns FALSE on FAIL and and the number of affected rows is successful.
    function product_image_delete($id){
        $data = array('post_image' => '');
        $this->db->where('id', $id);        
        $this->db->update('product', $data);
    }

    // Return FALSE on FAIL or the number of updated records, eg 1, 2, etc.
    function product_update($submitted_data_array){
        $this->db->where('id', $submitted_data_array['id']);
        $result = $this->db->update('product', $submitted_data_array);
    }
    
    function product_get_categories(){
        $this->db->select('
            product_categories.id, 
            product_categories.name, 
            product_categories.user_id,
            product_categories.created_timestamp,
            product_categories.updated_timestamp,
            COUNT(product.product_id) as number_of_product
        ');
        $this->db->from('product_categories');
        $this->db->join('product', 'product_categories.id = product.category_id', 'left');
        $this->db->group_by('product_categories.name');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function product_category_create($data) {
        if ($this->db->insert('categories', $data)){
            return TRUE;
        } else { 
            return FALSE;
        }
    }
    
    // takes in a category id
    function product_category_name_get($id){
        $query = $this->db->get_where('categories', array('id'=>$id));
        return $query->row();
    }
    
    // takes in a category id
    function product_posts_by_category($id){
        $this->db->select('categories.*,product.*');
        $this->db->join('categories','categories.id = product.category_id');
        $query = $this->db->get_where('product', array('category_id'=>$id));
        return $query->result_array();
    }
    
    function product_category_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('categories');
    }
    
    // returns the number of product using $id as category
    function count_posts_by_category($id) {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('category_id', $id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    // takes a new slug and sees if it is unique
    // if unique TRUE else FALSE
    function is_slug_unique_on_update($new_slug) {
        $result = $this->db->where(['slug'=>$new_slug])->from("product")->count_all_results();
        if ($result == '' || $result =='0') {
            return TRUE;
        } elseif ($result >= 1) {
            return FALSE;
        }
    }
    

    /** category update **/   
    function category_update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
           return false;
        }
        else {
            return true;
        }

    }
    
    function product_archive(){
        
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('product');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $product_archive = $this->db->get()->result_array();;
        return $product_archive;
    }
         
         

    //  This is ussed to produce the archive list and number of articles.
     function not_used_product_archive(){
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('product');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $product_archive = $this->db->get()->result_array();;
        return $product_archive;
        
        /**
         * The above active record prodoces the below sql:
         * select
         *      year(created_timestamp) as year,
         *      month(created_timestamp)as month,
         *      MONTHNAME(created_timestamp) month_name,    
         *      count(*) total_published
         *      From product
         *      GROUP BY year, MONTH(created_timestamp), MONTHNAME(created_timestamp)
         *      ORDER BY year DESC, month DESC';
         **/           
     }
 
 
 
}