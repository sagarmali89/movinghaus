<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccci_model extends CI_model {
    
    // this will either add a new entry if the array doesn't exist or
    // update the data if it does exist.
    // returns number of 
    public function ccci_country_list_update_db($country_list_array = NULL)
	{
	    if ($country_list_array != NULL) {
	        foreach ($country_list_array as $key => $country_list ) {
	            $data = $country_list_array[$key];
	            $this->db->replace('ccci_countries', $data);
	        }
	        return TRUE;
	    } else {
	        return FALSE;
	    }
	}
	
	// takes a 3 letter country name and returns the code
	// This method was created primarily to work with 
	// https://countryflags.io/
	// Can put a cool little flag by the exchange rate, for example.
	public function get_id(){
	    
	}	
	
	public function ccci_country_list_db()
	{
		
	}
	
	public function ccci_get_country_code($country)
	{
		
	}
	
	public function ccci_update_xrates($ccci_table_array = NULL)
	{
		if ($ccci_table_array != NULL) {
			// symbol_to_from, ccci_rate_of_exchange, ccci_updated
			foreach ($ccci_table_array as $key => $value)
			{
				$data = array(
						'ccci_symbol_to_from' => $key,
						'ccci_rate_of_exchange' => $value,
						'ccci_updated' => date("Y-m-d H:i:s")
					);
				$this->db->replace('ccci_xrates', $data);
			}	
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function ccci_get_xrates(){
		$this->db->select('ccci_symbol_to_from, ccci_rate_of_exchange, ccci_updated');
		$this->db->from('ccci_xrates');
		$this->db->order_by('ccci_symbol_to_from', 'asc');
		$query = $this->db->get();   
		return $query->result_array();
	}

}
