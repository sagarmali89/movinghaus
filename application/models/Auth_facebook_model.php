<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Auth_facebook_model extends CI_Model {
  
  public function __construct() {
    parent::__construct();
  }
  
  public function login_with_facebook($email) {
    $this->db->where('email',$email);
    $this->db->limit(1);
    $users = $this->db->count_all_results('users'); // are there any users with that email address?
    // if NOT in db register him in the db
    if(!isset($users) || $users<1) {
        $this->load->helper('string');
        $password = random_string('alnum',10); // we create a random password for the user...
        $register_id = $this->ion_auth->register($username,$password,$email,array(),array('2'));
        if($register_id) {
          $this->ion_auth->activate($register_id);
          $this->ion_auth->login($email,$password, TRUE);
        }
      // return FALSE;
    } else { // if already in db, log him in and set some session vars for easy access.
    
      $user = $this->db->where(array('email'=>$email))->limit(1)->get('users')->row();
      $_SESSION['identity']         = $user->email; // if you've set up email as the login identity column, you shouls use $user->email in here...
      $_SESSION['username']         = $user->username;
      $_SESSION['email']            = $user->email;
      $_SESSION['user_id']          = $user->id;
      $_SESSION['old_last_login']   = $user->last_login;
      // user's facebook access token is here.
      //$_SESSION['accessToken']      = $user->accessToken;

      return TRUE;
    }
  }
}