<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class MH_auth_admin_model extends CI_Model {
  public $error_message = "";


  public function __construct() {
    parent::__construct();
  }

  // fetch all loading zones from database for forms dropdown
  public function get_loading_zones(){
    $this->db->select('*');
    $this->db->from('loading_zone');
    return $this->db->get()->result();
  }

  // fatch all rates for usage as move to for forms dropdown 
  public function get_rate_to(){
    $this->db->select('rate_id, rate_to');
    $this->db->from('rates');
    return $this->db->get()->result();
  }
  
  // return loading zone name for display insted of id
  public function get_loading_zone_name($id){
    $this->db->select('loading_zone_name');
    $this->db->from('loading_zone');
    $this->db->where('loading_zone_id', $id);
    $name = $this->db->get()->row();
    if(sizeof($name) > 0){
      return $name->loading_zone_name;
    }else{
      return "";
    }
  }

  public function get_rate_to_value($id){
    $this->db->select('rate_to');
    $this->db->from('rates');
    $this->db->where('rate_id', $id);
    $row = $this->db->get()->row();
    if(sizeof($row) > 0){
      return $row->rate_to;
    }else{
      return "";
    }
  }

  // delete padrticular user record from database
  public function delete_user($id){
    if($this->db->delete('users', array('id' => $id))){
      return true;
    } else {
      return false;
    }; 
  }

  // fetch all employees records from database
  public function get_employees(){
    $this->db->select('*');
    $this->db->from('employees');
    return $this->db->get()->result();    
  }

  public function create_employee($identity, $password, $email, $additional_data){

    print_r($identity);
    echo "<br>";
    print_r($password);
    echo "<br>";
    print_r($email);
    echo "<br>";
    print_r($additional_data);

    //$hash = md5( rand(0,1000) )

  }
}