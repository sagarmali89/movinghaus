<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MH_login_attempts_model extends CI_model {
    
    public $table   = 'login_attempts';
    public $id      = 'id';
    public $order   = 'DESC';
    
    function __construct() {
        parent::__construct();
    }
    
    /*---------- LIST ----------*/
        
    function index_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    
    /*---------- CREATE ----------*/
    
    function create($data){
        
         $this->db->insert($this->table, $data);
         $id = $this->db->insert_id();
         return $id;
         
    }
    
    
    /*---------- READ ----------*/
    
    function read(){
        
    }
    
    
    /*---------- UPDATE ----------*/
    
    function update(){
        
    }
    
    
    /*---------- DELETE ----------*/
    
    function delete($id){
        // delete login attempt from login_attempts table.  Return true or false
        $result = $this->db->delete($this->table, array('id' => $id));
        if ($result == TRUE) {
            echo "TRUE";
            return TRUE;
        } else {
            echo "FALSE";
            return FALSE;
        }
    }
    
}