<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class MH_auth_public_model extends CI_Model {
  
  public function __construct() {
    parent::__construct();
  }

  public function get_loading_zones(){
    $this->db->select('*');
    $this->db->from('loading_zone');
    return $this->db->get()->result();
  }

  public function get_rate_to(){
    $this->db->select('rate_id, rate_to');
    $this->db->from('rates');
    return $this->db->get()->result();
  }
}