<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends CI_model {

    // returns the number of faq entries
    function faq_count(){
        $this->db->from('faqs');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    // $per_page is the limit.
    function faqs_get($slug = FALSE, $per_page = 5, $offset = 0, $year='', $month='', $category='' ){

        // pull many faqs
        if ($slug === FALSE) {

            $this->db->select('
                faq_categories.*,
                faqs.id,
                faqs.category_id,
                faqs.user_id,
                faqs.updated_user_id,
                faqs.title,
                faqs.slug,
                faqs.body,
                faqs.post_image,
                faqs.created_timestamp,
                faqs.updated_timestamp
            ');
            $this->db->join('faq_categories','faq_categories.id = faqs.category_id');

            // if user has requested to filter my month and year
            if ( (!empty($year)) && (!empty($month)) ) {
                $this->db->where('year(faqs.created_timestamp)', $year);
                $this->db->where('month(faqs.created_timestamp)', $month);
		    }
		    if (!empty($category))   {
		        $this->db->where('category_id', $category);
		    }
		    $this->db->limit($per_page, $offset);
            $query = $this->db->get('faqs');
            return $query->result_array();
        } else {
            // pull one faq
            $this->db->select('faq_categories.*,faqs.*');
            $this->db->join('faq_categories','faq_categories.id = faqs.category_id');
            $query = $this->db->get_where('faqs', array('slug' => $slug));
            return $query->row_array();
        }
    }

    function get_category_name($category_id){
        // take in a category_id and returns a name
        $this->db->select('faq_categories.name');
        $this->db->from('faq_categories');
        $this->db->where('faq_categories.id', $category_id); 
        return $this->db->get()->result(); 
        
    }

    function faq_create($submitted_data_array){
        return $this->db->insert('faqs', $submitted_data_array);
    }
    
    function faq_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('faqs');
    }
    
    // returns FALSE on FAIL and and the number of affected rows is successful.
    function faq_image_delete($id){
        $data = array('post_image' => '');
        $this->db->where('id', $id);        
        $this->db->update('faqs', $data);
    }

    // Return FALSE on FAIL or the number of updated records, eg 1, 2, etc.
    function faq_update($submitted_data_array){
        $this->db->where('id', $submitted_data_array['id']);
        $result = $this->db->update('faqs', $submitted_data_array);
    }
    
    function faq_get_categories(){
        $this->db->select('
            faq_categories.id, 
            faq_categories.name, 
            faq_categories.user_id,
            faq_categories.created_timestamp,
            faq_categories.updated_timestamp,
            COUNT(faqs.id) as number_of_faqs
        ');
        $this->db->from('faq_categories');
        $this->db->join('faqs', 'faq_categories.id = faqs.category_id', 'left');
        $this->db->group_by('faq_categories.name');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function faq_category_create($data) {
        if ($this->db->insert('faq_categories', $data)){
            return TRUE;
        } else { 
            return FALSE;
        }
    }
    
    // takes in a category id
    function faq_category_name_get($id){
        $query = $this->db->get_where('faq_categories', array('id'=>$id));
        return $query->row();
    }
    
    // takes in a category id
    function faq_posts_by_category($id){
        $this->db->select('faq_categories.*,faqs.*');
        $this->db->join('faq_categories','faq_categories.id = faqs.category_id');
        $query = $this->db->get_where('faqs', array('category_id'=>$id));
        return $query->result_array();
    }
    
    function faq_category_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('faq_categories');
    }
    
    // returns the number of faqs using $id as category
    function count_posts_by_category($id) {
        $this->db->select('*');
        $this->db->from('faqs');
        $this->db->where('category_id', $id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    // takes a new slug and sees if it is unique
    // if unique TRUE else FALSE
    function is_slug_unique_on_update($new_slug) {
        $result = $this->db->where(['slug'=>$new_slug])->from("faqs")->count_all_results();
        if ($result == '' || $result =='0') {
            return TRUE;
        } elseif ($result >= 1) {
            return FALSE;
        }
    }
    

    /** category update **/   
    function category_update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('faq_categories', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
           return false;
        }
        else {
            return true;
        }

    }
    
    function faq_archive(){
        
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('faqs');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $faq_archive = $this->db->get()->result_array();;
        return $faq_archive;
    }
         
         

    //  This is ussed to produce the archive list and number of articles.
     function not_used_faq_archive(){
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('faqs');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $faq_archive = $this->db->get()->result_array();;
        return $faq_archive;
        
        /**
         * The above active record prodoces the below sql:
         * select
         *      year(created_timestamp) as year,
         *      month(created_timestamp)as month,
         *      MONTHNAME(created_timestamp) month_name,    
         *      count(*) total_published
         *      From faqs
         *      GROUP BY year, MONTH(created_timestamp), MONTHNAME(created_timestamp)
         *      ORDER BY year DESC, month DESC';
         **/           
     }
     
}