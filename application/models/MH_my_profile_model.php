<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MH_my_profile_model extends CI_model {
    
    public $table   = 'login_attempts';
    public $id      = 'id';
    public $order   = 'DESC';
    
    function __construct() {
        parent::__construct();
    }
    
    /*---------- LIST ----------*/
        
    function index_all() {
    }
    
    
    /*---------- CREATE ----------*/
    
    function create($data){
    }
    
    
    /*---------- READ ----------*/
    
    function read(){
    }
    
    
    /*---------- UPDATE ----------*/
    
    function update(){
    }
    
    
    /*---------- DELETE ----------*/
    
    function delete($id){
    }
    
}