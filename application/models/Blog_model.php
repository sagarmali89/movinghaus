<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_model {

    // returns the number of blog entries
    function blog_count(){
        $this->db->from('posts');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    // $per_page is the limit.
    function blogs_get($slug = FALSE, $per_page = 5, $offset = 0, $year='', $month='' ){

        // pull many blogs
        if ($slug === FALSE) {

            $this->db->select('
                categories.*,
                posts.id,
                posts.category_id,
                posts.user_id,
                posts.updated_user_id,
                posts.title,
                posts.slug,
                posts.body,
                posts.post_image,
                posts.created_timestamp,
                posts.updated_timestamp
                
            ');
            $this->db->join('categories','categories.id = posts.category_id');

            // if user has requested to filter my month and year
            if ( (!empty($year)) && (!empty($month)) ) {
                $this->db->where('year(posts.created_timestamp)', $year);
                $this->db->where('month(posts.created_timestamp)', $month);
		    }
		    $this->db->limit($per_page, $offset);
            $query = $this->db->get('posts');
            return $query->result_array();
        } else {
            // pull one blog
            $this->db->select('categories.*,posts.*');
            $this->db->join('categories','categories.id = posts.category_id');
            $query = $this->db->get_where('posts', array('slug' => $slug));
            return $query->row_array();
        }
    }


    function blog_create($submitted_data_array){
        return $this->db->insert('posts', $submitted_data_array);
    }
    
    function blog_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('posts');
    }
    
    // returns FALSE on FAIL and and the number of affected rows is successful.
    function blog_image_delete($id){
        $data = array('post_image' => '');
        $this->db->where('id', $id);        
        $this->db->update('posts', $data);
    }

    // Return FALSE on FAIL or the number of updated records, eg 1, 2, etc.
    function blog_update($submitted_data_array){
        $this->db->where('id', $submitted_data_array['id']);
        $result = $this->db->update('posts', $submitted_data_array);
    }
    
    function blog_get_categories(){
        $this->db->select('
            categories.id, 
            categories.name, 
            categories.user_id,
            categories.created_timestamp,
            categories.updated_timestamp,
            COUNT(posts.id) as number_of_posts
        ');
        $this->db->from('categories');
        $this->db->join('posts', 'categories.id = posts.category_id', 'left');
        $this->db->group_by('categories.name');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function blog_category_create($data) {
        if ($this->db->insert('categories', $data)){
            return TRUE;
        } else { 
            return FALSE;
        }
    }
    
    // takes in a category id
    function blog_category_name_get($id){
        $query = $this->db->get_where('categories', array('id'=>$id));
        return $query->row();
    }
    
    // takes in a category id
    function blog_posts_by_category($id){
        $this->db->select('categories.*,posts.*');
        $this->db->join('categories','categories.id = posts.category_id');
        $query = $this->db->get_where('posts', array('category_id'=>$id));
        return $query->result_array();
    }
    
    function blog_category_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('categories');
    }
    
    // returns the number of posts using $id as category
    function count_posts_by_category($id) {
        $this->db->select('*');
        $this->db->from('posts');
        $this->db->where('category_id', $id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    // takes a new slug and sees if it is unique
    // if unique TRUE else FALSE
    function is_slug_unique_on_update($new_slug) {
        $result = $this->db->where(['slug'=>$new_slug])->from("posts")->count_all_results();
        if ($result == '' || $result =='0') {
            return TRUE;
        } elseif ($result >= 1) {
            return FALSE;
        }
    }
    

    /** category update **/   
    function category_update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
           return false;
        }
        else {
            return true;
        }

    }
    
    function blog_archive(){
        
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('posts');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $blog_archive = $this->db->get()->result_array();;
        return $blog_archive;
    }
         
         

    //  This is ussed to produce the archive list and number of articles.
     function not_used_blog_archive(){
        $this->db->select('year(created_timestamp) as year, month(created_timestamp) as month, monthname(created_timestamp) as monthname, COUNT(*) post_count');
        $this->db->from('posts');
        $this->db->group_by('year');
        $this->db->group_by('monthname');
        $this->db->group_by('month');
        $this->db->order_by('year', 'desc');
        $this->db->order_by('month', 'desc');
        $blog_archive = $this->db->get()->result_array();;
        return $blog_archive;
        
        /**
         * The above active record prodoces the below sql:
         * select
         *      year(created_timestamp) as year,
         *      month(created_timestamp)as month,
         *      MONTHNAME(created_timestamp) month_name,    
         *      count(*) total_published
         *      From posts
         *      GROUP BY year, MONTH(created_timestamp), MONTHNAME(created_timestamp)
         *      ORDER BY year DESC, month DESC';
         **/           
     }
     
}