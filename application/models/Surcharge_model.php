<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surcharge_model extends CI_model {

    function surcharge_load_data(){
        $this->db->select('surcharges.*,surcharge_categories.surcharge_category_name, units.unit_name');
        $this->db->order_by('surcharges.surcharge_id', 'DESC');
        $this->db->join('surcharge_categories', 'surcharges.surcharge_category_id = surcharge_categories.surcharge_category_id', 'left');
        $this->db->join('units', 'surcharges.surcharge_unit_id = units.unit_id', 'left');
        $query = $this->db->get('surcharges');
        return $query->result_array();
    }
    
    function get_surcharge_categories(){
        $this->db->select('surcharge_category_id, surcharge_category_name');
        $this->db->from('surcharge_categories');
        $this->db->order_by('surcharge_category_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_surcharge_curr(){
        $this->db->select('currencyId');
        $this->db->from('ccci_countries');
        $this->db->group_by('currencyId'); 
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_surcharge_units(){
        $this->db->select('*');
        $this->db->from('units');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_surcharge_names(){
        $this->db->select('surcharge_name');
        $this->db->from('surcharges');
        $query = $this->db->get();
        return $query->result();
    }


    function update_surcharge($surcharge_id, $data){
        $this->db->where('surcharge_id',$surcharge_id);
        if($this->db->update('surcharges',$data)){
            return true;
        } else {
            return false;
        };
    }
    
    function create_surcharge($data){
        if($this->db->insert('surcharges',$data)){
            return $this->db->insert_id();;
        } else {
            return false;
        };
    }
    
    function delete_surcharge($id){
        $this->db->where('surcharge_id', $id);
        $this->db->delete('surcharges');
    }
    
    function surcharge_count(){
        $this->db->from('surcharges');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    

    

/**
 * 
 * THIS SECTION USED FOR SURCHARGE CATEGORIES
 * 
 **/

    function surcharge_get_categories(){
        $this->db->select('
            surcharge_categories.surcharge_category_id, 
            surcharge_categories.surcharge_category_name, 
            surcharge_categories.surcharge_category_comment, 
            surcharge_categories.created_timestamp,
            surcharge_categories.updated_timestamp,
            COUNT(surcharges.surcharge_id) as number_of_surcharges
        ');
        $this->db->from('surcharge_categories');
        $this->db->join('surcharges', 'surcharge_categories.surcharge_category_id = surcharges.surcharge_category_id', 'left');
        $this->db->group_by('surcharge_categories.surcharge_category_name');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function surcharge_category_create($data) {
        if ($this->db->insert('surcharge_categories', $data)){
            return TRUE;
        } else { 
            return FALSE;
        }
    }
    
    // takes in a category id
    function surcharge_category_name_get($surcharge_category_id){
        $query = $this->db->get_where('surcharge_categories', array('surcharge_category_id'=>$surcharge_category_id));
        return $query->row();
    }
    
    function surcharge_category_delete($id){
        $this->db->where('surcharge_category_id', $id);
        $this->db->delete('surcharge_categories');
    }
    
    // returns the number of surcharges using $id as the surcharge category
    function count_surcharges_by_category($id) {
        $this->db->select('*');
        $this->db->from('surcharges');
        $this->db->where('surcharge_category_id', $id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    /** category update **/   
    function surcharge_category_update($surcharge_category_id, $data)
    {
        $this->db->trans_start();
        $this->db->where('surcharge_category_id', $surcharge_category_id);
        $this->db->update('surcharge_categories', $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
           return false;
        } else {
            return true;
        }
    }
    
}