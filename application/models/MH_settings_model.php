<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** this sets the main settings for the website **/

class MH_settings_model extends CI_Model {
 
    protected $table;
 
    public function __construct() {
                $this->table = 'mh_settings';
    }
 
    public function mh_get_configurations() {
            $query = $this->db->get($this->table);
            return $query->result();
    }
    
}