<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** this sets the main settings for the website **/

class MH_user_settings_model extends CI_Model {
 
    protected $table;
 
    public function __construct() {
        $this->table = 'mh_user_settings';
    }
 
    /** if there are settings, they will be returned, otherwise
     *  FALSE will be returned
     **/
     
    public function mh_get_configurations() {
        
        $mh_user_settings = array();
        
        // get the settings for the logged in user
        $this->db->where('mh_us_user_id', $this->ion_auth->user()->row()->id);
        
        // Get the settings from the database
        $query = $this->db->get($this->table);
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $settingRow) {
                $settings[$settingRow->mh_us_key] = $settingRow->mh_us_value;
            }
            $this->config->set_item('mh_user_settings', $settings);
            return $query->result_array();
        } else {
            return FALSE;
        }

       
       // print_r($this->config->item('mh_user_settings'));
        
        //echo $this->config->item('user_time_zone', 'mh_user_settings');
        
    }
    
    public function mh_insert_or_update_setting($setting_name, $timezone, $user_id) {
        // check to see if there is an entry.  If there is, update, 
        // if there isn't, add new entry.
        $this->db->where('mh_us_key', $setting_name);
        $this->db->where('mh_us_user_id', $user_id);
        $q = $this->db->get('mh_user_settings');
        
        
        if ( $q->num_rows() > 0 )  {
            // update here    
             $data = array(
                'mh_us_key'     => $setting_name,
                'mh_us_value'   => $timezone,
            );
            $this->db->where('mh_us_user_id',$user_id);
            $this->db->where('mh_us_key',$setting_name);
            $this->db->update('mh_user_settings',$data);
            
            
                
                
        } else {
            $data = array(
                'mh_us_key'     => $setting_name,
                'mh_us_value'   => $timezone,
            );
            $this->db->set('mh_us_user_id', $user_id);
            $this->db->insert('mh_user_settings',$data);
        }
    }
}