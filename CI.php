 <?php 
 // defined('BASEPATH') OR exit('No direct script access allowed');  <-- this will not work.
 // this is to enable an instance of CI outside the CI application folder.  This is used by CKFinder.
 ob_start();
 define('REQUEST', 'external');
 require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "index.php"; //or wherever the directory is relative to your path
 ob_end_clean();
 return $CI;